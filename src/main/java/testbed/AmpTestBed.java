package testbed;

import amp.*;
import amp.exceptions.AmpException;
import amp.group_ids.GroupID;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.factory.DefaultHPSFactory;
import amp.headless_prefixing_strategies.strategies.complex_hps.ComplexHPSWritable;
import amp.headless_prefixing_strategies.strategies.key_value_hps.KeyValueHPSMapConverter;
import amp.headless_prefixing_strategies.strategies.simple_hps.SimpleHPSReadable;
import amp.headless_prefixing_strategies.strategies.simple_hps.SimpleHPSWritable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyWritable;
import amp.serialization.IAmpByteFactory;
import amp.serialization.IAmpDeserializable;
import amp.typed_data.defaults.DefaultDataTypeEnum;
import amp.typed_data.IDataType;
import amp.typed_data.ITypedData;
import logging.IAmpexLogger;

import java.math.BigInteger;
import java.util.*;


/**
 * The Amp Binary Serialization Library was created by John Minor.
 */
public class AmpTestBed
{
    private static final GroupID testGroupID = new GroupID(5, 10, "Test Group");
    private static final GroupID writeLockedTestGroupID = ByteTools.writeLockGroupID(testGroupID);

    public static final GroupID PAYLOAD_GROUP_ID = new GroupID(1, 1, "Payload");
    public static final GroupID ID_GROUP_ID = new GroupID(1, 1, "ID");

    public static void main(String[] args) throws Exception
    {
        AmpLogging.startLogging();

        IAmpexLogger logger = AmpLogging.getLogger();

        if (logger == null)
        {
            throw new AmpException("WHY IS THIS NULL?");
        }

        IDataType type = DefaultDataTypeEnum.INDETERMINATE_TYPE;

        logger.info(type.name());

        IHeadlessPrefixingStrategyWritable bittlies = ComplexHPSWritable.create();
        bittlies.addElement("sillybilly");
        bittlies.addElement("sillybilly");
        bittlies.addElement("sillybilly");
        bittlies.addElement("sillybilly");
        bittlies.addElement((String) null);
        bittlies.addElement("sillybilly");

        IHeadlessPrefixingStrategyWritable complexHPSWritable = ComplexHPSWritable.create();

        complexHPSWritable.addElement(bittlies);
        complexHPSWritable.addBytes(new byte[15]);
        complexHPSWritable.addBytes(null);
        complexHPSWritable.addElement(true);
        complexHPSWritable.addElement((byte) 5);
        complexHPSWritable.addElement((short) 5);
        complexHPSWritable.addElement((char) 5);
        complexHPSWritable.addElement((int) 5);
        complexHPSWritable.addElement((float) 5);
        complexHPSWritable.addElement((long) 5);
        complexHPSWritable.addElement((double) 5);

        List<ITypedData> hcpaData = complexHPSWritable.getAllElements();

        logger.info(0 + " " + complexHPSWritable.travelToElementIndex(0));
        logger.info(5 + " " + complexHPSWritable.travelToElementIndex(5));
        logger.info(10 + " " + complexHPSWritable.travelToElementIndex(10));
        logger.info(11 + " " + complexHPSWritable.travelToElementIndex(11));

        byte[] complexHPSBytes = complexHPSWritable.serializeToBytes();

        IHeadlessPrefixingStrategyReadable complexHPSReadable = hcpaData.get(0).getDataAsIHeadlessPrefixingStrategyReadable();

        List<ITypedData> bitties = complexHPSReadable.getAllElements();

        for (int i = 0; i < bitties.size(); i++)
        {
            ITypedData bittle = bitties.get(i);
            byte[] datas = bittle.getData();
            String bittse;

            if (datas != null)
            {
                bittse = new String(bittle.getData());
            } else
            {
                bittse = "null";
            }

            logger.info(bittse);
        }

        IHeadlessPrefixingStrategyWritable simpleHPSWritable = SimpleHPSWritable.create();

        simpleHPSWritable.addElement(bittlies);
        simpleHPSWritable.addBytes(new byte[15]);
        simpleHPSWritable.addBytes(null);
        simpleHPSWritable.addElement(true);
        simpleHPSWritable.addElement((byte) 5);
        simpleHPSWritable.addElement((short) 5);
        simpleHPSWritable.addElement((char) 5);
        simpleHPSWritable.addElement((int) 5);
        simpleHPSWritable.addElement((float) 5);
        simpleHPSWritable.addElement((long) 5);
        simpleHPSWritable.addElement((double) 5);

        List<ITypedData> hpaData = simpleHPSWritable.getAllElements();

        byte[] simpleHPSBytes = simpleHPSWritable.serializeToBytes();
        //simpleHPSBytes[0]=55;


        complexHPSReadable = DefaultHPSFactory.staticBuild(simpleHPSBytes);

        hcpaData = complexHPSReadable.getAllElements();


        IHeadlessPrefixingStrategyReadable simpleHPSReadable = DefaultHPSFactory.staticBuild(simpleHPSBytes);

        hpaData.clear();
        while (simpleHPSWritable.hasNextElement())
        {
            hpaData.add(simpleHPSWritable.getNextElement());
        }

        KeyValueHPSMapConverter<IHeadlessPrefixingStrategyReadable> mapper = new KeyValueHPSMapConverter<>(DefaultHPSFactory.staticFactory, /*This factory exposes the proper serialization logic to the KeyValueHPSMapConverter.*/
                IHeadlessPrefixingStrategyReadable.class, /*This is because Java generics suck.*/
                false); //This flag determines if the KeyValueHPSMapConverter will accept null values from a Map or elements from an HPS.

        Map<String, IHeadlessPrefixingStrategyReadable> hashMap = new HashMap<>();

        //hashMap.put(null, complexHPSReadable);
        hashMap.put("hihi", complexHPSWritable);
        hashMap.put("byebye", simpleHPSWritable); //These two HPS objects are from earlier in the testbed.
        //hashMap.put("sisi", null); //If I uncommented this you'd get an exception on serializeMap() because of that false flag earlier.

        //IHeadlessPrefixingStrategyReadable watwat = hashMap.get(null);

        int bbbbb = 0;

        byte[] mapBytes = mapper.serializeMap(hashMap);
        hashMap = mapper.deserializeMap(mapBytes, new HashMap<>());

        IHeadlessPrefixingStrategyReadable hihi = hashMap.get("hi" + "hi");
        IHeadlessPrefixingStrategyReadable byebye = hashMap.get("bye" + "bye"); //And it works!

        int a = 0;

        //String hexValues = "0125636f6d2e616d7065782e616d706572616e65742e7061636b6574732e48616e647368616b6501080000000084db6e07019d0128696233464b4e394d77647874653136647033586853734248754c4754485979585774423644673d3d0105332e302e30010201b101584141414877306e58483074437a475336706f4f7030367851776c6245344d5254764e4d39553135684551705279737a734d6c55543746536752656d316e576a5051586f7870306869597a53342b456546354a71706f513d3d010c11100100000164e68f8da500";

        String hexValues = "0125636f6d2e616d7065782e616d706572616e65742e7061636b6574732e48616e647368616b65" +
                "0105332e302e30010201b101584141414877306e58483074437a475336706f4f7030367851776c6245344d5254764e4d39553135684551705279737a734d6c55543746536752656d316e576a5051586f7870306869597a53342b456546354a71706f513d3d010c11100100000164e68f8da500";

        byte[] handshakeBytes = hexStringToByteArray(hexValues);
        //handshakeBytes[1] = 91;

        IHeadlessPrefixingStrategyReadable handshakeHPS = SimpleHPSReadable.create(handshakeBytes);

        List<ITypedData> elements = handshakeHPS.getAllElements();

        for(ITypedData dat : elements)
        {
            System.out.println(new String(dat.getData(), AmpConstants.UTF8));
            System.out.println(new BigInteger(dat.getData()));
            System.out.println();
            System.out.println();
        }

        a = 1;

        /*

        try
        {
            throw new AmpException("testestest");
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            throw new AmpException("testestest");
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            throw new AmpException("testestest");
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            throw new AmpException("testestest");
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        int a = 0;

        //todo: add in a way for amplets to swap out groups of the same group id, or add the group if there is no id collision.

        ///*

        System.out.println("100");

        AC_SingleElement testAC = AC_SingleElement.create(testGroupID, "Hi!");

        long loremClassID = 555;//ByteTools.writeLockClassID(101);

        AC_ClassInstanceIDIsIndex loremAC = AC_ClassInstanceIDIsIndex.create(loremClassID, "Lorems Group");

        for (int i = 0; i < 10; i++)
        {
            byte[] input = new byte[1];

            if (i % 5 == 0)
            {
                input = lorem1.getBytes(AmpConstants.UTF8);
            }
            if (i % 5 == 1)
            {
                input = lorem2.getBytes(AmpConstants.UTF8);
            }
            if (i % 5 == 2)
            {
                input = lorem3.getBytes(AmpConstants.UTF8);
            }
            if (i % 5 == 3)
            {
                input = lorem4.getBytes(AmpConstants.UTF8);
            }
            if (i % 5 == 4)
            {
                input = lorem5.getBytes(AmpConstants.UTF8);
            }

            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
        }

        //GroupID.disableCollisionProtection();

        //GroupID.enableCollisionProtection();

        //GroupID.enableStrongCollisionProtection();

        new GroupID(555, 935, "Haha!");

        loremAC = AC_ClassInstanceIDIsIndex.create(loremClassID, "Lorems Group 2");

        for (int i = 0; i < 10; i++)
        {
            byte[] input = new byte[1];

            if (i % 5 == 0)
            {
                input = lorem1.getBytes(AmpConstants.UTF8);
            }
            if (i % 5 == 1)
            {
                input = lorem2.getBytes(AmpConstants.UTF8);
            }
            if (i % 5 == 2)
            {
                input = lorem3.getBytes(AmpConstants.UTF8);
            }
            if (i % 5 == 3)
            {
                input = lorem4.getBytes(AmpConstants.UTF8);
            }
            if (i % 5 == 4)
            {
                input = lorem5.getBytes(AmpConstants.UTF8);
            }

            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
            loremAC.addElement(input);
            input[0]++;
        }

        System.out.println("200");

        AmpClassCollection testCC = new AmpClassCollection();

        testCC.addClass(testAC);
        testCC.addClass(loremAC);

        Amplet amp = testCC.serializeToAmplet();

        System.out.println("300");

        byte[] trailer = new byte[5];
        trailer[0] = 0;
        trailer[1] = 1;
        trailer[2] = 2;
        trailer[3] = 3;
        trailer[4] = 5;

        amp.setTrailer(trailer);


        amp = amp.repackSelf();

        //byte[] ampletSize = amp.serializeToBytes();

        System.out.println("400");

        for (int i = 0; i < 101; i++)
        {
            amp = amp.repackSelf();

            ArrayList<UnpackedGroup> loremsGroupList = amp.unpackClass(loremClassID);

            for (UnpackedGroup loremsGroup : loremsGroupList)
            {
                for (int j = 0; j < loremsGroup.getNumberOfElements() - 1; j++)
                {
                    loremsGroup.addElement(loremsGroup.getElement(0));
                    loremsGroup.removeElement(0);
                }
            }
        }

        amp = amp.repackSelf();

        System.out.println("500");

        ArrayList<UnpackedGroup> loremsGroupList = amp.unpackClass(loremClassID);

        for (UnpackedGroup loremsGroup : loremsGroupList)
        {
            System.out.println("\nLorems:\n");

            int count = loremsGroup.getNumberOfElements();
            for (int i = 0; i < count; i++)
            {
                System.out.println(new String(loremsGroup.getElement(i), AmpConstants.UTF8));
            }
        }

        String testString = amp.unpackGroup(testGroupID).getElementAsString(0);

        System.out.println("\n" + testString);
        //*/

        //XodusAmpMap xam = new XodusAmpMap("hi/hi");

        //xam.clearSafe();

        //for(int i = 0 ; i < 5000 ; i++)
        //{
        //xam.putBytes("index: " + i, ByteTools.deconstructInt(i));

        //byte[] intBytes = xam.getBytes("index: " + i);
        //System.out.println(ByteTools.buildInt(intBytes[0], intBytes[1], intBytes[2], intBytes[3]));
        //}

        //xam.close();
        //*/
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    static final String lorem1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lacinia tincidunt risus et porttitor. Morbi varius sit amet neque aliquet imperdiet. Etiam eu eros euismod, volutpat ante id, volutpat justo. Integer sit amet massa sodales nibh ultrices auctor. Duis scelerisque ante eget tristique vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut luctus luctus libero, in rutrum odio malesuada vel. Fusce auctor dolor eros, nec aliquam lectus dignissim et. Vivamus sagittis metus quis neque pretium sagittis. Proin ullamcorper est id nisi porttitor, ut malesuada lacus maximus.";
    static final String lorem2 = "Integer blandit eleifend urna. Aliquam convallis, quam vitae placerat gravida, metus nisl maximus orci, ac ultricies arcu lectus eu nulla. Quisque condimentum velit eu odio imperdiet, quis dapibus velit rhoncus. Nullam efficitur augue a lorem efficitur bibendum. Aenean eget ipsum dictum, commodo leo et, tempor mauris. Donec lobortis ligula a arcu interdum finibus. Vestibulum venenatis dolor nec mauris aliquet, vel aliquam enim venenatis. Sed varius imperdiet mi non posuere. Fusce porttitor, dolor vel cursus iaculis, nibh mi gravida mi, id mattis augue quam sit amet tellus. Proin dui nulla, cursus sit amet augue nec, auctor tincidunt justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam congue est diam, non euismod velit pretium quis. Phasellus eget varius dolor. Cras sit amet tellus sit amet massa consequat viverra. Nulla sit amet augue id lacus mattis viverra sed sed massa.";
    static final String lorem3 = "In eget pharetra eros, vel pharetra arcu. Sed condimentum massa porttitor nibh porttitor venenatis. Ut malesuada aliquam ipsum sit amet sodales. Suspendisse posuere, augue in facilisis lacinia, neque massa accumsan sapien, euismod efficitur risus risus id felis. Mauris ultricies lacinia mi, sed volutpat nunc suscipit vel. Sed hendrerit, urna porta suscipit fermentum, libero arcu faucibus velit, at blandit mauris ligula et nibh. Aliquam eu sem odio. Integer accumsan nisl sed ornare convallis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec ultricies felis felis, nec venenatis nisi rutrum quis. Ut viverra sodales est ut mattis. Fusce tempus sem et magna convallis bibendum. In tincidunt efficitur odio a pellentesque. Phasellus sollicitudin purus id finibus faucibus.";
    static final String lorem4 = "Mauris tincidunt augue sed nisi viverra vehicula. Etiam rutrum nisl vitae diam tempor, et facilisis sapien consequat. Nunc dictum vitae ex sed posuere. Praesent maximus lacus nisl, eleifend posuere urna vestibulum et. Aliquam eget ligula eget mauris lobortis rhoncus. Morbi at mi sit amet leo convallis posuere vel non risus. Nulla nunc diam, suscipit vitae libero vel, elementum placerat est. Nam tincidunt ligula nec purus congue, in rutrum sapien ultricies. Curabitur finibus quam quis enim consequat, et sagittis turpis hendrerit.";
    static final String lorem5 = "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla sed tempus sapien, blandit laoreet mi. In pellentesque hendrerit dui eget efficitur. Suspendisse facilisis quam vitae dignissim sollicitudin. Duis euismod turpis vel fermentum maximus. Nam non nisl eu augue facilisis placerat. Aenean blandit, dolor eget luctus sodales, tortor lorem tincidunt magna, sed vulputate neque metus ac lacus. Nulla convallis sit amet odio at pharetra.";
}
