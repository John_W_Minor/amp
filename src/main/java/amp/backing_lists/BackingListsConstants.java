package amp.backing_lists;

class BackingListsConstants
{
    static final int NATURAL = 0;
    static final int KILOBYTE = 1000;
    static final int TEN_KILOBYTES = KILOBYTE * 10;
    static final int FIFTY_KILOBYTES = TEN_KILOBYTES * 5;
    static final int ONE_HUNDRED_KILOBYTES = TEN_KILOBYTES * 2;
    static final int FIVE_HUNDRED_KILOBYTES = ONE_HUNDRED_KILOBYTES * 5;
    static final int MEGABYTE = FIVE_HUNDRED_KILOBYTES * 2;
    static final int TEN_MEGABYTES = MEGABYTE * 10;
    static final int FIFTY_MEGABYTES = TEN_MEGABYTES * 5;
}
