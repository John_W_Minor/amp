package amp.backing_lists;

public enum GrowModeEnum
{
    NATURAL(BackingListsConstants.NATURAL),
    KILOBYTE(BackingListsConstants.KILOBYTE),
    TEN_KILOBYTES(BackingListsConstants.TEN_KILOBYTES),
    FIFTY_KILOBYTES(BackingListsConstants.FIFTY_KILOBYTES),
    ONE_HUNDRED_KILOBYTES(BackingListsConstants.ONE_HUNDRED_KILOBYTES),
    FIVE_HUNDRED_KILOBYTES(BackingListsConstants.FIVE_HUNDRED_KILOBYTES),
    MEGABYTE(BackingListsConstants.MEGABYTE),
    TEN_MEGABYTES(BackingListsConstants.TEN_MEGABYTES),
    FIFTY_MEGABYTES(BackingListsConstants.FIFTY_MEGABYTES);

    private final int growth;

    GrowModeEnum(int _growth)
    {
        growth = _growth;
    }

    public int getGrowth()
    {
        return growth;
    }
}
