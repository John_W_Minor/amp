package amp.backing_lists;

import amp.AmpConstants;
import amp.exceptions.AmpBackingListIndexOutOfBoundsException;
import amp.exceptions.errors.AmpMemoryAllocationError;

public class ByteList
{
    private static final int DEFAULT_CAPACITY = 10;

    private static final byte[] UNINFLATED = {};

    private byte[] bytes;

    private int size = 0;

    private GrowModeEnum growMode = GrowModeEnum.NATURAL;

    public ByteList()
    {
        bytes = UNINFLATED;
    }

    public ByteList(int _initialCapacity)
    {
        if (_initialCapacity > 0)
        {
            bytes = new byte[_initialCapacity];
        } else
        {
            //default capacity of 10
            bytes = UNINFLATED;
        }
    }

    public ByteList(byte[] _bytes)
    {
        if (_bytes == null || _bytes.length == 0)
        {
            bytes = UNINFLATED;
            return;
        }

        bytes = _bytes.clone();

        size = _bytes.length;
    }

    //region capacity
    public void setGrowMode(GrowModeEnum _growMode)
    {
        growMode = _growMode;
    }

    public GrowModeEnum getGrowMode()
    {
        return growMode;
    }

    public void trimToSize()
    {
        if (size < bytes.length)
        {
            if (size == 0)
            {
                bytes = UNINFLATED;
                return;
            }

            reallocateBuffer(size);
        }
    }

    public void ensureCapacity(int _minCapacity)
    {
        if ((_minCapacity <= bytes.length) || (bytes == UNINFLATED && _minCapacity < DEFAULT_CAPACITY))
        {
            return;
        }

        ensureExplicitCapacity(_minCapacity);
    }

    private void ensureCapacityInternal(int _minCapacity)
    {
        if (bytes == UNINFLATED)
        {
            _minCapacity = Math.max(_minCapacity, DEFAULT_CAPACITY);
        }

        ensureExplicitCapacity(_minCapacity);
    }

    private void ensureExplicitCapacity(int _minCapacity)
    {
        if (_minCapacity - bytes.length > 0)
        {
            grow(_minCapacity);
        }
    }

    private void grow(int _minCapacity)
    {
        int oldCapacity = bytes.length;
        int newCapacity;

        if (growMode == GrowModeEnum.NATURAL)
        {
            newCapacity = oldCapacity + (oldCapacity >> 1);
        } else
        {
            newCapacity = oldCapacity + growMode.getGrowth();
        }

        if (newCapacity - _minCapacity < 0)
        {
            newCapacity = _minCapacity;
        }

        reallocateBuffer(newCapacity);
    }

    private void reallocateBuffer(int _capacity)
    {
        if (_capacity < 0 || _capacity > AmpConstants.BYTE_ARRAY_MAX_BYTE_COUNT)
        {
            throw new OutOfMemoryError();
        }

        if (_capacity < size)
        {
            throw new AmpMemoryAllocationError();
        }

        byte[] tempArray = new byte[_capacity];

        System.arraycopy(bytes, 0, tempArray, 0, size);

        bytes = tempArray;
    }
    //endregion

    public int size()
    {
        return size;
    }

    public boolean isEmpty()
    {
        return size == 0;
    }

    public ByteList copy()
    {
        return new ByteList(toArray());
    }

    public byte[] toArray()
    {
        byte[] tempArray = new byte[size];

        System.arraycopy(bytes, 0, tempArray, 0, size);

        return tempArray;
    }

    //region positional access
    public byte get(int _index)
    {
        rangeCheck(_index);

        return bytes[_index];
    }

    public byte[] getNBytes(int _startIndex, int _n)
    {
        rangeCheck(_startIndex);
        rangeCheck(_startIndex + _n - 1);

        byte[] tempArray = new byte[_n];

        System.arraycopy(bytes, _startIndex, tempArray, 0, _n);

        return tempArray;
    }

    public void set(int _index, byte _value)
    {
        rangeCheck(_index);

        bytes[_index] = _value;
    }

    public boolean add(byte _value)
    {
        ensureCapacityInternal(size + 1);
        bytes[size] = _value;
        size++;
        return true;
    }

    public boolean addAll(byte[] _values)
    {
        if (_values != null && _values.length != 0)
        {
            int numNew = _values.length;
            ensureCapacityInternal(size + numNew);
            System.arraycopy(_values, 0, bytes, size, numNew);
            size += numNew;
            return true;
        }
        return false;
    }

    public void remove(int _index)
    {
        rangeCheck(_index);

        int numMoved = size - _index - 1;
        if (numMoved > 0)
        {
            System.arraycopy(bytes, _index + 1, bytes, _index, numMoved);
        }
        size--;
    }

    public void removeNBytes(int _startIndex, int _n)
    {
        rangeCheck(_startIndex);
        rangeCheck(_startIndex + _n - 1);

        int numMoved = size - _startIndex - _n;
        if (numMoved > 0)
        {
            System.arraycopy(bytes, _startIndex + _n, bytes, _startIndex, numMoved);
        }
        size -= _n;
    }

    public void clear()
    {
        size = 0;
    }

    public void clearToGC()
    {
        bytes = UNINFLATED;
        size = 0;
    }

    private void rangeCheck(int _index)
    {
        if (_index >= size)
        {
            throw new AmpBackingListIndexOutOfBoundsException(_index, size);
        }
    }
    //endregion
}
