package amp.exceptions;

import amp.AmpConstants;

public class UnsupportedElementSizeException extends AmpException
{
    public UnsupportedElementSizeException(int _elementLength)
    {
        super(generateMessage(_elementLength));
    }

    private static String generateMessage(int _elementLength)
    {
        if (_elementLength <= 0)
        {
            return "Element byte footprint must not be smaller than 1.";
        }

        if (_elementLength >= AmpConstants.ELEMENT_MAX_BYTE_FOOTPRINT)
        {
            return "Element byte footprint must not equal or exceed " + AmpConstants.ELEMENT_MAX_BYTE_FOOTPRINT + ".";
        }

        return null;
    }
}
