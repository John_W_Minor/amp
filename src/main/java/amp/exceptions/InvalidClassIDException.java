package amp.exceptions;

import amp.AmpConstants;

public class InvalidClassIDException extends AmpException
{
    public InvalidClassIDException(long _classID)
    {
        super(generateMessage(_classID));
    }

    private static String generateMessage(long _classID)
    {
        if (_classID < 0)
        {
            return "Class ID must be greater than -1.";
        }

        if (_classID >= AmpConstants.CLASS_ID_MAX_VALUE)
        {
            return "Class ID must be smaller than " + AmpConstants.CLASS_ID_MAX_VALUE + ".";
        }

        return null;
    }
}
