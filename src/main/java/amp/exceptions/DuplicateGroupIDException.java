package amp.exceptions;

import amp.group_ids.GroupID;

public class DuplicateGroupIDException extends AmpException
{
    public DuplicateGroupIDException(GroupID _collidingGroupID, GroupID _registeredGroupID)
    {
        super(generateMessage(_collidingGroupID, _registeredGroupID));
    }

    private static String generateMessage(GroupID _collidingGroupID, GroupID _registeredGroupID)
    {
        return "\nThere are colliding Amp Group IDs.\nColliding Class ID is: " + _collidingGroupID.getClassID() +
                "\nColliding Class Instance ID is: " + _collidingGroupID.getClassInstanceID() +
                "\nThe name of this Group ID is: " + _collidingGroupID.getName() +
                "\nThe name of the other Group ID is: " + _registeredGroupID.getName();
    }
}
