package amp.exceptions;

public class NoCompatibleVersionedFactoryAvailableException extends AmpException
{
    public NoCompatibleVersionedFactoryAvailableException()
    {
        super("No compatible versioned factories were available to the DefaultHPSFactory.");
    }
}
