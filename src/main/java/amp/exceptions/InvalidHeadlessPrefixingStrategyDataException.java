package amp.exceptions;

public class InvalidHeadlessPrefixingStrategyDataException extends AmpException
{
    public InvalidHeadlessPrefixingStrategyDataException(String _type)
    {
        super(generateMessage(_type));
    }

    public static String generateMessage(String _type)
    {
        return "Input bytes cannot be read by " + _type + ".";
    }
}
