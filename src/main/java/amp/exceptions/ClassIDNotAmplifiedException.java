package amp.exceptions;

public class ClassIDNotAmplifiedException extends AmpException
{
    public ClassIDNotAmplifiedException()
    {
        super("Failed to store Amplet as element because the class ID was not amplified.");
    }
}
