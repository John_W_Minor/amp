package amp.exceptions.errors;

public class ConflictingHPSBuilderVersionsError extends AmpError
{
    public ConflictingHPSBuilderVersionsError(int _version)
    {
        super("Version number " + _version + " was already used in DefaultHPSFactory. This is a critical failure of serialization design that can and will create invalid state.");
    }
}
