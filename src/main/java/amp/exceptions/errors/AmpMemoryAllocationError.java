package amp.exceptions.errors;

public class AmpMemoryAllocationError extends AmpError
{
    public AmpMemoryAllocationError()
    {
        super("Unable to allocate requested memory.");
    }
}
