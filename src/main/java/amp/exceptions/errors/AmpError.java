package amp.exceptions.errors;

import amp.AmpLogging;
import logging.IAmpexLogger;

public class AmpError extends Error
{
    public AmpError()
    {
    }

    public AmpError(String _message)
    {
        super(_message);
    }

    @Override
    public void printStackTrace()
    {
        IAmpexLogger logger = AmpLogging.getLogger();
        if (logger != null)
        {
            logger.error("", this);
        }
    }
}
