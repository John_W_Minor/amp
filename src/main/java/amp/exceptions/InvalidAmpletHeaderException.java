package amp.exceptions;

public class InvalidAmpletHeaderException extends AmpException
{
    public InvalidAmpletHeaderException(String _message)
    {
        super(_message);
    }
}
