package amp.exceptions;

public class InvalidGroupHeaderException extends AmpException
{
    public InvalidGroupHeaderException(String _message)
    {
        super(_message);
    }
}
