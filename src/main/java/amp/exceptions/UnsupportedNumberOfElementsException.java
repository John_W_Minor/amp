package amp.exceptions;

import amp.AmpConstants;

public class UnsupportedNumberOfElementsException extends AmpException
{
    public UnsupportedNumberOfElementsException(int _elementCount)
    {
        super(generateMessage(_elementCount));
    }

    private static String generateMessage(int _elementCount)
    {
        if (_elementCount <= 0)
        {
            return "There must be at least one group.";
        }

        if (_elementCount >= AmpConstants.ELEMENT_COUNT_MAX)
        {
            return "The number of groups must not equal of exceed " + AmpConstants.ELEMENT_COUNT_MAX + ".";
        }

        return null;
    }
}
