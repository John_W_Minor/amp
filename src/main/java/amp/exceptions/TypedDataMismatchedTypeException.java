package amp.exceptions;

public class TypedDataMismatchedTypeException extends AmpException
{
    public TypedDataMismatchedTypeException(String _type)
    {
        super(generateMessage(_type));
    }

    private static String generateMessage(String _type)
    {
        return "Given data cannot ever be " + _type + ".";
    }
}
