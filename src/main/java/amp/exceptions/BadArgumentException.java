package amp.exceptions;

public class BadArgumentException extends AmpException
{
    public BadArgumentException(String _message)
    {
        super(_message);
    }
}
