package amp.exceptions;

import amp.AmpConstants;

public class UnsupportedGroupCountException extends AmpException
{
    public UnsupportedGroupCountException(int _groupCount)
    {
        super(generateMessage(_groupCount));
    }

    private static String generateMessage(int _groupCount)
    {
        if (_groupCount <= 0)
        {
            return "There must be at least one group.";
        }

        if (_groupCount >= AmpConstants.GROUPS_MAX_COUNT)
        {
            return "The number of groups must not equal of exceed " + AmpConstants.GROUPS_MAX_COUNT + ".";
        }

        return null;
    }
}
