package amp.exceptions;

public class YouDidNotKnowWhatYouWereDoingException extends AmpException
{
    public YouDidNotKnowWhatYouWereDoingException()
    {
        super("Don't use unsafe methods if you don't know what you're doing.");
    }
}
