package amp.exceptions;

import amp.AmpConstants;
import amp.ByteTools;

public class UnsupportedAmpVersionException extends AmpException
{
    public UnsupportedAmpVersionException(int _version)
    {
        super(generateMessage(_version));
    }

    private static String generateMessage(int _version)
    {
        short[] versionArray = ByteTools.deconcatenateShorts(_version);
        String ampletVersion = versionArray[0] + "." + versionArray[1];
        return "\nAmp Standard version mismatch detected during deserialization.\nSerialized Amplet used Amp Standard " + ampletVersion + " and this library uses Amp Standard " + AmpConstants.VERSIONSTRING + ".";
    }
}
