package amp.exceptions;

public class KeyWithoutValueException extends AmpException
{
    public KeyWithoutValueException()
    {
        super("Given KeyValueHPS data has a key without a corresponding value.");
    }
}
