package amp.exceptions;

import amp.typed_data.IDataType;

public class UnexpectedTypeException extends AmpException
{
    public UnexpectedTypeException(IDataType _type, String _context)
    {
        super(generateMessage(_type, _context));
    }

    public static String generateMessage(IDataType _type, String _context)
    {
        return _type.name() + " was not an expected type in " + _context + ".";
    }
}
