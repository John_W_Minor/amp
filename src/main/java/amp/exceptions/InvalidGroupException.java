package amp.exceptions;

public class InvalidGroupException extends AmpException
{
    public InvalidGroupException(String _message)
    {
        super(_message);
    }
}
