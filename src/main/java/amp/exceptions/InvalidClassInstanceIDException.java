package amp.exceptions;

import amp.AmpConstants;

public class InvalidClassInstanceIDException extends AmpException
{
    public InvalidClassInstanceIDException(long _classInstanceID)
    {
        super(generateMessage(_classInstanceID));
    }

    private static String generateMessage(long _classInstanceID)
    {
        if (_classInstanceID < 0)
        {
            return "Class Instance ID must be greater than -1.";
        }

        if (_classInstanceID >= AmpConstants.CLASS_INSTANCE_ID_MAX_VALUE)
        {
            return "Class Instance ID must be smaller than " + AmpConstants.CLASS_INSTANCE_ID_MAX_VALUE + ".";
        }

        return null;
    }
}
