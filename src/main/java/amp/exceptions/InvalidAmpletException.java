package amp.exceptions;

public class InvalidAmpletException extends AmpException
{
    public InvalidAmpletException(String _message)
    {
        super(_message);
    }

    public InvalidAmpletException(boolean _areGroupsValid, boolean _doRepeatGroupIDsExist)
    {
        this(_areGroupsValid, _doRepeatGroupIDsExist, true);
    }

    public InvalidAmpletException(boolean _areGroupsValid, boolean _doRepeatGroupIDsExist, boolean _areThereSufficientBytes)
    {
        super(generateMessage(_areGroupsValid, _doRepeatGroupIDsExist, _areThereSufficientBytes));
    }

    private static String generateMessage(boolean _areGroupsValid, boolean _doRepeatGroupIDsExist, boolean _areThereSufficientBytes)
    {
        String message = "";

        if (!_areGroupsValid)
        {
            message += "Groups are not valid. ";
        }

        if (_doRepeatGroupIDsExist)
        {
            message += "Repeat group IDs exist. ";
        }

        if (!_areThereSufficientBytes)
        {
            message += "Byte count does not match header records.";
        }

        return message;
    }
}
