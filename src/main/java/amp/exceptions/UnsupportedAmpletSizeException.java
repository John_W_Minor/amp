package amp.exceptions;

import amp.AmpConstants;

public class UnsupportedAmpletSizeException extends AmpException
{
    public UnsupportedAmpletSizeException(int _ampletSize)
    {
        super(generateMessage(_ampletSize));
    }

    private static String generateMessage(int _ampletSize)
    {
        if (_ampletSize <= 0)
        {
            return "Amplet must not be smaller than 1 bytes.";
        }

        if (_ampletSize >= AmpConstants.GROUPS_MAX_COUNT)
        {
            return "Amplet must not equal or exceed " + AmpConstants.BYTE_ARRAY_MAX_BYTE_COUNT + " bytes.";
        }

        return null;
    }
}
