package amp.exceptions;

import amp.AmpLogging;
import logging.IAmpexLogger;

public class AmpException extends RuntimeException
{
    private static boolean exceptionsEnabled = true;

    public AmpException()
    {

    }

    public AmpException(String _message)
    {
        super(_message);
    }

    @Override
    public void printStackTrace()
    {
        IAmpexLogger logger = AmpLogging.getLogger();
        if (logger != null)
        {
            logger.error("", this);
        }
    }

    public static void enableExceptions()
    {
        exceptionsEnabled = true;
    }

    public static void disableExceptions()
    {
        exceptionsEnabled = false;
    }

    public static boolean areExceptionsEnabled()
    {
        return exceptionsEnabled;
    }
}
