package amp.exceptions;

public class AmpBackingListIndexOutOfBoundsException extends AmpException
{
    public AmpBackingListIndexOutOfBoundsException(int _index, int _size)
    {
        super(generateMessage(_index, _size));
    }

    private static String generateMessage(int _index, int _size)
    {
        return "Index: " + _index + " Size: " + _size;
    }
}
