package amp.group_primitives;

import amp.ByteTools;
import amp.AmpConstants;
import amp.group_ids.GroupID;
import amp.exceptions.*;

public class GroupHeader
{
    //region Member Fields
    private GroupID groupID = null;
    private long classID = 0;
    private long classInstanceID = 0;

    private int byteCountPerElement = 0;
    private int elementCount = 0;

    private int byteCountTotalAllElements = 0;

    private int byteArrayStartPosition = 0;
    private int byteArrayEndPosition = 0;

    private boolean markedForDeletion = false;

    private boolean validGroup = false;
    //endregion

    //region Constructors
    private GroupHeader(GroupID _groupID, int _byteCountPerElement)
    {
        if (_groupID != null)
        {
            groupID = _groupID;
            classID = groupID.getClassID();
            classInstanceID = groupID.getClassInstanceID();
            boolean validClassID = classID > -1 && classID < AmpConstants.CLASS_ID_MAX_VALUE;
            boolean validClassInstanceID = classInstanceID > -1 && classInstanceID < AmpConstants.CLASS_ID_MAX_VALUE;
            boolean validByteCountPerElement = _byteCountPerElement > 0 && _byteCountPerElement < AmpConstants.ELEMENT_MAX_BYTE_FOOTPRINT;

            if (validClassID && validClassInstanceID && validByteCountPerElement)
            {
                byteCountPerElement = _byteCountPerElement;

                validGroup = true;
            } else if (AmpException.areExceptionsEnabled())
            {
                if (!validClassID)
                {
                    throw new InvalidClassIDException(classID);
                }

                if (!validClassInstanceID)
                {
                    throw new InvalidClassInstanceIDException(classInstanceID);
                }

                if (!validByteCountPerElement)
                {
                    throw new UnsupportedElementSizeException(_byteCountPerElement);
                }
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into GroupHeader().");
        }
    }

    private GroupHeader(GroupID _groupID, int _byteCountPerElement, int _elementCount, int _byteArrayStartPosition)
    {
        if (_groupID != null)
        {
            groupID = _groupID;
            classID = groupID.getClassID();
            classInstanceID = groupID.getClassInstanceID();

            boolean validClassID = classID > -1 && classID < AmpConstants.CLASS_ID_MAX_VALUE;
            boolean validClassInstanceID = classInstanceID > -1 && classInstanceID < AmpConstants.CLASS_ID_MAX_VALUE;
            boolean validByteCountPerElement = _byteCountPerElement > 0 && _byteCountPerElement < AmpConstants.ELEMENT_MAX_BYTE_FOOTPRINT;
            boolean validElementCount = _elementCount > 0 && _elementCount < AmpConstants.ELEMENT_COUNT_MAX;

            if (validClassID && validClassInstanceID && validByteCountPerElement && validElementCount)
            {
                byteCountPerElement = _byteCountPerElement;

                elementCount = _elementCount;

                byteCountTotalAllElements = elementCount * byteCountPerElement;

                byteArrayStartPosition = _byteArrayStartPosition;

                byteArrayEndPosition = byteArrayStartPosition + byteCountTotalAllElements;

                validGroup = true;
            } else if (AmpException.areExceptionsEnabled())
            {
                if (!validClassID)
                {
                    throw new InvalidClassIDException(classID);
                }

                if (!validClassInstanceID)
                {
                    throw new InvalidClassInstanceIDException(classInstanceID);
                }

                if (!validByteCountPerElement)
                {
                    throw new UnsupportedElementSizeException(_byteCountPerElement);
                }

                if (!validElementCount)
                {
                    throw new UnsupportedNumberOfElementsException(_elementCount);
                }
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into GroupHeader().");
        }
    }

    public static GroupHeader create(GroupID _groupID, int _byteCountPerElement)
    {
        GroupHeader tempGroupHeader = new GroupHeader(_groupID, _byteCountPerElement);
        if (tempGroupHeader.isValidGroup())
        {
            return tempGroupHeader;
        }
        return null;
    }

    public static GroupHeader create(GroupID _groupID, int _byteCountPerElement, int _elementCount, int _byteArrayStartPosition)
    {
        GroupHeader tempGroupHeader = new GroupHeader(_groupID, _byteCountPerElement, _elementCount, _byteArrayStartPosition);
        if (tempGroupHeader.isValidGroup())
        {
            return tempGroupHeader;
        }
        return null;
    }
    //endregion

    //region Setters
    void setElementCount(int _elementCount)
    {
        if (validGroup)
        {
            if (_elementCount < 0)
            {
                _elementCount = 0;
            }
            elementCount = _elementCount;
            byteCountTotalAllElements = elementCount * byteCountPerElement;
            byteArrayEndPosition = byteArrayStartPosition + byteCountTotalAllElements;
        }
    }
    //endregion

    //region Getters.
    public long getClassID()
    {
        return classID;
    }

    public long getClassInstanceID()
    {
        return classInstanceID;
    }

    public GroupID getGroupID()
    {
        return groupID;
    }


    public int getByteCountPerElement()
    {
        return byteCountPerElement;
    }

    public int getElementCount()
    {
        return elementCount;
    }


    public int getByteCountTotalAllElements()
    {
        return byteCountTotalAllElements;
    }


    public int getByteArrayStartPosition()
    {
        return byteArrayStartPosition;
    }

    public int getByteArrayEndPosition()
    {
        return byteArrayEndPosition;
    }


    public boolean isMarkedForDeletion()
    {
        return markedForDeletion;
    }


    public boolean isValidGroup()
    {
        return validGroup;
    }


    public boolean isWriteLocked()
    {
        return ByteTools.isClassIDWriteLocked(classID);
    }

    public boolean isAmpletCompatible()
    {
        return ByteTools.isClassIDAmplified(classID);
    }
    //endregion

    public void markForDeletion()
    {
        markedForDeletion = !isWriteLocked();
    }
}
