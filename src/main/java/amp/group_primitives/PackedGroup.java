package amp.group_primitives;

import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;

public class PackedGroup
{
    //region Member Fields
    private byte[] packedHeaderBytes = null;
    private byte[] packedElementsBytes = null;

    private int byteCountTotalAllElements = 0;

    private boolean validPackedGroup = false;
    //endregion

    //region Constructors
    private PackedGroup(byte[] _packedHeaderBytes, byte[] _packedElementBytes)
    {
        boolean packedHeaderBytesNotNull = _packedHeaderBytes != null;
        boolean packedElementBytesNotNull = _packedElementBytes != null;
        if (packedHeaderBytesNotNull && packedElementBytesNotNull)
        {
            packedHeaderBytes = _packedHeaderBytes.clone();
            packedElementsBytes = _packedElementBytes.clone();
            byteCountTotalAllElements = packedElementsBytes.length;
            validPackedGroup = true;
        } else if (AmpException.areExceptionsEnabled())
        {
            if (packedHeaderBytesNotNull)
            {
                throw new BadArgumentException("Null _packedHeaderBytes argument was passed into PackedGroup().");
            }

            if (packedElementBytesNotNull)
            {
                throw new BadArgumentException("Null _packedElementBytes argument was passed into PackedGroup().");
            }
        }
    }

    public static PackedGroup create(byte[] _packedHeaderBytes, byte[] _packedElementBytes)
    {
        PackedGroup tempPackedGroup = new PackedGroup(_packedHeaderBytes, _packedElementBytes);
        if (tempPackedGroup.isValidPackedGroup())
        {
            return tempPackedGroup;
        }
        return null;
    }
    //endregion

    //region Getters
    public byte[] getPackedHeaderBytes()
    {
        return packedHeaderBytes.clone();
    }

    public byte[] getPackedElementsBytes()
    {
        return packedElementsBytes.clone();
    }

    public int getByteCountTotalAllElements()
    {
        return byteCountTotalAllElements;
    }

    public boolean isValidPackedGroup()
    {
        return validPackedGroup;
    }
    //endregion
}
