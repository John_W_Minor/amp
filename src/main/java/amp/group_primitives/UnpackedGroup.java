package amp.group_primitives;

import amp.AmpConstants;
import amp.ByteTools;
import amp.Amplet;
import amp.HeadlessAmplet;
import amp.group_ids.GroupID;
import amp.headless_amplet.*;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;
import amp.exceptions.*;
import amp.group_primitives.tools.GroupTools;


import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;

public class UnpackedGroup implements IAmpAmpletSerializable
{
    //region Member Fields
    private GroupHeader header = null;

    private boolean changed = false;

    private boolean sealed = false;

    private ArrayList<byte[]> unpackedElements = null;

    private boolean validGroup = false;
    //endregion

    //region Constructors
    private UnpackedGroup(GroupID _groupID, int _byteCountPerElement)
    {
        if (_groupID != null)
        {
            long classID = _groupID.getClassID();
            long classInstanceID = _groupID.getClassInstanceID();

            boolean validClassID = classID > -1 && classID < AmpConstants.CLASS_ID_MAX_VALUE;
            boolean validClassInstanceID = classInstanceID > -1 && classInstanceID < AmpConstants.CLASS_ID_MAX_VALUE;
            boolean validByteCountPerElement = _byteCountPerElement > 0 && _byteCountPerElement < AmpConstants.ELEMENT_MAX_BYTE_FOOTPRINT;

            if (validClassID && validClassInstanceID && validByteCountPerElement)
            {
                unpackedElements = new ArrayList<>();

                changed = true;

                header = GroupHeader.create(_groupID, _byteCountPerElement);
                if (header != null)
                {
                    validGroup = header.isValidGroup();

                    if (!validGroup && AmpException.areExceptionsEnabled())
                    {
                        throw new InvalidGroupHeaderException("Invalid header passed into UnpackedGroup().");
                    }
                } else if (AmpException.areExceptionsEnabled())
                {
                    throw new InvalidGroupHeaderException("Failed to build group header.");
                }

            } else if (AmpException.areExceptionsEnabled())
            {
                if (!validClassID)
                {
                    throw new InvalidClassIDException(classID);
                }

                if (!validClassInstanceID)
                {
                    throw new InvalidClassInstanceIDException(classInstanceID);
                }

                if (!validByteCountPerElement)
                {
                    throw new UnsupportedElementSizeException(_byteCountPerElement);
                }
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into UnpackedGroup().");
        }
    }

    private UnpackedGroup(GroupHeader _header)
    {
        if (_header != null)
        {
            unpackedElements = new ArrayList<>();

            changed = true;

            header = _header;
            validGroup = header.isValidGroup();

            if (!validGroup && AmpException.areExceptionsEnabled())
            {
                throw new InvalidGroupHeaderException("Invalid header passed into UnpackedGroup().");
            }

        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _header argument was passed into UnpackedGroup().");
        }
    }

    private UnpackedGroup(GroupHeader _header, byte[] _bytes)
    {
        boolean headerNotNull = _header != null;
        boolean bytesNotNull = _bytes != null;

        if (headerNotNull && bytesNotNull)
        {
            sealed = true;

            header = _header;
            validGroup = header.isValidGroup();

            if (validGroup)
            {
                unpackedElements = GroupTools.unpackElementsFromByteArray(_header, _bytes);

                if (unpackedElements == null)
                {
                    validGroup = false;

                    throw new ByteArrayOperationFailureException();
                }

            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidGroupHeaderException("Invalid header passed into UnpackedGroup().");
            }

        } else if (AmpException.areExceptionsEnabled())
        {
            if (headerNotNull)
            {
                throw new BadArgumentException("Null _header argument was passed into UnpackedGroup().");
            }

            if (bytesNotNull)
            {
                throw new BadArgumentException("Null _bytes argument was passed into UnpackedGroup().");
            }
        }
    }


    public static UnpackedGroup create(GroupID _groupID, int _byteCountPerElement)
    {
        UnpackedGroup tempUnpackedGroup = new UnpackedGroup(_groupID, _byteCountPerElement);
        if (tempUnpackedGroup.isValidGroup())
        {
            return tempUnpackedGroup;
        }
        return null;
    }

    public static UnpackedGroup create(GroupHeader _header)
    {
        UnpackedGroup tempUnpackedGroup = new UnpackedGroup(_header);
        if (tempUnpackedGroup.isValidGroup())
        {
            return tempUnpackedGroup;
        }
        return null;
    }

    public static UnpackedGroup create(GroupHeader _header, byte[] _bytes)
    {
        UnpackedGroup tempUnpackedGroup = new UnpackedGroup(_header, _bytes);
        if (tempUnpackedGroup.isValidGroup())
        {
            return tempUnpackedGroup;
        }
        return null;
    }
    //endregion

    //region Normal Getters.
    public GroupHeader getHeader()
    {
        return header;
    }

    public boolean isChanged()
    {
        return changed;
    }

    public boolean isValidGroup()
    {
        return validGroup;
    }
    //endregion

    //region Group Getters
    public int getNumberOfElements()
    {
        if (validGroup)
        {
            return unpackedElements.size();
        }
        return 0;
    }

    public byte[] getElement(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1))
        {
            return unpackedElements.get(_index).clone();
        }
        return null;
    }

    public Integer getElementAsUnsignedByte(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.BYTE_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildUnsignedByte(bytes[0]);
        }
        return null;
    }

    public Byte getElementAsByte(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.BYTE_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return bytes[0];
        }
        return null;
    }

    public Boolean getElementAsBoolean(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.BOOLEAN_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildBoolean(bytes[0]);
        }
        return null;
    }

    public Short getElementAsShort(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.SHORT_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildShort(bytes[0], bytes[1]);
        }
        return null;
    }

    public Character getElementAsChar(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.CHAR_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildChar(bytes[0], bytes[1]);
        }
        return null;
    }

    public Long getElementAsUnsignedInt(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.INT_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildUnsignedInt(bytes[0], bytes[1], bytes[2], bytes[3]);
        }
        return null;
    }

    public Integer getElementAsInt(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.INT_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildInt(bytes[0], bytes[1], bytes[2], bytes[3]);
        }
        return null;
    }

    public Float getElementAsFloat(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.FLOAT_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildFloat(bytes[0], bytes[1], bytes[2], bytes[3]);
        }
        return null;
    }

    public Long getElementAsLong(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.LONG_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildLong(bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]);
        }
        return null;
    }

    public Double getElementAsDouble(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1) && getHeader().getByteCountPerElement() == AmpConstants.DOUBLE_BYTE_FOOTPRINT)
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildDouble(bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]);
        }
        return null;
    }

    public String getElementAsString(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1))
        {
            byte[] bytes = unpackedElements.get(_index);
            return new String(bytes, AmpConstants.UTF8);
        }
        return null;
    }

    public String getElementAsString(int _index, String _charsetName) throws UnsupportedEncodingException
    {
        if (validGroup && _index <= (unpackedElements.size() - 1))
        {
            byte[] bytes = unpackedElements.get(_index);
            return new String(bytes, _charsetName);
        }
        return null;
    }

    public String getElementAsHexString(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1))
        {
            byte[] bytes = unpackedElements.get(_index);
            return ByteTools.buildHexString(bytes);
        }
        return null;
    }

    public BigInteger getElementAsBigInteger(int _index)
    {
        if (validGroup && _index <= (unpackedElements.size() - 1))
        {
            byte[] bytes = unpackedElements.get(_index);
            return new BigInteger(bytes);
        }
        return null;
    }

    @Deprecated
    public HeadlessAmplet getElementAsHeadlessAmplet(int _index)
    {
        if (validGroup)
        {
            return HeadlessAmplet.create(unpackedElements.get(_index));
        }
        return null;
    }

    public IHeadlessAmpletTwinReadable getElementAsIHeadlessAmpletTwinReadable(int _index)
    {
        if (validGroup)
        {
            return HeadlessAmpletTwinReadable.create(unpackedElements.get(_index));
        }
        return null;
    }

    public IHeadlessAmpletWritable getElementAsIHeadlessAmpletWritable(int _index)
    {
        if (validGroup)
        {
            return HeadlessAmpletWritable.create(unpackedElements.get(_index));
        }
        return null;
    }

    public Amplet getElementAsAmplet(int _index)
    {
        if (validGroup && header.isAmpletCompatible())
        {
            return Amplet.create(unpackedElements.get(_index));
        }
        return null;
    }
    //endregion

    //region Group Adders
    public boolean addElement(byte[] _element)
    {
        //minus 1 because it can't equal AmpConstants.Element_Count_Max number of elements
        if (validGroup && !isWriteLockedAndSealed() && unpackedElements.size() < (AmpConstants.ELEMENT_COUNT_MAX - 1))
        {
            if (_element.length == header.getByteCountPerElement())
            {
                unpackedElements.add(_element.clone());
                header.setElementCount(unpackedElements.size());
                changed = true;
                return true;
            }
        }
        return false;
    }

    public boolean addElement(byte _element)
    {
        byte[] bytes = {_element};

        return addElement(bytes);
    }

    public boolean addElement(boolean _element)
    {
        byte[] bytes = ByteTools.deconstructBoolean(_element);

        return addElement(bytes);
    }

    public boolean addElement(short _element)
    {
        byte[] bytes = ByteTools.deconstructShort(_element);

        return addElement(bytes);
    }

    public boolean addElement(char _element)
    {
        byte[] bytes = ByteTools.deconstructChar(_element);

        return addElement(bytes);
    }

    public boolean addElement(int _element)
    {
        byte[] bytes = ByteTools.deconstructInt(_element);

        return addElement(bytes);
    }

    public boolean addElement(float _element)
    {
        byte[] bytes = ByteTools.deconstructFloat(_element);

        return addElement(bytes);
    }

    public boolean addElement(long _element)
    {
        byte[] bytes = ByteTools.deconstructLong(_element);

        return addElement(bytes);
    }

    public boolean addElement(double _element)
    {
        byte[] bytes = ByteTools.deconstructDouble(_element);

        return addElement(bytes);
    }

    public boolean addElement(String _element)
    {
        return addElement(_element.getBytes(AmpConstants.UTF8));
    }

    public boolean addElement(String _element, String _charsetName) throws UnsupportedEncodingException
    {
        return addElement(_element.getBytes(_charsetName));
    }

    public boolean addElement(BigInteger _element)
    {
        return addElement(_element.toByteArray());
    }

    public boolean addElement(IAmpByteSerializable _element)
    {
        return addElement(_element.serializeToBytes());
    }

    public boolean addElement(IAmpAmpletSerializable _element)
    {
        if (header.isAmpletCompatible())
        {
            Amplet tempAmplet = _element.serializeToAmplet();
            if (tempAmplet != null)
            {
                return addElement(tempAmplet.serializeToBytes());
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidAmpletException("Failed to create amplet.");
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new ClassIDNotAmplifiedException();
        }
        return false;
    }
    //endregion

    //region Element Deletion
    public boolean removeElement(int _index)
    {
        if (validGroup && !isWriteLockedAndSealed() && _index <= (unpackedElements.size() - 1))
        {
            unpackedElements.remove(_index);
            header.setElementCount(unpackedElements.size());
            changed = true;
            return true;
        }
        return false;
    }

    public boolean clearElements()
    {
        if (validGroup && !isWriteLockedAndSealed())
        {
            unpackedElements.clear();
            header.setElementCount(unpackedElements.size());
            changed = true;
            return true;
        }
        return false;
    }
    //endregion

    public boolean isWriteLockedAndSealed()
    {
        return (header.isWriteLocked() && sealed);
    }

    public boolean doesGroupHaveExpectedByteCountPerElement(int _byteCount)
    {
        return header.getByteCountPerElement() == _byteCount;
    }

    public void seal()
    {
        sealed = true;
    }


    //region Serialization
    public byte[] packElementsIntoByteArray()
    {
        if (validGroup)
        {
            return GroupTools.packElementsIntoByteArray(header, unpackedElements);
        }
        return null;
    }

    @Override
    public Amplet serializeToAmplet()
    {
        return Amplet.create(this);
    }
    //endregion
}
