package amp.group_primitives.tools;

import amp.exceptions.*;
import amp.group_ids.GroupID;
import amp.group_primitives.GroupHeader;
import amp.ByteTools;
import amp.AmpConstants;

import java.util.ArrayList;

public class GroupHeaderTools
{
    public static ArrayList<GroupHeader> processHeader(byte[] _bytes)
    {
        int headerLength = getHeaderLength(_bytes);

        if (headerLength != 0)
        {
            int version = ByteTools.buildInt(_bytes[0], _bytes[1], _bytes[2], _bytes[3]);
            if (version != AmpConstants.VERSIONASINTEGER)
            {
                short[] versionArray = ByteTools.deconcatenateShorts(version);
                short majorVersion = versionArray[0];
                short minorVersion = versionArray[1];

                boolean compatible = (majorVersion == AmpConstants.MAJOR_VERSION) && (minorVersion <= AmpConstants.MINOR_VERSION);

                if (!compatible)
                {
                    if (AmpException.areExceptionsEnabled())
                    {
                        throw new UnsupportedAmpVersionException(version);
                    }

                    return null;
                }
            }

            int groupCount = ByteTools.buildInt(_bytes[4], _bytes[5], _bytes[6], _bytes[7]);

            if (groupCount > 0 && groupCount < AmpConstants.GROUPS_MAX_COUNT)
            {
                ArrayList<GroupHeader> groupHeaders = new ArrayList<>(groupCount);

                int byteArrayStartPosition = headerLength;

                for (int i = 0; i < groupCount; i++)
                {
                    int headerPosition = AmpConstants.INT_BYTE_FOOTPRINT + AmpConstants.INT_BYTE_FOOTPRINT + AmpConstants.GROUP_HEADER_BYTE_FOOTPRINT * i;
                    long classID = ByteTools.buildUnsignedInt(_bytes[headerPosition], _bytes[headerPosition + 1], _bytes[headerPosition + 2], _bytes[headerPosition + 3]);

                    headerPosition += AmpConstants.INT_BYTE_FOOTPRINT;
                    long classInstanceID = ByteTools.buildUnsignedInt(_bytes[headerPosition], _bytes[headerPosition + 1], _bytes[headerPosition + 2], _bytes[headerPosition + 3]);

                    headerPosition += AmpConstants.INT_BYTE_FOOTPRINT;
                    int byteCountPerElement = ByteTools.buildInt(_bytes[headerPosition], _bytes[headerPosition + 1], _bytes[headerPosition + 2], _bytes[headerPosition + 3]);

                    headerPosition += AmpConstants.INT_BYTE_FOOTPRINT;
                    int elementCount = ByteTools.buildInt(_bytes[headerPosition], _bytes[headerPosition + 1], _bytes[headerPosition + 2], _bytes[headerPosition + 3]);

                    GroupID tempGroupID = new GroupID(classID, classInstanceID, "default", false);

                    GroupHeader tempGroupHeader = GroupHeader.create(tempGroupID, byteCountPerElement, elementCount, byteArrayStartPosition);

                    if (tempGroupHeader == null)
                    {
                        if (AmpException.areExceptionsEnabled())
                        {
                            throw new InvalidGroupHeaderException("Failed to create group header.");
                        }

                        return null;
                    }

                    groupHeaders.add(tempGroupHeader);

                    byteArrayStartPosition += elementCount * byteCountPerElement;
                }

                if (groupHeaders.size() > 0 && groupHeaders.size() < AmpConstants.GROUPS_MAX_COUNT)
                {
                    return groupHeaders;
                } else if (AmpException.areExceptionsEnabled())
                {
                    throw new UnsupportedGroupCountException(groupHeaders.size());
                }

            } else if (AmpException.areExceptionsEnabled())
            {
                throw new UnsupportedGroupCountException(groupCount);
            }

        } else if (AmpException.areExceptionsEnabled())
        {
            throw new InvalidAmpletHeaderException("Failed to calculate Amplet header byte footprint.");
        }

        return null;
    }

    public static int getHeaderLength(byte[] _bytes)
    {
        if (_bytes != null && _bytes.length > (AmpConstants.INT_BYTE_FOOTPRINT * 2))
        {
            //int version = ByteTools.buildInt(_bytes[0], _bytes[1], _bytes[2], _bytes[3]);

            int groupCount = ByteTools.buildInt(_bytes[4], _bytes[5], _bytes[6], _bytes[7]);

            int headerLength = AmpConstants.INT_BYTE_FOOTPRINT + AmpConstants.INT_BYTE_FOOTPRINT + groupCount * AmpConstants.GROUP_HEADER_BYTE_FOOTPRINT;

            if (groupCount > 0 && _bytes.length > headerLength)
            {
                return headerLength;
            }
        }
        return 0;
    }

    public static byte[] packHeader(GroupHeader _header)
    {
        if (_header.isValidGroup())
        {
            byte[] classIDBytes = ByteTools.deconstructUnsignedInt(_header.getClassID());
            byte[] classInstanceIDBytes = ByteTools.deconstructUnsignedInt(_header.getClassInstanceID());
            byte[] byteCountPerElementBytes = ByteTools.deconstructInt(_header.getByteCountPerElement());
            byte[] elementCountBytes = ByteTools.deconstructInt(_header.getElementCount());

            byte[] headerBytes = new byte[AmpConstants.GROUP_HEADER_BYTE_FOOTPRINT];

            int position = 0;
            System.arraycopy(classIDBytes, 0, headerBytes, position, AmpConstants.INT_BYTE_FOOTPRINT);

            position += AmpConstants.INT_BYTE_FOOTPRINT;
            System.arraycopy(classInstanceIDBytes, 0, headerBytes, position, AmpConstants.INT_BYTE_FOOTPRINT);

            position += AmpConstants.INT_BYTE_FOOTPRINT;
            System.arraycopy(byteCountPerElementBytes, 0, headerBytes, position, AmpConstants.INT_BYTE_FOOTPRINT);

            position += AmpConstants.INT_BYTE_FOOTPRINT;
            System.arraycopy(elementCountBytes, 0, headerBytes, position, AmpConstants.INT_BYTE_FOOTPRINT);

            return headerBytes;

        } else if (AmpException.areExceptionsEnabled())
        {
            throw new InvalidGroupHeaderException("Group header passed into packHeader() was invalid.");
        }

        return null;
    }
}
