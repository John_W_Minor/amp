package amp.group_primitives.tools;

import amp.ByteTools;
import amp.AmpConstants;
import amp.exceptions.*;
import amp.group_primitives.GroupHeader;
import amp.group_primitives.PackedGroup;
import amp.group_primitives.UnpackedGroup;

import java.util.ArrayList;

public class GroupTools
{
    //region Unpacking Methods
    public static ArrayList<byte[]> unpackElementsFromByteArray(GroupHeader _header, byte[] _bytes)
    {
        boolean headerNotNull = _header != null;
        boolean bytesNotNull = _bytes != null;

        if (headerNotNull && bytesNotNull)
        {
            boolean headerIsValid = _header.isValidGroup();

            if (headerIsValid)
            {
                int elementCount = _header.getElementCount();

                ArrayList<byte[]> unpackedElements = new ArrayList<>(elementCount * 2);

                int byteCountPerElement = _header.getByteCountPerElement();

                int copyPosition = _header.getByteArrayStartPosition();

                for (int i = 0; i < elementCount; i++)
                {
                    byte[] element = new byte[byteCountPerElement];

                    System.arraycopy(_bytes, copyPosition, element, 0, byteCountPerElement);

                    copyPosition += byteCountPerElement;

                    unpackedElements.add(element);
                }

                if (unpackedElements.size() != 0)
                {
                    return unpackedElements;
                } else if (AmpException.areExceptionsEnabled())
                {
                    throw new ByteArrayOperationFailureException();
                }

            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidGroupHeaderException("Invalid group header passed into unpackElementsFromByteArray().");
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            if (!headerNotNull)
            {
                throw new BadArgumentException("Null _header argument was passed into unpackElementsFromByteArray().");
            }

            if (!bytesNotNull)
            {
                throw new BadArgumentException("Null _bytes argument was passed into unpackElementsFromByteArray().");
            }
        }
        return null;
    }

    public static byte[] extractPackedGroupBytes(GroupHeader _header, byte[] _bytes)
    {
        boolean headerNotNull = _header != null;
        boolean bytesNotNull = _bytes != null;

        if (headerNotNull && bytesNotNull)
        {
            if (_header.isValidGroup())
            {
                byte[] packedElements = new byte[_header.getByteCountTotalAllElements()];

                System.arraycopy(_bytes, _header.getByteArrayStartPosition(), packedElements, 0, _header.getByteCountTotalAllElements());

                if (packedElements.length != 0)
                {
                    return packedElements;
                } else if (AmpException.areExceptionsEnabled())
                {
                    throw new ByteArrayOperationFailureException();
                }

            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidGroupHeaderException("Invalid group header passed into extractPackedGroupBytes().");
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            if (!headerNotNull)
            {
                throw new BadArgumentException("Null _header argument was passed into extractPackedGroupBytes().");
            }

            if (!bytesNotNull)
            {
                throw new BadArgumentException("Null _bytes argument was passed into extractPackedGroupBytes().");
            }
        }
        return null;
    }

    //packing methods
    public static PackedGroup packGroupFromByteArray(GroupHeader _header, byte[] _bytes)
    {
        boolean headerNotNull = _header != null;
        boolean bytesNotNull = _bytes != null;

        if (headerNotNull && bytesNotNull)
        {
            if (_header.isValidGroup())
            {
                return PackedGroup.create(GroupHeaderTools.packHeader(_header), extractPackedGroupBytes(_header, _bytes));
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidGroupHeaderException("Invalid group header passed into packGroupFromByteArray().");
            }

        } else if (AmpException.areExceptionsEnabled())
        {
            if (!headerNotNull)
            {
                throw new BadArgumentException("Null _header argument was passed into packGroupFromByteArray().");
            }

            if (!bytesNotNull)
            {
                throw new BadArgumentException("Null _bytes argument was passed into packGroupFromByteArray().");
            }
        }
        return null;
    }

    public static PackedGroup repackUnpackedGroup(UnpackedGroup _unpackedGroup)
    {
        if (_unpackedGroup != null)
        {
            if (_unpackedGroup.isValidGroup())
            {
                return PackedGroup.create(GroupHeaderTools.packHeader(_unpackedGroup.getHeader()), _unpackedGroup.packElementsIntoByteArray());

            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidGroupHeaderException("Invalid group header passed into repackUnpackedGroup().");
            }

        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _unpackedGroup argument was passed into repackUnpackedGroup().");
        }
        return null;
    }

    public static byte[] packElementsIntoByteArray(GroupHeader _header, ArrayList<byte[]> _unpackedElements)
    {
        boolean headerNotNull = _header != null;
        boolean unpackedElementsNotNull = _unpackedElements != null;

        if (headerNotNull && unpackedElementsNotNull)
        {
            if (_header.isValidGroup())
            {
                int byteCountPerElement = _header.getByteCountPerElement();
                int totalBytes = _header.getByteCountTotalAllElements();

                byte[] packedElements = new byte[totalBytes];

                int position = 0;

                for (int i = 0; i < _unpackedElements.size(); i++)
                {
                    byte[] temp = _unpackedElements.get(i);

                    System.arraycopy(temp, 0, packedElements, position, byteCountPerElement);

                    position += byteCountPerElement;
                }

                if (packedElements.length != 0)
                {
                    return packedElements;
                } else if (AmpException.areExceptionsEnabled())
                {
                    throw new ByteArrayOperationFailureException();
                }

            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidGroupHeaderException("Invalid group header passed into packElementsIntoByteArray().");
            }

        } else if (AmpException.areExceptionsEnabled())
        {
            if (!headerNotNull)
            {
                throw new BadArgumentException("Null _header argument was passed into packElementsIntoByteArray().");
            }

            if (!unpackedElementsNotNull)
            {
                throw new BadArgumentException("Null _unpackedElements argument was passed into packElementsIntoByteArray().");
            }
        }
        return null;
    }
    //endregion

    //region Building Methods
    public static byte[] buildAmpletBytesFromUnpackedGroups(ArrayList<UnpackedGroup> _unpackedGroups)
    {
        if (_unpackedGroups != null)
        {
            ArrayList<PackedGroup> packedGroups = new ArrayList<>();

            for (UnpackedGroup ug : _unpackedGroups)
            {
                packedGroups.add(GroupTools.repackUnpackedGroup(ug));
            }

            return buildAmpletBytesFromPackedGroups(packedGroups);

        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _unpackedGroup argument was passed into buildAmpletBytesFromUnpackedGroups().");
        }
        return null;
    }

    public static byte[] buildAmpletBytesFromPackedGroups(ArrayList<PackedGroup> _packedGroups, byte[] _trailer)
    {
        if (_packedGroups != null)
        {
            int position = 0;

            byte[] versionBytes = ByteTools.deconstructInt(AmpConstants.VERSIONASINTEGER);

            int groupsCount = _packedGroups.size();
            byte[] groupsCountBytes = ByteTools.deconstructInt(groupsCount);

            int headerByteCount = AmpConstants.INT_BYTE_FOOTPRINT + AmpConstants.INT_BYTE_FOOTPRINT + groupsCount * AmpConstants.GROUP_HEADER_BYTE_FOOTPRINT;
            int elementsByteCount = 0;
            for (PackedGroup pg : _packedGroups)
            {
                elementsByteCount += pg.getByteCountTotalAllElements();
            }

            int trailerByteCount = 0;

            if (_trailer != null)
            {
                trailerByteCount = _trailer.length;
            }

            int totalByteCount = headerByteCount + elementsByteCount + trailerByteCount;

            if (totalByteCount < 1 || totalByteCount > AmpConstants.BYTE_ARRAY_MAX_BYTE_COUNT)
            {
                if (AmpException.areExceptionsEnabled())
                {
                    throw new UnsupportedAmpletSizeException(totalByteCount);
                }
                return null;
            }

            byte[] bytes = new byte[totalByteCount];

            System.arraycopy(versionBytes, 0, bytes, position, AmpConstants.INT_BYTE_FOOTPRINT);
            position += AmpConstants.INT_BYTE_FOOTPRINT;

            System.arraycopy(groupsCountBytes, 0, bytes, position, AmpConstants.INT_BYTE_FOOTPRINT);
            position += AmpConstants.INT_BYTE_FOOTPRINT;

            for (PackedGroup pg : _packedGroups)
            {
                byte[] temp = pg.getPackedHeaderBytes();

                System.arraycopy(temp, 0, bytes, position, AmpConstants.GROUP_HEADER_BYTE_FOOTPRINT);

                position += AmpConstants.GROUP_HEADER_BYTE_FOOTPRINT;
            }

            for (PackedGroup pg : _packedGroups)
            {
                byte[] temp = pg.getPackedElementsBytes();
                int tempLength = temp.length;

                System.arraycopy(temp, 0, bytes, position, tempLength);

                position += tempLength;
            }

            if (_trailer != null)
            {
                System.arraycopy(_trailer, 0, bytes, position, trailerByteCount);
                //position += totalByteCount;
            }

            return bytes;
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _packedGroups argument was passed into buildAmpletBytesFromPackedGroups().");
        }
        return null;
    }

    public static byte[] buildAmpletBytesFromPackedGroups(ArrayList<PackedGroup> _packedGroups)
    {
        return buildAmpletBytesFromPackedGroups(_packedGroups, null);
    }
    //endregion
}
