package amp.serialization;

public interface IAmpBytesAccessible
{
    byte[] serializeToBytes();
}
