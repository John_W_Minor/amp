package amp.serialization;

import amp.Amplet;

public interface IAmpAmpletSerializable
{
    Amplet serializeToAmplet();
}
