package amp.serialization;

import amp.typed_data.ITypedData;

public interface IAmpByteFactory
{
    IAmpDeserializable build(byte[] _bytes);

    IAmpDeserializable build(ITypedData _data);
}
