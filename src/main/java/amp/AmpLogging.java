package amp;

import logging.DefaultLogger;
import logging.IAmpexLogger;

public class AmpLogging
{
    private volatile static IAmpexLogger logger;
    private volatile static boolean isLogging = false;

    public synchronized static void startLogging()
    {
        startLogging(new DefaultLogger());
    }

    public synchronized static void startLogging(IAmpexLogger _logger)
    {
        if (logger == null)
        {
            logger = _logger;
        }
        isLogging = true;
        logger.info("Amp logging enabled.");
    }

    public synchronized static void stopLogging()
    {
        logger.info("Amp logging disabled.");
        isLogging = false;
    }

    public synchronized static IAmpexLogger getLogger()
    {
        if (isLogging)
        {
            return logger;
        }
        return null;
    }

    public synchronized boolean isLoggingEnabled()
    {
        return isLogging;
    }
}
