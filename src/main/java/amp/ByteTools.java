package amp;

import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.group_ids.GroupID;

import java.nio.ByteBuffer;

public class ByteTools
{
    //These methods are useful for working with bytes from amplets.
    //region Builders
    public static int buildUnsignedByte(byte _a)
    {
        byte zero = 0;
        ByteBuffer buffer = ByteBuffer.allocate(4).put(zero).put(zero).put(zero).put(_a);

        return buffer.getInt(0);
    }

    public static boolean buildBoolean(byte _a)
    {
        return _a == 1;
    }

    public static short buildShort(byte _a, byte _b)
    {
        ByteBuffer buffer = ByteBuffer.allocate(2).put(_a).put(_b);

        return buffer.getShort(0);
    }

    public static char buildChar(byte _a, byte _b)
    {
        ByteBuffer buffer = ByteBuffer.allocate(2).put(_a).put(_b);

        return buffer.getChar(0);
    }

    public static long buildUnsignedInt(byte _a, byte _b, byte _c, byte _d)
    {
        byte zero = 0;
        ByteBuffer buffer = ByteBuffer.allocate(8).put(zero).put(zero).put(zero).put(zero).put(_a).put(_b).put(_c).put(_d);

        return buffer.getLong(0);
    }

    public static int buildInt(byte _a, byte _b, byte _c, byte _d)
    {
        ByteBuffer buffer = ByteBuffer.allocate(4).put(_a).put(_b).put(_c).put(_d);

        return buffer.getInt(0);
    }

    public static float buildFloat(byte _a, byte _b, byte _c, byte _d)
    {
        ByteBuffer buffer = ByteBuffer.allocate(4).put(_a).put(_b).put(_c).put(_d);

        return buffer.getFloat(0);
    }

    public static long buildLong(byte _a, byte _b, byte _c, byte _d, byte _e, byte _f, byte _g, byte _h)
    {
        ByteBuffer buffer = ByteBuffer.allocate(8).put(_a).put(_b).put(_c).put(_d).put(_e).put(_f).put(_g).put(_h);

        return buffer.getLong(0);
    }

    public static double buildDouble(byte _a, byte _b, byte _c, byte _d, byte _e, byte _f, byte _g, byte _h)
    {
        ByteBuffer buffer = ByteBuffer.allocate(8).put(_a).put(_b).put(_c).put(_d).put(_e).put(_f).put(_g).put(_h);

        return buffer.getDouble(0);
    }

    public static String buildHexString(byte[] array)
    {
        StringBuilder sb = new StringBuilder();
        for (byte b : array)
        {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }
    //endregion

    //region Deconstructors
    public static byte[] deconstructUnsignedByte(int _value)
    {
        byte value = (byte) _value;

        return ByteBuffer.allocate(AmpConstants.BYTE_BYTE_FOOTPRINT).put(value).array();
    }

    public static byte[] deconstructBoolean(boolean _value)
    {
        if (_value)
        {
            byte[] bytes = {1};
            return bytes;
        }

        byte[] bytes = {0};
        return bytes;
    }

    public static byte[] deconstructShort(short _value)
    {
        return ByteBuffer.allocate(AmpConstants.SHORT_BYTE_FOOTPRINT).putShort(_value).array();
    }

    public static byte[] deconstructChar(char _value)
    {
        return ByteBuffer.allocate(AmpConstants.CHAR_BYTE_FOOTPRINT).putChar(_value).array();
    }

    public static byte[] deconstructUnsignedInt(long _value)
    {
        int value = (int) _value;

        return ByteBuffer.allocate(AmpConstants.INT_BYTE_FOOTPRINT).putInt(value).array();
    }

    public static byte[] deconstructInt(int _value)
    {
        return ByteBuffer.allocate(AmpConstants.INT_BYTE_FOOTPRINT).putInt(_value).array();
    }

    public static byte[] deconstructFloat(float _value)
    {
        return ByteBuffer.allocate(AmpConstants.FLOAT_BYTE_FOOTPRINT).putFloat(_value).array();
    }

    public static byte[] deconstructLong(long _value)
    {
        return ByteBuffer.allocate(AmpConstants.LONG_BYTE_FOOTPRINT).putLong(_value).array();
    }

    public static byte[] deconstructDouble(double _value)
    {
        return ByteBuffer.allocate(AmpConstants.DOUBLE_BYTE_FOOTPRINT).putDouble(_value).array();
    }
    //endregion

    //region Short Concat/Deconcat
    public static int concatenateShorts(short _a, short _b)
    {
        byte[] a = deconstructShort(_a);
        byte[] b = deconstructShort(_b);
        return buildInt(a[0], a[1], b[0], b[1]);
    }

    public static short[] deconcatenateShorts(int _value)
    {
        short[] shorts = new short[2];

        byte[] intBytes = deconstructInt(_value);
        shorts[0] = buildShort(intBytes[0], intBytes[1]);
        shorts[1] = buildShort(intBytes[2], intBytes[3]);
        return shorts;
    }
    //endregion

    //These methods make it easy to work with ClassIDs without groupID objects
    //region Class ID
    public static long writeLockClassID(long _classID)
    {
        return _classID | AmpConstants.WRITE_LOCK_MARKER;
    }

    public static boolean isClassIDWriteLocked(long _classID)
    {
        return (_classID & AmpConstants.WRITE_LOCK_MARKER) == AmpConstants.WRITE_LOCK_MARKER;
    }

    public static long amplifyClassID(long _classID)
    {
        return _classID | AmpConstants.AMPLET_CLASS_MARKER;
    }

    public static boolean isClassIDAmplified(long _classID)
    {
        return (_classID & AmpConstants.AMPLET_CLASS_MARKER) == AmpConstants.AMPLET_CLASS_MARKER;
    }
    //endregion

    //with groupID object
    //region Group ID
    public static GroupID writeLockGroupID(GroupID _groupID)
    {
        if (_groupID != null)
        {
            long classID = writeLockClassID(_groupID.getClassID());
            return new GroupID(classID, _groupID.getClassInstanceID(), _groupID.getName(), true);
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into writeLockGroupID().");
        }
        return null;
    }

    public static boolean isGroupIDWriteLocked(GroupID _groupID)
    {
        if (_groupID != null)
        {
            return isClassIDWriteLocked(_groupID.getClassID());
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into isGroupIDWriteLocked().");
        }
        return false;
    }

    public static GroupID amplifyGroupID(GroupID _groupID)
    {
        if (_groupID != null)
        {
            long classID = amplifyClassID(_groupID.getClassID());
            return new GroupID(classID, _groupID.getClassInstanceID(), _groupID.getName(), true);
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into amplifyGroupID().");
        }
        return null;
    }

    public static boolean isGroupIDAmplified(GroupID _groupID)
    {
        if (_groupID != null)
        {
            return isClassIDAmplified(_groupID.getClassID());
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into isGroupIDAmplified().");
        }
        return false;
    }
    //endregion
}
