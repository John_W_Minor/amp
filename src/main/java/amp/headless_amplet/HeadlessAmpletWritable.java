package amp.headless_amplet;

import amp.backing_lists.ByteList;
import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.headless_amplet.abstract_classes.AbstractHeadlessAmpletWritable;

public class HeadlessAmpletWritable extends AbstractHeadlessAmpletWritable
{
    //Headless Amplet objects use no prefixing to note where data is
    //because it is assumed they will only be used when the programmer has a strong guarantee about the byte footprint if specific data.

    //region Constructors
    private HeadlessAmpletWritable()
    {
        validHeadlessAmplet = true;
    }

    private HeadlessAmpletWritable(byte[] _bytes)
    {
        if (_bytes != null)
        {
            bytes = new ByteList(_bytes);

            validHeadlessAmplet = true;
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _bytes argument was passed into HeadlessAmpletWritable().");
        }
    }

    public static HeadlessAmpletWritable create()
    {
        return new HeadlessAmpletWritable();
    }

    public static HeadlessAmpletWritable create(byte[] _bytes)
    {
        HeadlessAmpletWritable tempHeadless = new HeadlessAmpletWritable(_bytes);
        if (tempHeadless.isValidHeadlessAmplet())
        {
            return tempHeadless;
        }
        return null;
    }
    //endregion
}