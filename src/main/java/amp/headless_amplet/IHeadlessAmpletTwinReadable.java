package amp.headless_amplet;

import amp.backing_lists.ByteList;

public interface IHeadlessAmpletTwinReadable extends IHeadlessAmpletReadable
{
    ByteList getTwinBytesUnsafe(boolean _doYouKnowWhatYouAreDoing);
}
