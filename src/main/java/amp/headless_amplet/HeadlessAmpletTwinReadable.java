package amp.headless_amplet;

import amp.backing_lists.ByteList;
import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.exceptions.YouDidNotKnowWhatYouWereDoingException;
import amp.headless_amplet.abstract_classes.AbstractHeadlessAmpletReadable;

public class HeadlessAmpletTwinReadable extends AbstractHeadlessAmpletReadable implements IHeadlessAmpletTwinReadable
{
    //region Constructors
    private HeadlessAmpletTwinReadable(byte[] _bytes)
    {
        if (_bytes != null)
        {
            bytes = new ByteList(_bytes);

            validHeadlessAmplet = true;
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _bytes argument was passed into HeadlessAmpletTwinReadable().");
        }
    }

    private HeadlessAmpletTwinReadable(IHeadlessAmpletTwinReadable _hamplet)
    {
        if (_hamplet != null)
        {
            bytes = _hamplet.getTwinBytesUnsafe(true);

            validHeadlessAmplet = true;
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _hamplet argument was passed into HeadlessAmpletTwinReadable().");
        }
    }

    public static HeadlessAmpletTwinReadable create(byte[] _bytes)
    {
        HeadlessAmpletTwinReadable tempHeadless = new HeadlessAmpletTwinReadable(_bytes);
        if (tempHeadless.isValidHeadlessAmplet())
        {
            return tempHeadless;
        }
        return null;
    }

    public static HeadlessAmpletTwinReadable createTwin(IHeadlessAmpletTwinReadable _hamplet)
    {
        HeadlessAmpletTwinReadable tempHeadless = new HeadlessAmpletTwinReadable(_hamplet);
        if (tempHeadless.isValidHeadlessAmplet())
        {
            return tempHeadless;
        }
        return null;
    }
    //endregion

    //region Twinning
    @Override
    public ByteList getTwinBytesUnsafe(boolean _doYouKnowWhatYouAreDoing)
    {
        if (_doYouKnowWhatYouAreDoing)
        {
            return bytes;
        }
        throw new YouDidNotKnowWhatYouWereDoingException();
    }
    //endregion
}
