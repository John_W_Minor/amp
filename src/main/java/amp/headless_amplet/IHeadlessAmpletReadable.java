package amp.headless_amplet;

import amp.Amplet;
import amp.serialization.IAmpByteSerializable;
import amp.serialization.IAmpDeserializable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public interface IHeadlessAmpletReadable extends IAmpByteSerializable, IAmpDeserializable
{
    //region Cursor Position
    void resetCursor();

    void setCursor(int _index);

    int getCursor();

    int getMaxCursorPosition();
    //endregion

    //region Body Travellers
    byte[] getNextNBytes(int _n);

    Integer getNextUnsignedByte();

    Byte getNextByte();

    Boolean getNextBoolean();

    Short getNextShort();

    Character getNextCharacter();

    Long getNextUnsignedInt();

    Integer getNextInt();

    Float getNextFloat();

    Long getNextLong();

    Double getNextDouble();

    String getNextString(int _n);

    String getNextString(int _n, String _charsetName) throws UnsupportedEncodingException;

    String getNextHexString(int _n);

    BigInteger getNextBigInteger(int _n);

    Amplet getNextAmplet(int _n);
    //endregion

    //region Getters
    int getByteFootprint();

    boolean isValidHeadlessAmplet();
    //endregion
}
