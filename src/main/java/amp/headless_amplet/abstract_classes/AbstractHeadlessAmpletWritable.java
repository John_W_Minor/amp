package amp.headless_amplet.abstract_classes;

import amp.AmpConstants;
import amp.Amplet;
import amp.ByteTools;
import amp.backing_lists.GrowModeEnum;
import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.exceptions.InvalidAmpletException;
import amp.headless_amplet.IHeadlessAmpletWritable;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public abstract class AbstractHeadlessAmpletWritable extends AbstractHeadlessAmpletReadable implements IHeadlessAmpletWritable
{
    //region Adders
    @Override
    public boolean addBytes(byte[] _bytes)
    {
        if (_bytes != null)
        {
            return bytes.addAll(_bytes);
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _bytes argument was passed into addBytes().");
        }

        return false;
    }

    @Override
    public boolean addElement(byte _element)
    {
        byte[] byteArray = {_element};

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(boolean _element)
    {
        byte[] byteArray = ByteTools.deconstructBoolean(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(short _element)
    {
        byte[] byteArray = ByteTools.deconstructShort(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(char _element)
    {
        byte[] byteArray = ByteTools.deconstructChar(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(int _element)
    {
        byte[] byteArray = ByteTools.deconstructInt(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(float _element)
    {
        byte[] byteArray = ByteTools.deconstructFloat(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(long _element)
    {
        byte[] byteArray = ByteTools.deconstructLong(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(double _element)
    {
        byte[] byteArray = ByteTools.deconstructDouble(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(String _element)
    {
        if (_element != null)
        {
            return addBytes(_element.getBytes(AmpConstants.UTF8));
        }
        return addBytes(null);
    }

    @Override
    public boolean addElement(String _element, String _charsetName) throws UnsupportedEncodingException
    {
        if (_element != null)
        {
            return addBytes(_element.getBytes(_charsetName));
        }
        return addBytes(null);
    }

    @Override
    public boolean addElement(BigInteger _element)
    {
        if (_element != null)
        {
            return addBytes(_element.toByteArray());
        }
        return addBytes(null);
    }

    @Override
    public boolean addElement(IAmpByteSerializable _element)
    {
        if (_element != null)
        {
            return addBytes(_element.serializeToBytes());
        }
        return addBytes(null);
    }

    @Override
    public boolean addElement(IAmpAmpletSerializable _element)
    {
        if (_element != null)
        {
            Amplet tempAmp = _element.serializeToAmplet();
            if (tempAmp != null)
            {
                return addBytes(tempAmp.serializeToBytes());
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidAmpletException("Failed to create amplet.");
            }
        }
        return addBytes(null);
    }
    //endregion

    //region List Features
    @Override
    public int getCurrentSize()
    {
        return bytes.size();
    }

    @Override
    public void ensureCapacity(int _minCapacity)
    {
        GrowModeEnum currentGrowMode = bytes.getGrowMode();
        bytes.setGrowMode(GrowModeEnum.NATURAL);
        bytes.ensureCapacity(_minCapacity);
        bytes.setGrowMode(currentGrowMode);
    }

    @Override
    public void setGrowMode(GrowModeEnum _growMode)
    {
        bytes.setGrowMode(_growMode);
    }

    @Override
    public GrowModeEnum getGrowMode()
    {
        return bytes.getGrowMode();
    }

    @Override
    public void trimToSize()
    {
        bytes.trimToSize();
    }
    //endregion

    //region Body Travellers
    @Override
    public boolean deleteNextNBytes(int _n)
    {
        int count = bytes.size();

        if (_n < 1)
        {
            return false;
        }

        if ((cursor + _n) > count)
        {
            return false;
        }

        bytes.removeNBytes(cursor, _n);

        return true;
    }
    //endregion
}
