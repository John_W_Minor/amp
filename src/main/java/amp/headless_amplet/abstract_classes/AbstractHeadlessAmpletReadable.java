package amp.headless_amplet.abstract_classes;

import amp.AmpConstants;
import amp.Amplet;
import amp.ByteTools;
import amp.backing_lists.ByteList;
import amp.headless_amplet.IHeadlessAmpletReadable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public abstract class AbstractHeadlessAmpletReadable implements IHeadlessAmpletReadable
{
    //region Member Fields
    protected ByteList bytes = new ByteList();
    protected int cursor = 0;

    protected boolean validHeadlessAmplet = false;
    //endregion

    //region Cursor Position
    @Override
    public void resetCursor()
    {
        cursor = 0;
    }

    @Override
    public void setCursor(int _index)
    {
        if (_index > bytes.size())
        {
            _index = bytes.size() - 1;
        }

        if (_index < 0)
        {
            _index = 0;
        }
        cursor = _index;
    }

    @Override
    public int getCursor()
    {
        return cursor;
    }

    @Override
    public int getMaxCursorPosition()
    {
        return bytes.size() - 1;
    }
    //endregion

    //region Body Travellers
    @Override
    public byte[] getNextNBytes(int _n)
    {
        if (_n == 0)
        {
            return new byte[0];
        }

        if (_n < 0)
        {
            return null;
        }

        if ((cursor + _n) > bytes.size())
        {
            return null;
        }

        byte[] tempArray = bytes.getNBytes(cursor, _n);

        cursor += _n;

        return tempArray;
    }

    @Override
    public Integer getNextUnsignedByte()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.BYTE_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.BYTE_BYTE_FOOTPRINT)
        {
            return ByteTools.buildUnsignedByte(byteArray[0]);
        }
        return null;
    }

    @Override
    public Byte getNextByte()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.BYTE_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.BYTE_BYTE_FOOTPRINT)
        {
            return byteArray[0];
        }
        return null;
    }

    @Override
    public Boolean getNextBoolean()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.BYTE_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.BYTE_BYTE_FOOTPRINT)
        {
            return ByteTools.buildBoolean(byteArray[0]);
        }
        return null;
    }

    @Override
    public Short getNextShort()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.SHORT_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.SHORT_BYTE_FOOTPRINT)
        {
            return ByteTools.buildShort(byteArray[0], byteArray[1]);
        }
        return null;
    }

    @Override
    public Character getNextCharacter()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.CHAR_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.CHAR_BYTE_FOOTPRINT)
        {
            return ByteTools.buildChar(byteArray[0], byteArray[1]);
        }
        return null;
    }

    @Override
    public Long getNextUnsignedInt()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.INT_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.INT_BYTE_FOOTPRINT)
        {
            return ByteTools.buildUnsignedInt(byteArray[0], byteArray[1], byteArray[2], byteArray[3]);
        }
        return null;
    }

    @Override
    public Integer getNextInt()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.INT_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.INT_BYTE_FOOTPRINT)
        {
            return ByteTools.buildInt(byteArray[0], byteArray[1], byteArray[2], byteArray[3]);
        }
        return null;
    }

    @Override
    public Float getNextFloat()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.FLOAT_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.FLOAT_BYTE_FOOTPRINT)
        {
            return ByteTools.buildFloat(byteArray[0], byteArray[1], byteArray[2], byteArray[3]);
        }
        return null;
    }

    @Override
    public Long getNextLong()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.LONG_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.LONG_BYTE_FOOTPRINT)
        {
            return ByteTools.buildLong(byteArray[0], byteArray[1], byteArray[2], byteArray[3], byteArray[4], byteArray[5], byteArray[6], byteArray[7]);
        }
        return null;
    }

    @Override
    public Double getNextDouble()
    {
        byte[] byteArray = getNextNBytes(AmpConstants.DOUBLE_BYTE_FOOTPRINT);
        if (byteArray != null && byteArray.length == AmpConstants.DOUBLE_BYTE_FOOTPRINT)
        {
            return ByteTools.buildDouble(byteArray[0], byteArray[1], byteArray[2], byteArray[3], byteArray[4], byteArray[5], byteArray[6], byteArray[7]);
        }
        return null;
    }

    @Override
    public String getNextString(int _n)
    {
        byte[] byteArray = getNextNBytes(_n);
        if (byteArray != null && byteArray.length == _n)
        {
            return new String(byteArray, AmpConstants.UTF8);
        }
        return null;
    }

    @Override
    public String getNextString(int _n, String _charsetName) throws UnsupportedEncodingException
    {
        byte[] byteArray = getNextNBytes(_n);
        if (byteArray != null && byteArray.length == _n)
        {
            return new String(byteArray, _charsetName);
        }
        return null;
    }

    @Override
    public String getNextHexString(int _n)
    {
        byte[] byteArray = getNextNBytes(_n);
        if (byteArray != null && byteArray.length == _n)
        {
            return ByteTools.buildHexString(byteArray);
        }
        return null;
    }

    @Override
    public BigInteger getNextBigInteger(int _n)
    {
        byte[] byteArray = getNextNBytes(_n);
        if (byteArray != null && byteArray.length == _n)
        {
            return new BigInteger(byteArray);
        }
        return null;
    }

    @Override
    public Amplet getNextAmplet(int _n)
    {
        byte[] byteArray = getNextNBytes(_n);
        if (byteArray != null && byteArray.length == _n)
        {
            return Amplet.create(byteArray);
        }
        return null;
    }
    //endregion

    //region Getters
    @Override
    public int getByteFootprint()
    {
        return bytes.size();
    }

    @Override
    public boolean isValidHeadlessAmplet()
    {
        return validHeadlessAmplet;
    }
    //endregion

    //region Serialization
    @Override
    public byte[] serializeToBytes()
    {
        return bytes.toArray();
    }
    //endregion
}
