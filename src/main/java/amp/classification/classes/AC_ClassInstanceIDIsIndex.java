package amp.classification.classes;

import amp.AmpConstants;
import amp.Amplet;
import amp.ByteTools;
import amp.exceptions.AmpException;
import amp.exceptions.ClassIDNotAmplifiedException;
import amp.exceptions.InvalidAmpletException;
import amp.exceptions.InvalidClassIDException;
import amp.group_ids.GroupID;
import amp.group_primitives.UnpackedGroup;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;

public class AC_ClassInstanceIDIsIndex implements IAmpClass, IAmpAmpletSerializable
{
    //THIS AC DOES GUARANTEE THE INPUT ORDER IS THE OUTPUT ORDER
    //BUT IF YOU EDIT AN AMPLET THIS GUARANTEE MAY BE BROKEN
    //THIS AC IS EXTREMELY INEFFICIENT AND WAS DEVELOPED BEFORE HAS, HPAS, AND HPSES.
    //YOU SHOULD PROBABLY USE A HA OR HPS INSTEAD BECAUSE THEY ARE SMALLER FOR THIS KIND OF SERIALIZATION
    //AND THEY OFFER STRONGER ORDER GUARANTEES
    //AMPLETS WERE MEANT TO BE FAST WAYS TO DEAL WITH LARGE AMOUNTS OF DATA
    //THEY WERE DESIGNED FOR RANDOM ACCESS, NOT SEQUENTIAL ACCESS, SO IT'S DUBIOUS TO RELY ON THEM FOR INPUT ORDER GUARANTEES

    //region Member Fields
    private int index = 0;
    private long classID;
    private String name;

    private ArrayList<UnpackedGroup> unpackedGroups = new ArrayList<>();

    private boolean validClass;
    //endregion

    //region Constructors
    private AC_ClassInstanceIDIsIndex(long _classID, String _name)
    {
        classID = _classID;
        name = _name;

        validClass = classID > -1 && classID < AmpConstants.CLASS_ID_MAX_VALUE;

        if (!validClass && AmpException.areExceptionsEnabled())
        {
            throw new InvalidClassIDException(classID);
        }
    }

    public static AC_ClassInstanceIDIsIndex create(long _classID, String _name)
    {
        AC_ClassInstanceIDIsIndex tempAC = new AC_ClassInstanceIDIsIndex(_classID, _name);
        if (tempAC.isValidClass())
        {
            return tempAC;
        }
        return null;
    }
    //endregion

    //region Adders
    public boolean addElement(byte[] _element)
    {
        if (_element != null)
        {
            int byteCount = _element.length;

            if (byteCount > 0 && byteCount < AmpConstants.ELEMENT_MAX_BYTE_FOOTPRINT)
            {
                if (index > -1 && index < AmpConstants.CLASS_INSTANCE_ID_MAX_VALUE)
                {

                    GroupID tempGroupID = new GroupID(classID, index, name, true, true);

                    UnpackedGroup temp = UnpackedGroup.create(tempGroupID, byteCount);

                    if (temp != null)
                    {
                        unpackedGroups.add(temp);

                        temp.addElement(_element);
                        index++;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean addElement(byte _element)
    {
        byte[] bytes = {_element};

        return addElement(bytes);
    }

    public boolean addElement(boolean _element)
    {
        byte[] bytes = ByteTools.deconstructBoolean(_element);

        return addElement(bytes);
    }

    public boolean addElement(short _element)
    {
        byte[] bytes = ByteTools.deconstructShort(_element);

        return addElement(bytes);
    }

    public boolean addElement(char _element)
    {
        byte[] bytes = ByteTools.deconstructChar(_element);

        return addElement(bytes);
    }

    public boolean addElement(int _element)
    {
        byte[] bytes = ByteTools.deconstructInt(_element);

        return addElement(bytes);
    }

    public boolean addElement(float _element)
    {
        byte[] bytes = ByteTools.deconstructFloat(_element);

        return addElement(bytes);
    }

    public boolean addElement(long _element)
    {
        byte[] bytes = ByteTools.deconstructLong(_element);

        return addElement(bytes);
    }

    public boolean addElement(double _element)
    {
        byte[] bytes = ByteTools.deconstructDouble(_element);

        return addElement(bytes);
    }

    public boolean addElement(String _element)
    {
        return addElement(_element.getBytes(AmpConstants.UTF8));
    }

    public boolean addElement(String _element, String _charsetName) throws UnsupportedEncodingException
    {
        return addElement(_element.getBytes(_charsetName));
    }

    public boolean addElement(BigInteger _element)
    {
        return addElement(_element.toByteArray());
    }

    public boolean addElement(IAmpByteSerializable _element)
    {
        return addElement(_element.serializeToBytes());
    }

    public boolean addElement(IAmpAmpletSerializable _element)
    {
        if (ByteTools.isClassIDAmplified(classID))
        {
            Amplet tempAmp = _element.serializeToAmplet();
            if (tempAmp != null)
            {
                return addElement(tempAmp.serializeToBytes());
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidAmpletException("Failed to create amplet.");
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new ClassIDNotAmplifiedException();
        }

        return false;
    }
    //endregion

    //region Getters
    @Override
    public long getClassID()
    {
        return classID;
    }

    @Override
    public ArrayList<UnpackedGroup> getUnpackedGroups()
    {
        return unpackedGroups;
    }

    @Override
    public boolean isValidClass()
    {
        return validClass;
    }
    //endregion

    //region Serialization
    @Override
    public Amplet serializeToAmplet()
    {
        return Amplet.create(this);
    }
    //endregion
}
