package amp.classification.classes;

import amp.group_primitives.UnpackedGroup;

import java.util.ArrayList;

public interface IAmpClass
{
    long getClassID();

    boolean isValidClass();

    ArrayList<UnpackedGroup> getUnpackedGroups();
}
