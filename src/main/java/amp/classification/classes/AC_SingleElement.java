package amp.classification.classes;

import amp.AmpConstants;
import amp.ByteTools;
import amp.Amplet;
import amp.group_ids.GroupID;
import amp.exceptions.*;
import amp.group_primitives.UnpackedGroup;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;

public class AC_SingleElement implements IAmpClass, IAmpAmpletSerializable
{
    //IF YOU ABSOLUTELY NEED TO USE AN AMPLET TO STORE ALL OF YOUR DATA
    //CONSIDER STORING SINGLE DATA ELEMENTS SERIALLY IN AN HA OR HPS
    //UNLESS FAST, RANDOM ACCESS IS MORE IMPORTANT FOR YOUR NEEDS

    //region Member Fields
    private GroupID groupID = null;
    private long classID = 0;
    private long classInstanceID = 0;

    private int elementLength = 0;

    private UnpackedGroup singleElementUnpackedGroup = null;

    private boolean validClass = false;
    //endregion

    //region Constructors
    private AC_SingleElement(GroupID _groupID, byte[] _element)
    {
        if (_groupID != null && _element != null)
        {
            groupID = _groupID;

            classID = groupID.getClassID();
            classInstanceID = groupID.getClassInstanceID();

            elementLength = _element.length;

            boolean elementLengthValid = elementLength > 0 && elementLength < AmpConstants.ELEMENT_MAX_BYTE_FOOTPRINT;

            if (elementLengthValid)
            {
                boolean classIDValid = classID > -1 && classID < AmpConstants.CLASS_ID_MAX_VALUE;
                boolean classInstanceIDValid = classInstanceID > -1 && classInstanceID < AmpConstants.CLASS_INSTANCE_ID_MAX_VALUE;
                if (classIDValid && classInstanceIDValid)
                {
                    singleElementUnpackedGroup = UnpackedGroup.create(groupID, _element.length);
                    if (singleElementUnpackedGroup != null)
                    {
                        singleElementUnpackedGroup.addElement(_element);
                        singleElementUnpackedGroup.seal();
                        validClass = true;
                    } else if (AmpException.areExceptionsEnabled())
                    {
                        throw new InvalidGroupException("Unable to make unpacked group.");
                    }

                } else if (AmpException.areExceptionsEnabled())
                {
                    if (!classIDValid)
                    {
                        throw new InvalidClassIDException(classID);
                    }

                    if (!classInstanceIDValid)
                    {
                        throw new InvalidClassInstanceIDException(classInstanceID);
                    }
                }

            } else if (AmpException.areExceptionsEnabled())
            {
                throw new UnsupportedElementSizeException(_element.length);
            }

        } else if (AmpException.areExceptionsEnabled())
        {
            if (_groupID == null)
            {
                throw new BadArgumentException("Null _groupID argument was passed into AC_SingleElement().");
            }

            if (_element == null)
            {
                throw new BadArgumentException("Null _element argument was passed into AC_SingleElement().");
            }
        }
    }

    public static AC_SingleElement create(GroupID _groupID, byte[] _element)
    {
        AC_SingleElement tempAC_SingleElement = new AC_SingleElement(_groupID, _element);
        if (tempAC_SingleElement.isValidClass())
        {
            return tempAC_SingleElement;
        }
        return null;
    }

    public static AC_SingleElement create(GroupID _groupID, byte _element)
    {
        byte[] bytes = {_element};

        return create(_groupID, bytes);
    }

    public static AC_SingleElement create(GroupID _groupID, boolean _element)
    {
        return create(_groupID, ByteTools.deconstructBoolean(_element));
    }

    public static AC_SingleElement create(GroupID _groupID, short _element)
    {
        byte[] bytes = ByteTools.deconstructShort(_element);

        return create(_groupID, bytes);
    }

    public static AC_SingleElement create(GroupID _groupID, char _element)
    {
        byte[] bytes = ByteTools.deconstructChar(_element);

        return create(_groupID, bytes);
    }

    public static AC_SingleElement create(GroupID _groupID, int _element)
    {
        byte[] bytes = ByteTools.deconstructInt(_element);

        return create(_groupID, bytes);
    }

    public static AC_SingleElement create(GroupID _groupID, float _element)
    {
        byte[] bytes = ByteTools.deconstructFloat(_element);

        return create(_groupID, bytes);
    }

    public static AC_SingleElement create(GroupID _groupID, long _element)
    {
        byte[] bytes = ByteTools.deconstructLong(_element);

        return create(_groupID, bytes);
    }

    public static AC_SingleElement create(GroupID _groupID, double _element)
    {
        byte[] bytes = ByteTools.deconstructDouble(_element);

        return create(_groupID, bytes);
    }

    public static AC_SingleElement create(GroupID _groupID, String _element)
    {
        return create(_groupID, _element.getBytes(AmpConstants.UTF8));
    }

    public static AC_SingleElement create(GroupID _groupID, String _element, String _charsetName) throws UnsupportedEncodingException
    {
        return create(_groupID, _element.getBytes(_charsetName));
    }

    public static AC_SingleElement create(GroupID _groupID, BigInteger _element)
    {
        return create(_groupID, _element.toByteArray());
    }

    public static AC_SingleElement create(GroupID _groupID, IAmpByteSerializable _element)
    {
        return create(_groupID, _element.serializeToBytes());
    }

    public static AC_SingleElement create(GroupID _groupID, IAmpAmpletSerializable _element)
    {
        if (ByteTools.isClassIDAmplified(_groupID.getClassID()))
        {
            Amplet tempAmp = _element.serializeToAmplet();
            if (tempAmp != null)
            {
                return create(_groupID, tempAmp.serializeToBytes());
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidAmpletException("Failed to create amplet.");
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new ClassIDNotAmplifiedException();
        }

        return null;
    }
    //endregion

    //region Getters
    public GroupID getGroupID()
    {
        return groupID;
    }

    @Override
    public long getClassID()
    {
        return classID;
    }

    public long getClassInstanceID()
    {
        return classInstanceID;
    }

    public byte[] getElement()
    {
        if (validClass)
        {
            return singleElementUnpackedGroup.getElement(0).clone();
        }
        return null;
    }

    @Override
    public ArrayList<UnpackedGroup> getUnpackedGroups()
    {
        if (validClass)
        {
            ArrayList<UnpackedGroup> tempList = new ArrayList<>();

            //we already know this group will be successfully made because we passed the validClass check.
            UnpackedGroup tempGroup = UnpackedGroup.create(groupID, elementLength);
            tempGroup.addElement(getElement());
            tempGroup.seal();

            tempList.add(tempGroup);

            return tempList;
        }
        return null;
    }

    @Override
    public boolean isValidClass()
    {
        return validClass;
    }
    //endregion

    //region Serialization
    @Override
    public Amplet serializeToAmplet()
    {
        if (validClass)
        {
            return Amplet.create(this);
        }
        return null;
    }
    //endregion
}
