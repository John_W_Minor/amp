package amp.classification.classes;

import amp.AmpConstants;
import amp.ByteTools;
import amp.Amplet;
import amp.exceptions.AmpException;
import amp.exceptions.ClassIDNotAmplifiedException;
import amp.exceptions.InvalidAmpletException;
import amp.exceptions.InvalidClassIDException;
import amp.group_ids.GroupID;
import amp.group_primitives.UnpackedGroup;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;

public class AC_ClassInstanceIDIsByteFootprint implements IAmpClass, IAmpAmpletSerializable
{
    //THIS AC DOES NOT GUARANTEE THE INPUT ORDER IS THE OUTPUT ORDER
    //THIS AC WAS DESIGNED BEFORE HAS, HPAS, AND HPSES WERE DEVELOPED
    //DEPENDING ON HOW MUCH THE BYTE FOOTPRINT OF YOUR DATA ELEMENTS VARIES
    //YOU SHOULD CONSIDER USING HAS OR HPSES INSTEAD
    //HOWEVER THIS IS AN EXTREMELY EFFICIENT WAY TO STORE AND MANIPULATE DATA
    //IF THE BYTE FOOTPRINTS OF YOUR DATA ELEMENTS HAVE LITTLE VARIANCE

    //region Member Fields
    private long classID;
    private String name;

    private ArrayList<UnpackedGroup> unpackedGroups = new ArrayList<>();

    private boolean validClass;
    //endregion

    //region Constructors
    private AC_ClassInstanceIDIsByteFootprint(long _classID, String _name)
    {
        classID = _classID;
        name = _name;

        validClass = classID > -1 && classID < AmpConstants.CLASS_ID_MAX_VALUE;

        if (!validClass && AmpException.areExceptionsEnabled())
        {
            throw new InvalidClassIDException(classID);
        }
    }

    public static AC_ClassInstanceIDIsByteFootprint create(long _classID, String _name)
    {
        AC_ClassInstanceIDIsByteFootprint tempAC = new AC_ClassInstanceIDIsByteFootprint(_classID, _name);
        if (tempAC.isValidClass())
        {
            return tempAC;
        }
        return null;
    }
    //endregion

    //region Adders
    public boolean addElement(byte[] _element)
    {
        if (_element != null)
        {
            int byteCount = _element.length;

            if (byteCount > 0 && byteCount < AmpConstants.ELEMENT_MAX_BYTE_FOOTPRINT)
            {

                UnpackedGroup temp = null;

                for (UnpackedGroup unpacked : unpackedGroups)
                {
                    if (unpacked.getHeader().getClassInstanceID() == byteCount)
                    {
                        temp = unpacked;
                    }
                }

                if (temp == null)
                {
                    GroupID tempGroupID = new GroupID(classID, byteCount, name, true, true);

                    temp = UnpackedGroup.create(tempGroupID, byteCount);

                    if (temp == null)
                    {
                        return false;
                    }

                    unpackedGroups.add(temp);
                }

                temp.addElement(_element);
                return true;
            }
        }
        return false;
    }

    public boolean addElement(byte _element)
    {
        byte[] bytes = {_element};

        return addElement(bytes);
    }

    public boolean addElement(boolean _element)
    {
        byte[] bytes = ByteTools.deconstructBoolean(_element);

        return addElement(bytes);
    }

    public boolean addElement(short _element)
    {
        byte[] bytes = ByteTools.deconstructShort(_element);

        return addElement(bytes);
    }

    public boolean addElement(char _element)
    {
        byte[] bytes = ByteTools.deconstructChar(_element);

        return addElement(bytes);
    }

    public boolean addElement(int _element)
    {
        byte[] bytes = ByteTools.deconstructInt(_element);

        return addElement(bytes);
    }

    public boolean addElement(float _element)
    {
        byte[] bytes = ByteTools.deconstructFloat(_element);

        return addElement(bytes);
    }

    public boolean addElement(long _element)
    {
        byte[] bytes = ByteTools.deconstructLong(_element);

        return addElement(bytes);
    }

    public boolean addElement(double _element)
    {
        byte[] bytes = ByteTools.deconstructDouble(_element);

        return addElement(bytes);
    }

    public boolean addElement(String _element)
    {
        return addElement(_element.getBytes(AmpConstants.UTF8));
    }

    public boolean addElement(String _element, String _charsetName) throws UnsupportedEncodingException
    {
        return addElement(_element.getBytes(_charsetName));
    }

    public boolean addElement(BigInteger _element)
    {
        return addElement(_element.toByteArray());
    }

    public boolean addElement(IAmpByteSerializable _element)
    {
        return addElement(_element.serializeToBytes());
    }

    public boolean addElement(IAmpAmpletSerializable _element)
    {
        if (ByteTools.isClassIDAmplified(classID))
        {
            Amplet tempAmp = _element.serializeToAmplet();
            if (tempAmp != null)
            {
                return addElement(tempAmp.serializeToBytes());
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidAmpletException("Failed to create amplet.");
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new ClassIDNotAmplifiedException();
        }

        return false;
    }
    //endregion

    //region Getters
    @Override
    public long getClassID()
    {
        return classID;
    }

    @Override
    public ArrayList<UnpackedGroup> getUnpackedGroups()
    {
        return unpackedGroups;
    }

    @Override
    public boolean isValidClass()
    {
        return validClass;
    }
    //endregion

    //region Serialization
    @Override
    public Amplet serializeToAmplet()
    {
        return Amplet.create(this);
    }
    //endregion
}
