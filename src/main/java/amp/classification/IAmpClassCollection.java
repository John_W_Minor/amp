package amp.classification;

import amp.Amplet;
import amp.group_primitives.UnpackedGroup;

import java.util.ArrayList;

public interface IAmpClassCollection
{
    ArrayList<UnpackedGroup> getUnpackedGroups();

    Amplet serializeToAmplet();
}
