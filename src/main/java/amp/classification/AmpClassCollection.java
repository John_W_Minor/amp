package amp.classification;

import amp.Amplet;
import amp.classification.classes.IAmpClass;
import amp.group_primitives.UnpackedGroup;
import amp.serialization.IAmpAmpletSerializable;

import java.util.ArrayList;

public class AmpClassCollection implements IAmpClassCollection, IAmpAmpletSerializable
{
    private ArrayList<IAmpClass> ampClasses = new ArrayList<>();

    public boolean addClass(IAmpClass _ampClass)
    {
        if (_ampClass != null)
        {
            boolean success = true;

            ArrayList<UnpackedGroup> storedUnpackedGroups = getUnpackedGroups();
            ArrayList<UnpackedGroup> candidateUnpackedGroups = _ampClass.getUnpackedGroups();

            for (UnpackedGroup candidateGroup : candidateUnpackedGroups)
            {
                long candidateClassID = candidateGroup.getHeader().getClassID();
                long candidateClassInstanceID = candidateGroup.getHeader().getClassInstanceID();

                for (UnpackedGroup storedGroup : storedUnpackedGroups)
                {
                    long storedClassID = storedGroup.getHeader().getClassID();
                    long storedClassInstanceID = storedGroup.getHeader().getClassInstanceID();

                    if (candidateClassID == storedClassID && candidateClassInstanceID == storedClassInstanceID)
                    {
                        success = false;
                        break;
                    }
                }
            }

            if (success)
            {
                ampClasses.add(_ampClass);

                return true;
            }
        }
        return false;
    }

    @Override
    public ArrayList<UnpackedGroup> getUnpackedGroups()
    {
        ArrayList<UnpackedGroup> unpackedGroups = new ArrayList<>();

        for (IAmpClass ac : ampClasses)
        {
            ArrayList<UnpackedGroup> temp = ac.getUnpackedGroups();

            unpackedGroups.addAll(temp);
        }

        return unpackedGroups;
    }

    @Override
    public Amplet serializeToAmplet()
    {
        return Amplet.create(this);
    }
}
