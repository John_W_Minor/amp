package amp.headless_prefixing_strategies;

import amp.backing_lists.GrowModeEnum;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public interface IHeadlessPrefixingStrategyWritable extends IHeadlessPrefixingStrategyReadable
{
    //region List Features
    int getCurrentSize();

    void ensureCapacity(int _minCapacity);

    void setGrowMode(GrowModeEnum _growMode);

    GrowModeEnum getGrowMode();

    void trimToSize();
    //endregion

    //region Adders
    boolean addBytes(byte[] _bytes);

    boolean addElement(byte _element);

    boolean addElement(boolean _element);

    boolean addElement(short _element);

    boolean addElement(char _element);

    boolean addElement(int _element);

    boolean addElement(float _element);

    boolean addElement(long _element);

    boolean addElement(double _element);

    boolean addElement(String _element);

    boolean addElement(String _element, String _charsetName) throws UnsupportedEncodingException;

    boolean addElement(BigInteger _element);

    boolean addElement(IAmpByteSerializable _element);

    boolean addElement(IAmpAmpletSerializable _element);
    //endregion

    //region Body Travellers
    boolean deleteNextElement();
    //endregion
}
