package amp.headless_prefixing_strategies;

import amp.serialization.IAmpByteSerializable;
import amp.serialization.IAmpDeserializable;
import amp.typed_data.ITypedData;

import java.util.List;

public interface IHeadlessPrefixingStrategyReadable extends IAmpByteSerializable, IAmpDeserializable
{
    //region Getters
    int getByteFootprint();

    boolean isValid();
    //endregion

    //region Cursor Position
    void resetCursor();

    void setCursor(int _index);

    int getCursor();

    int getMaxCursorPosition();
    //endregion

    //region Body Travellers
    ITypedData getNextElement();

    ITypedData peekNextElement();

    boolean hasNextElement();

    boolean skipNextElement();

    int getNumberOfElements();

    boolean travelToElementIndex(int _elementIndex);

    ITypedData travelToElementIndexAndGetElement(int _elementIndex);

    ITypedData getElementAtElementIndexNoTravel(int _elementIndex);

    List<ITypedData> getAllElements();
    //endregion
}
