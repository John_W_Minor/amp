package amp.headless_prefixing_strategies.strategies.key_value_hps;

import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.exceptions.TypedDataMismatchedTypeException;
import amp.headless_amplet.HeadlessAmpletTwinReadable;
import amp.typed_data.AbstractTypedData;
import amp.typed_data.IDataType;
import amp.typed_data.ITypedData;

class KeyValueHPSTypedData extends AbstractTypedData
{
    private final KeyValueHPSTypeEnum type;

    private KeyValueHPSTypedData(byte[] _data, KeyValueHPSTypeEnum _type)
    {
        type = _type;

        if (type == null)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _type argument was passed into KeyValueHPSTypedData().");
            }
            return;
        }

        if (type == KeyValueHPSTypeEnum.NULL_TYPE)
        {
            if (_data != null)
            {
                valid = false;
                if (AmpException.areExceptionsEnabled())
                {
                    throw new TypedDataMismatchedTypeException(type.name());
                }
            }
            return;
        }

        if (_data == null)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _data argument was passed into KeyValueHPSTypedData(), and the given type wasn't NULL_TYPE.");
            }
            return;
        }

        data = HeadlessAmpletTwinReadable.create(_data);
    }

    static ITypedData create(byte[] _data, KeyValueHPSTypeEnum _type)
    {
        ITypedData tempData = new KeyValueHPSTypedData(_data, _type);
        if (tempData.isValid())
        {
            return tempData;
        }
        return null;
    }

    @Override
    public IDataType getType()
    {
        return type.getType();
    }
}
