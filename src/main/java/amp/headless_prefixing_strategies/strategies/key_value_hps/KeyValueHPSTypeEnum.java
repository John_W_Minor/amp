package amp.headless_prefixing_strategies.strategies.key_value_hps;

import amp.typed_data.defaults.DefaultDataTypeEnum;
import amp.typed_data.IDataType;

enum KeyValueHPSTypeEnum
{
    NULL_TYPE(KeyValueHPSConstants.NULL_PREFIX_CODE, DefaultDataTypeEnum.NULL_TYPE),
    KEY_TYPE(null, DefaultDataTypeEnum.KEY_TYPE),
    VALUE_TYPE(null, DefaultDataTypeEnum.VALUE_TYPE);

    private final Byte prefixCode;
    private final IDataType type;

    KeyValueHPSTypeEnum(Byte _prefixCode, IDataType _type)
    {
        prefixCode = _prefixCode;
        type = _type;
    }

    Byte getPrefixCode()
    {
        return prefixCode;
    }

    IDataType getType()
    {
        return type;
    }
}
