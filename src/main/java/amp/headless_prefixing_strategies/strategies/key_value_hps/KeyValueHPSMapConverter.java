package amp.headless_prefixing_strategies.strategies.key_value_hps;

import amp.AmpConstants;
import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.exceptions.KeyWithoutValueException;
import amp.exceptions.UnexpectedTypeException;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyWritable;
import amp.serialization.IAmpByteFactory;
import amp.serialization.IAmpDeserializable;
import amp.typed_data.defaults.DefaultDataTypeEnum;
import amp.typed_data.ITypedData;
import jdk.nashorn.internal.ir.annotations.Ignore;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class KeyValueHPSMapConverter<V extends IAmpDeserializable>
{
    private IAmpByteFactory factory;
    private boolean acceptNullData;

    public KeyValueHPSMapConverter(IAmpByteFactory _factory, Class<V> _factoryOutput, boolean _acceptNullData)
    {
        factory = _factory;
        if (factory == null)
        {
            throw new BadArgumentException("Null _factory argument was passed into KeyValueHPSMapConverter().");
        }

        if (_factoryOutput == null)
        {
            throw new BadArgumentException("Null _factoryOutput argument was passed into KeyValueHPSMapConverter().");
        }

        Class factoryOutputOne = null;
        Class factoryOutputTwo = null;

        try
        {
            factoryOutputOne = _factory.getClass().getMethod("build", byte[].class).getReturnType();
            factoryOutputTwo = _factory.getClass().getMethod("build", ITypedData.class).getReturnType();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        if(factoryOutputOne != factoryOutputTwo)
        {
            throw new BadArgumentException("_factory argument's return types do not match each other in KeyValueHPSMapConverter().");
        }

        if(factoryOutputOne != _factoryOutput || factoryOutputTwo != _factoryOutput)
        {
            throw new BadArgumentException("_factory argument's return types do not match the declared _factoryOutput return type in KeyValueHPSMapConverter().");
        }

        acceptNullData = _acceptNullData;
    }

    public Map<String, V> deserializeMap(byte[] _bytes, Map<String, V> _map)
    {
        if (_bytes == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _bytes argument was passed into deserializeMap().");
            }
            return null;
        }

        if (_map == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _map argument was passed into deserializeMap().");
            }
            return null;
        }

        IHeadlessPrefixingStrategyReadable keyValueHPSReadable = KeyValueHPSReadable.create(_bytes);
        if (keyValueHPSReadable == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("The _bytes argument passed into deserializeMap() could not be converted into a KeyValueHPSWritable Object.");
            }
            return null;
        }

        return deserializeMap(keyValueHPSReadable, _map);
    }

    public Map<String, V> deserializeMap(ITypedData _data, Map<String, V> _map)
    {
        if (_data == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _data argument was passed into deserializeMap().");
            }
            return null;
        }

        if (_map == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _map argument was passed into deserializeMap().");
            }
            return null;
        }

        IHeadlessPrefixingStrategyReadable keyValueHPSReadable = KeyValueHPSReadable.createTwin(_data);
        if (keyValueHPSReadable == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("The _data argument passed into deserializeMap() could not be converted into a KeyValueHPSWritable Object.");
            }
            return null;
        }

        return deserializeMap(keyValueHPSReadable, _map);
    }

    private Map<String, V> deserializeMap(IHeadlessPrefixingStrategyReadable _keyValueHPSReadable, Map<String, V> _map)
    {
        List<ITypedData> dataList = _keyValueHPSReadable.getAllElements();

        int size = dataList.size();

        if (size % 2 != 0)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new KeyWithoutValueException();
            }
            return null;
        }

        int numElements = size / 2;

        for (int i = 0; i < numElements; i++)
        {
            int currentIndex = i * 2;

            ITypedData keyData = dataList.get(currentIndex);
            ITypedData valueData = dataList.get(currentIndex + 1);

            if (keyData.getType() != DefaultDataTypeEnum.KEY_TYPE)
            {
                if (AmpException.areExceptionsEnabled())
                {
                    throw new UnexpectedTypeException(keyData.getType(), "deserializeMap()");
                }
                return null;
            }

            if (valueData.getType() != DefaultDataTypeEnum.NULL_TYPE && valueData.getType() != DefaultDataTypeEnum.VALUE_TYPE)
            {
                if (AmpException.areExceptionsEnabled())
                {
                    throw new UnexpectedTypeException(valueData.getType(), "deserializeMap()");
                }
                return null;
            }

            if (valueData.getType() == DefaultDataTypeEnum.NULL_TYPE && !acceptNullData)
            {

                if (AmpException.areExceptionsEnabled())
                {
                    throw new UnexpectedTypeException(valueData.getType(), "deserializeMap() because nulls were disallowed");
                }
                return null;
            }

            String key = new String(keyData.getData(), AmpConstants.UTF8);
            //We know this won't cause problems because we can't reach this code
            //without passing the type checks in the constructor.
            V value = (V) factory.build(valueData);

            _map.put(key, value);
        }

        return _map;
    }

    public byte[] serializeMap(Map<String, V> _map)
    {
        if (_map == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _map argument was passed into serializeMap().");
            }
            return null;
        }

        IHeadlessPrefixingStrategyWritable keyValueHPSWritable = KeyValueHPSWritable.create();

        Set<String> keys = _map.keySet();

        for (String key : keys)
        {
            if(key == null)
            {
                if (AmpException.areExceptionsEnabled())
                {
                    throw new BadArgumentException("The _map argument passed into serializeMap() had a null key.");
                }
            }

            V value = _map.get(key);

            if (value == null && !acceptNullData)
            {
                if (AmpException.areExceptionsEnabled())
                {
                    throw new BadArgumentException("The _map argument passed into serializeMap() had a null element when nulls were disallowed.");
                }
                return null;
            }

            byte[] bytes = null;

            if (value != null)
            {
                bytes = value.serializeToBytes();
            }

            keyValueHPSWritable.addElement(key);
            keyValueHPSWritable.addBytes(bytes);
        }

        return keyValueHPSWritable.serializeToBytes();
    }
}
