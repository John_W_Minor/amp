package amp.headless_prefixing_strategies.strategies.key_value_hps;

class KeyValueHPSConstants
{
    //Formats built on top of HAs should have version numbers greater than 4
    //This is an easy way to ensure there are no collisions with HPAs.
    static final char KEY_VALUE_HPS_VERSION = 30_000;

    static final byte KEY_ONE_BYTE_SECOND_PREFIX_CODE = 0;
    static final byte KEY_TWO_BYTE_SECOND_PREFIX_CODE = 1;
    static final byte KEY_FOUR_BYTE_SECOND_PREFIX_CODE = 2;

    static final int KEY_SINGLE_PREFIX_OFFSET = KEY_FOUR_BYTE_SECOND_PREFIX_CODE + 1;
    static final int KEY_SINGLE_PREFIX_MAX_FOOTPRINT = 255 - KEY_SINGLE_PREFIX_OFFSET;

    static final byte NULL_PREFIX_CODE = 0;
    static final byte VALUE_ONE_BYTE_SECOND_PREFIX_CODE = 1;
    static final byte VALUE_TWO_BYTE_SECOND_PREFIX_CODE = 2;
    static final byte VALUE_FOUR_BYTE_SECOND_PREFIX_CODE = 3;

    static final int VALUE_SINGLE_PREFIX_OFFSET = VALUE_FOUR_BYTE_SECOND_PREFIX_CODE + 1;
    static final int VALUE_SINGLE_PREFIX_MAX_FOOTPRINT = 255 - VALUE_SINGLE_PREFIX_OFFSET;
}
