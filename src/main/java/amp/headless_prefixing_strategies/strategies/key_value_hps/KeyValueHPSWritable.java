package amp.headless_prefixing_strategies.strategies.key_value_hps;

import amp.exceptions.*;
import amp.headless_prefixing_strategies.AbstractHPSWritable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyWritable;
import amp.headless_prefixing_strategies.strategies.StrategyHelper;
import amp.typed_data.ITypedData;

class KeyValueHPSWritable extends AbstractHPSWritable
{
    private boolean isKeyRead = true;
    private boolean isKeyWrite = true;

    //region Constructors
    private KeyValueHPSWritable()
    {
        super();

        writableHamplet.addElement(KeyValueHPSConstants.KEY_VALUE_HPS_VERSION);
        setCursor(0);
    }

    private KeyValueHPSWritable(byte[] _bytes)
    {
        super(_bytes);

        if (!valid)
        {
            return;
        }

        Character version = readableHamplet.getNextCharacter();

        if (version == null || version != KeyValueHPSConstants.KEY_VALUE_HPS_VERSION)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("KeyValueHPSWritable");
            }
        }
    }

    public static IHeadlessPrefixingStrategyWritable create()
    {
        return new KeyValueHPSWritable();
    }

    public static IHeadlessPrefixingStrategyWritable create(byte[] _bytes)
    {
        IHeadlessPrefixingStrategyWritable keyValueHPSWritable = new KeyValueHPSWritable(_bytes);
        if (keyValueHPSWritable.isValid())
        {
            return keyValueHPSWritable;
        }
        return null;
    }
    //endregion

    //region Adders
    @Override
    public boolean addBytes(byte[] _bytes)
    {
        if (isKeyWrite)
        {
            if (_bytes == null)
            {
                if (AmpException.areExceptionsEnabled())
                {
                    throw new BadArgumentException("Null _bytes argument was passed into addBytes().");
                }
                return false;
            }

            boolean returnVal = addBytesOfKeyType(_bytes);
            if (returnVal)
            {
                isKeyWrite = !isKeyWrite;
            }
            return returnVal;
        }

        boolean returnVal = addBytesOfValueType(_bytes);
        if (returnVal)
        {
            isKeyWrite = !isKeyWrite;
        }
        return returnVal;
    }

    //region addBytes() Helpers

    private boolean addBytesOfKeyType(byte[] _bytes)
    {
        int size = _bytes.length;

        StrategyHelper.insertSizePrefixing(size,
                KeyValueHPSConstants.KEY_SINGLE_PREFIX_MAX_FOOTPRINT,
                KeyValueHPSConstants.KEY_SINGLE_PREFIX_OFFSET,
                KeyValueHPSConstants.KEY_ONE_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.KEY_TWO_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.KEY_FOUR_BYTE_SECOND_PREFIX_CODE,
                writableHamplet);

        writableHamplet.addBytes(_bytes);

        return true;
    }

    private boolean addBytesOfValueType(byte[] _bytes)
    {
        if (_bytes == null)
        {
            writableHamplet.addElement(KeyValueHPSTypeEnum.NULL_TYPE.getPrefixCode());
            return true;
        }

        int size = _bytes.length;

        StrategyHelper.insertSizePrefixing(size,
                KeyValueHPSConstants.VALUE_SINGLE_PREFIX_MAX_FOOTPRINT,
                KeyValueHPSConstants.VALUE_SINGLE_PREFIX_OFFSET,
                KeyValueHPSConstants.VALUE_ONE_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.VALUE_TWO_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.VALUE_FOUR_BYTE_SECOND_PREFIX_CODE,
                writableHamplet);

        writableHamplet.addBytes(_bytes);

        return true;
    }
    //endregion
    //endregion

    //region Body Travellers
    @Override
    public ITypedData getNextElement()
    {
        int startingCursor = readableHamplet.getCursor();

        Integer firstPrefix = readableHamplet.getNextUnsignedByte();

        if (firstPrefix == null)
        {
            readableHamplet.setCursor(startingCursor);
            return null;
        }

        if (isKeyRead)
        {
            ITypedData returnData = returnKeyITypedData(startingCursor, firstPrefix);
            if (returnData != null)
            {
                isKeyRead = !isKeyRead;
            }
            return returnData;
        }

        if (firstPrefix.byteValue() == KeyValueHPSTypeEnum.NULL_TYPE.getPrefixCode())
        {
            ITypedData returnData = getNullITypedData();
            if (returnData != null)
            {
                isKeyRead = !isKeyRead;
            }
            return returnData;
        }


        ITypedData returnData = returnValueITypedData(startingCursor, firstPrefix);
        if (returnData != null)
        {
            isKeyRead = !isKeyRead;
        }
        return returnData;
    }

    //region getNextElement() Helpers
    private ITypedData returnKeyITypedData(int _startingCursor, int firstPrefix)
    {
        Integer size = StrategyHelper.readSizePrefixing(firstPrefix,
                KeyValueHPSConstants.KEY_SINGLE_PREFIX_OFFSET,
                KeyValueHPSConstants.KEY_ONE_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.KEY_TWO_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.KEY_FOUR_BYTE_SECOND_PREFIX_CODE,
                readableHamplet);

        if (size == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        byte[] data = readableHamplet.getNextNBytes(size);

        if (data == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        return KeyValueHPSTypedData.create(data, KeyValueHPSTypeEnum.KEY_TYPE);
    }

    private ITypedData getNullITypedData()
    {
        return KeyValueHPSTypedData.create(null, KeyValueHPSTypeEnum.NULL_TYPE);
    }

    private ITypedData returnValueITypedData(int _startingCursor, int firstPrefix)
    {
        Integer size = StrategyHelper.readSizePrefixing(firstPrefix,
                KeyValueHPSConstants.VALUE_SINGLE_PREFIX_OFFSET,
                KeyValueHPSConstants.VALUE_ONE_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.VALUE_TWO_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.VALUE_FOUR_BYTE_SECOND_PREFIX_CODE,
                readableHamplet);

        if (size == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        byte[] data = readableHamplet.getNextNBytes(size);

        if (data == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        return KeyValueHPSTypedData.create(data, KeyValueHPSTypeEnum.VALUE_TYPE);
    }
    //endregion

    //endregion
}
