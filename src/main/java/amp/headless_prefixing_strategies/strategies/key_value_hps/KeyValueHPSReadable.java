package amp.headless_prefixing_strategies.strategies.key_value_hps;

import amp.exceptions.AmpException;
import amp.exceptions.InvalidHeadlessPrefixingStrategyDataException;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_prefixing_strategies.AbstractHPSReadable;
import amp.headless_prefixing_strategies.strategies.StrategyHelper;
import amp.typed_data.ITypedData;

public class KeyValueHPSReadable extends AbstractHPSReadable
{
    private boolean isKeyRead = true;

    //region Constructors
    private KeyValueHPSReadable(byte[] _bytes)
    {
        super(_bytes);

        if (!valid)
        {
            return;
        }

        Character version = readableHamplet.getNextCharacter();

        if (version == null || version != KeyValueHPSConstants.KEY_VALUE_HPS_VERSION)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("KeyValueHPSReadable");
            }
        }
    }

    private KeyValueHPSReadable(IHeadlessAmpletTwinReadable _hamplet)
    {
        super(_hamplet);

        if (!valid)
        {
            return;
        }

        Character version = readableHamplet.getNextCharacter();

        if (version == null || version != KeyValueHPSConstants.KEY_VALUE_HPS_VERSION)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("KeyValueHPSReadable");
            }
        }
    }

    private KeyValueHPSReadable(ITypedData _data)
    {
        super(_data);

        if (!valid)
        {
            return;
        }

        Character version = readableHamplet.getNextCharacter();

        if (version == null || version != KeyValueHPSConstants.KEY_VALUE_HPS_VERSION)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("KeyValueHPSReadable");
            }
        }
    }

    public static KeyValueHPSReadable create(byte[] _bytes)
    {
        KeyValueHPSReadable keyValueHPSReadable = new KeyValueHPSReadable(_bytes);
        if (keyValueHPSReadable.isValid())
        {
            return keyValueHPSReadable;
        }
        return null;
    }

    public static KeyValueHPSReadable createTwin(IHeadlessAmpletTwinReadable _hamplet)
    {
        KeyValueHPSReadable keyValueHPSReadable = new KeyValueHPSReadable(_hamplet);
        if (keyValueHPSReadable.isValid())
        {
            return keyValueHPSReadable;
        }
        return null;
    }

    public static KeyValueHPSReadable createTwin(ITypedData _data)
    {
        KeyValueHPSReadable keyValueHPSReadable = new KeyValueHPSReadable(_data);
        if (keyValueHPSReadable.isValid())
        {
            return keyValueHPSReadable;
        }
        return null;
    }
    //endregion

    //region Body Travellers
    @Override
    public ITypedData getNextElement()
    {
        int startingCursor = readableHamplet.getCursor();

        Integer firstPrefix = readableHamplet.getNextUnsignedByte();

        if (firstPrefix == null)
        {
            readableHamplet.setCursor(startingCursor);
            return null;
        }

        if (isKeyRead)
        {
            ITypedData returnData = returnKeyITypedData(startingCursor, firstPrefix);
            if (returnData != null)
            {
                isKeyRead = !isKeyRead;
            }
            return returnData;
        }

        if (firstPrefix.byteValue() == KeyValueHPSTypeEnum.NULL_TYPE.getPrefixCode())
        {
            ITypedData returnData = getNullITypedData();
            if (returnData != null)
            {
                isKeyRead = !isKeyRead;
            }
            return returnData;
        }


        ITypedData returnData = returnValueITypedData(startingCursor, firstPrefix);
        if (returnData != null)
        {
            isKeyRead = !isKeyRead;
        }
        return returnData;
    }

    //region getNextElement() Helpers
    private ITypedData returnKeyITypedData(int _startingCursor, int firstPrefix)
    {
        Integer size = StrategyHelper.readSizePrefixing(firstPrefix,
                KeyValueHPSConstants.KEY_SINGLE_PREFIX_OFFSET,
                KeyValueHPSConstants.KEY_ONE_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.KEY_TWO_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.KEY_FOUR_BYTE_SECOND_PREFIX_CODE,
                readableHamplet);

        if (size == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        byte[] data = readableHamplet.getNextNBytes(size);

        if (data == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        return KeyValueHPSTypedData.create(data, KeyValueHPSTypeEnum.KEY_TYPE);
    }

    private ITypedData getNullITypedData()
    {
        return KeyValueHPSTypedData.create(null, KeyValueHPSTypeEnum.NULL_TYPE);
    }

    private ITypedData returnValueITypedData(int _startingCursor, int firstPrefix)
    {
        Integer size = StrategyHelper.readSizePrefixing(firstPrefix,
                KeyValueHPSConstants.VALUE_SINGLE_PREFIX_OFFSET,
                KeyValueHPSConstants.VALUE_ONE_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.VALUE_TWO_BYTE_SECOND_PREFIX_CODE,
                KeyValueHPSConstants.VALUE_FOUR_BYTE_SECOND_PREFIX_CODE,
                readableHamplet);

        if (size == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        byte[] data = readableHamplet.getNextNBytes(size);

        if (data == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        return KeyValueHPSTypedData.create(data, KeyValueHPSTypeEnum.VALUE_TYPE);
    }
    //endregion

    //endregion
}
