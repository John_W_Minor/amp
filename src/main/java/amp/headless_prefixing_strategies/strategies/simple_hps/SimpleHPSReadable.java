package amp.headless_prefixing_strategies.strategies.simple_hps;

import amp.ByteTools;
import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.exceptions.InvalidHeadlessPrefixingStrategyDataException;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_prefixing_strategies.AbstractHPSReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.factory.IHPSVersionedFactory;
import amp.typed_data.defaults.DefaultTypedData;
import amp.typed_data.ITypedData;

public class SimpleHPSReadable extends AbstractHPSReadable
{
    //region Constructors
    private SimpleHPSReadable(byte[] _bytes)
    {
        super(_bytes);

        if (!valid)
        {
            return;
        }

        prefixingOffset = 0;

        if (_bytes.length > 0 &&
                _bytes[0] != SimpleHPSConstants.SIMPLE_HPS_MIN_VERSION &&
                _bytes[0] != SimpleHPSConstants.SIMPLE_HPS_MIDDLE_VERSION &&
                _bytes[0] != SimpleHPSConstants.SIMPLE_HPS_MAX_VERSION)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("SimpleHPSReadable");
            }
            valid = false;
        }
    }

    private SimpleHPSReadable(IHeadlessAmpletTwinReadable _hamplet)
    {
        super(_hamplet);

        if (!valid)
        {
            return;
        }

        prefixingOffset = 0;

        Byte check = readableHamplet.getNextByte();
        resetCursor();

        if (check == null ||
                (check != SimpleHPSConstants.SIMPLE_HPS_MIN_VERSION &&
                        check != SimpleHPSConstants.SIMPLE_HPS_MIDDLE_VERSION &&
                        check != SimpleHPSConstants.SIMPLE_HPS_MAX_VERSION)
                )
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("SimpleHPSReadable");
            }
            valid = false;
        }
    }

    private SimpleHPSReadable(ITypedData _data)
    {
        super(_data);

        if (!valid)
        {
            return;
        }

        prefixingOffset = 0;

        Byte check = readableHamplet.getNextByte();
        resetCursor();

        if (check == null ||
                (check != SimpleHPSConstants.SIMPLE_HPS_MIN_VERSION &&
                        check != SimpleHPSConstants.SIMPLE_HPS_MIDDLE_VERSION &&
                        check != SimpleHPSConstants.SIMPLE_HPS_MAX_VERSION)
                )
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("SimpleHPSReadable");
            }
            valid = false;
        }
    }

    public static SimpleHPSReadable create(byte[] _bytes)
    {
        SimpleHPSReadable simpleHPSReadable = new SimpleHPSReadable(_bytes);
        if (simpleHPSReadable.isValid())
        {
            return simpleHPSReadable;
        }
        return null;
    }

    public static SimpleHPSReadable createTwin(IHeadlessAmpletTwinReadable _hamplet)
    {
        SimpleHPSReadable simpleHPSReadable = new SimpleHPSReadable(_hamplet);
        if (simpleHPSReadable.isValid())
        {
            return simpleHPSReadable;
        }
        return null;
    }

    public static SimpleHPSReadable createTwin(ITypedData _data)
    {
        SimpleHPSReadable simpleHPSReadable = new SimpleHPSReadable(_data);
        if (simpleHPSReadable.isValid())
        {
            return simpleHPSReadable;
        }
        return null;
    }
    //endregion

    //region Body Travellers
    @Override
    public ITypedData getNextElement()
    {
        int startingCursor = getCursor();

        Byte prefixSize = readableHamplet.getNextByte();

        if (prefixSize == null)
        {
            setCursor(startingCursor);
            return null;
        }

        if (prefixSize == SimpleHPSConstants.SIMPLE_FOUR_BYTE_SECOND_PREFIX_CODE)
        {
            Integer intSize = readableHamplet.getNextInt();

            if (intSize == null)
            {
                setCursor(startingCursor);
                return null;
            }

            byte[] tempBytes = readableHamplet.getNextNBytes(intSize);

            if (tempBytes == null)
            {
                return null;
            }

            return DefaultTypedData.create(tempBytes);
        } else if (prefixSize == SimpleHPSConstants.SIMPLE_TWO_BYTE_SECOND_PREFIX_CODE)
        {
            Character charSize = readableHamplet.getNextCharacter();

            if (charSize == null)
            {
                setCursor(startingCursor);
                return null;
            }

            byte[] tempBytes = readableHamplet.getNextNBytes(charSize);

            if (tempBytes == null)
            {
                return null;
            }

            return DefaultTypedData.create(tempBytes);
        } else if (prefixSize == SimpleHPSConstants.SIMPLE_ONE_BYTE_SECOND_PREFIX_CODE)
        {
            Byte byteSize = readableHamplet.getNextByte();

            if (byteSize == null)
            {
                setCursor(startingCursor);
                return null;
            }

            int size = ByteTools.buildUnsignedByte(byteSize);

            byte[] tempBytes = readableHamplet.getNextNBytes(size);

            if (tempBytes == null)
            {
                return null;
            }

            return DefaultTypedData.create(tempBytes);
        } else
        {
            setCursor(startingCursor);
            return null;
        }
    }
    //endregion

    //region Version Factory
    private static final IHPSVersionedFactory VERSIONED_FACTORY = new IHPSVersionedFactory()
    {
        @Override
        public IHeadlessPrefixingStrategyReadable build(byte[] _bytes)
        {
            return SimpleHPSReadable.create(_bytes);
        }

        @Override
        public IHeadlessPrefixingStrategyReadable build(ITypedData _data)
        {
            return SimpleHPSReadable.createTwin(_data);
        }

        @Override
        public IHeadlessPrefixingStrategyReadable buildTwin(IHeadlessAmpletTwinReadable _hamplet)
        {
            return SimpleHPSReadable.createTwin(_hamplet);
        }

        @Override
        public char getVersion()
        {
            return SimpleHPSConstants.SIMPLE_HPS_MAX_VERSION;
        }
    };

    public static IHPSVersionedFactory getVersionedFactory()
    {
        return VERSIONED_FACTORY;
    }
    //endregion

}
