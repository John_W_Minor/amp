package amp.headless_prefixing_strategies.strategies.simple_hps;

import amp.AmpConstants;
import amp.ByteTools;
import amp.exceptions.*;
import amp.headless_prefixing_strategies.AbstractHPSWritable;
import amp.typed_data.defaults.DefaultTypedData;
import amp.typed_data.ITypedData;

public class SimpleHPSWritable extends AbstractHPSWritable
{
    //region Constructors
    private SimpleHPSWritable()
    {
        super();

        prefixingOffset = 0;
    }

    private SimpleHPSWritable(byte[] _bytes)
    {
        super(_bytes);

        if (!valid)
        {
            return;
        }

        prefixingOffset = 0;

        if (_bytes.length > 0 &&
                _bytes[0] != SimpleHPSConstants.SIMPLE_HPS_MIN_VERSION &&
                _bytes[0] != SimpleHPSConstants.SIMPLE_HPS_MIDDLE_VERSION &&
                _bytes[0] != SimpleHPSConstants.SIMPLE_HPS_MAX_VERSION)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("SimpleHPSWritable");
            }
            valid = false;
        }
    }

    public static SimpleHPSWritable create()
    {
        return new SimpleHPSWritable();
    }

    public static SimpleHPSWritable create(byte[] _bytes)
    {
        SimpleHPSWritable simpleHPSWritable = new SimpleHPSWritable(_bytes);
        if (simpleHPSWritable.isValid())
        {
            return simpleHPSWritable;
        }
        return null;
    }
    //endregion

    //region Adders
    public boolean addBytes(byte[] _bytes)
    {
        if (_bytes == null)
        {
            writableHamplet.addElement((byte) 1);
            writableHamplet.addElement((byte) 0);

            return true;
        }

        int size = _bytes.length;

        if (size == 0)
        {
            writableHamplet.addElement((byte) 1);
            writableHamplet.addElement((byte) 0);

            return true;
        }

        byte prefixSize;

        if (size < AmpConstants.UBYTE_MAX_VALUE)
        {
            prefixSize = SimpleHPSConstants.SIMPLE_ONE_BYTE_SECOND_PREFIX_CODE;
        } else if (size < AmpConstants.USHORT_MAX_VALUE)
        {
            prefixSize = SimpleHPSConstants.SIMPLE_TWO_BYTE_SECOND_PREFIX_CODE;
        } else
        {
            prefixSize = SimpleHPSConstants.SIMPLE_FOUR_BYTE_SECOND_PREFIX_CODE;
        }

        writableHamplet.addElement(prefixSize);

        if (prefixSize == SimpleHPSConstants.SIMPLE_FOUR_BYTE_SECOND_PREFIX_CODE)
        {
            writableHamplet.addElement(size);
        } else if (prefixSize == SimpleHPSConstants.SIMPLE_TWO_BYTE_SECOND_PREFIX_CODE)
        {
            writableHamplet.addElement((short) size);
        } else// if (prefixSize == SimpleHPSConstants.SIMPLE_ONE_BYTE_SECOND_PREFIX_CODE)
        {
            writableHamplet.addElement((byte) size);
        }

        writableHamplet.addBytes(_bytes);

        return true;
    }
    //endregion

    //region Body Travellers
    @Override
    public ITypedData getNextElement()
    {
        int startingCursor = getCursor();

        Byte prefixSize = readableHamplet.getNextByte();

        if (prefixSize == null)
        {
            setCursor(startingCursor);
            return null;
        }

        if (prefixSize == 4)
        {
            Integer intSize = readableHamplet.getNextInt();

            if (intSize == null)
            {
                setCursor(startingCursor);
                return null;
            }

            byte[] tempBytes = readableHamplet.getNextNBytes(intSize);

            if (tempBytes == null)
            {
                return null;
            }

            return DefaultTypedData.create(tempBytes);
        } else if (prefixSize == 2)
        {
            Character charSize = readableHamplet.getNextCharacter();

            if (charSize == null)
            {
                setCursor(startingCursor);
                return null;
            }

            byte[] tempBytes = readableHamplet.getNextNBytes(charSize);

            if (tempBytes == null)
            {
                return null;
            }

            return DefaultTypedData.create(tempBytes);
        } else if (prefixSize == 1)
        {
            Byte byteSize = readableHamplet.getNextByte();

            if (byteSize == null)
            {
                setCursor(startingCursor);
                return null;
            }

            int size = ByteTools.buildUnsignedByte(byteSize);

            byte[] tempBytes = readableHamplet.getNextNBytes(size);

            if (tempBytes == null)
            {
                return null;
            }

            return DefaultTypedData.create(tempBytes);
        } else
        {
            setCursor(startingCursor);
            return null;
        }
    }
    //endregion
}
