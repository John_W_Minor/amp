package amp.headless_prefixing_strategies.strategies.simple_hps;

class SimpleHPSConstants
{
    static final byte SIMPLE_ONE_BYTE_SECOND_PREFIX_CODE = 1;
    static final byte SIMPLE_TWO_BYTE_SECOND_PREFIX_CODE = 2;
    static final byte SIMPLE_FOUR_BYTE_SECOND_PREFIX_CODE = 4;

    static final char SIMPLE_HPS_MIN_VERSION = SIMPLE_ONE_BYTE_SECOND_PREFIX_CODE;
    static final char SIMPLE_HPS_MIDDLE_VERSION = SIMPLE_TWO_BYTE_SECOND_PREFIX_CODE;
    static final char SIMPLE_HPS_MAX_VERSION = SIMPLE_FOUR_BYTE_SECOND_PREFIX_CODE;
}
