package amp.headless_prefixing_strategies.strategies.complex_hps;

import amp.AmpConstants;
import amp.typed_data.defaults.DefaultDataTypeEnum;
import amp.typed_data.IDataType;

enum ComplexHPSTypeEnum
{
    NULL_TYPE(null, ComplexHPSConstants.NULL_PREFIX_CODE, DefaultDataTypeEnum.NULL_TYPE),
    BOOLEAN_TYPE(AmpConstants.BOOLEAN_BYTE_FOOTPRINT, ComplexHPSConstants.BOOLEAN_PREFIX_CODE, DefaultDataTypeEnum.BOOLEAN_TYPE),
    BYTE_TYPE(AmpConstants.BYTE_BYTE_FOOTPRINT, ComplexHPSConstants.BYTE_PREFIX_CODE, DefaultDataTypeEnum.BYTE_TYPE),
    SHORT_TYPE(AmpConstants.SHORT_BYTE_FOOTPRINT, ComplexHPSConstants.SHORT_PREFIX_CODE, DefaultDataTypeEnum.SHORT_TYPE),
    CHAR_TYPE(AmpConstants.CHAR_BYTE_FOOTPRINT, ComplexHPSConstants.CHAR_PREFIX_CODE, DefaultDataTypeEnum.CHAR_TYPE),
    INT_TYPE(AmpConstants.INT_BYTE_FOOTPRINT, ComplexHPSConstants.INT_PREFIX_CODE, DefaultDataTypeEnum.INT_TYPE),
    FLOAT_TYPE(AmpConstants.FLOAT_BYTE_FOOTPRINT, ComplexHPSConstants.FLOAT_PREFIX_CODE, DefaultDataTypeEnum.FLOAT_TYPE),
    LONG_TYPE(AmpConstants.LONG_BYTE_FOOTPRINT, ComplexHPSConstants.LONG_PREFIX_CODE, DefaultDataTypeEnum.LONG_TYPE),
    DOUBLE_TYPE(AmpConstants.DOUBLE_BYTE_FOOTPRINT, ComplexHPSConstants.DOUBLE_PREFIX_CODE, DefaultDataTypeEnum.DOUBLE_TYPE),
    COMPLEX_TYPE(null, null, DefaultDataTypeEnum.COMPLEX_TYPE);

    private final Integer expectedSize;
    private final Byte prefixCode;
    private final IDataType type;

    ComplexHPSTypeEnum(Integer _expectedSize, Byte _prefixCode, IDataType _type)
    {
        expectedSize = _expectedSize;
        prefixCode = _prefixCode;
        type = _type;
    }

    Integer getExpectedSize()
    {
        return expectedSize;
    }

    Byte getPrefixCode()
    {
        return prefixCode;
    }

    IDataType getType()
    {
        return type;
    }
}

