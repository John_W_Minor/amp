package amp.headless_prefixing_strategies.strategies.complex_hps;

import amp.exceptions.*;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_prefixing_strategies.AbstractHPSReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.factory.IHPSVersionedFactory;
import amp.headless_prefixing_strategies.strategies.StrategyHelper;
import amp.typed_data.ITypedData;

public class ComplexHPSReadable extends AbstractHPSReadable
{
    //region Constructors
    private ComplexHPSReadable(byte[] _bytes)
    {
        super(_bytes);

        if (!valid)
        {
            return;
        }

        Character version = readableHamplet.getNextCharacter();

        if (version == null || version != ComplexHPSConstants.COMPLEX_HPS_VERSION)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("ComplexHPSReadable");
            }
        }
    }

    private ComplexHPSReadable(IHeadlessAmpletTwinReadable _hamplet)
    {
        super(_hamplet);

        if (!valid)
        {
            return;
        }

        Character version = readableHamplet.getNextCharacter();

        if (version == null || version != ComplexHPSConstants.COMPLEX_HPS_VERSION)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("ComplexHPSReadable");
            }
        }
    }

    private ComplexHPSReadable(ITypedData _data)
    {
        super(_data);

        if (!valid)
        {
            return;
        }

        Character version = readableHamplet.getNextCharacter();

        if (version == null || version != ComplexHPSConstants.COMPLEX_HPS_VERSION)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("ComplexHPSReadable");
            }
        }
    }

    public static ComplexHPSReadable create(byte[] _bytes)
    {
        ComplexHPSReadable hcpamplet = new ComplexHPSReadable(_bytes);
        if (hcpamplet.isValid())
        {
            return hcpamplet;
        }
        return null;
    }

    public static ComplexHPSReadable createTwin(IHeadlessAmpletTwinReadable _hamplet)
    {
        ComplexHPSReadable hcpamplet = new ComplexHPSReadable(_hamplet);
        if (hcpamplet.isValid())
        {
            return hcpamplet;
        }
        return null;
    }

    public static ComplexHPSReadable createTwin(ITypedData _data)
    {
        ComplexHPSReadable hcpamplet = new ComplexHPSReadable(_data);
        if (hcpamplet.isValid())
        {
            return hcpamplet;
        }
        return null;
    }
    //endregion

    //region Body Travellers
    @Override
    public ITypedData getNextElement()
    {
        int startingCursor = getCursor();

        Integer firstPrefix = readableHamplet.getNextUnsignedByte();

        if (firstPrefix == null)
        {
            setCursor(startingCursor);
            return null;
        }

        ComplexHPSTypeEnum type = readType(firstPrefix);

        if (type == null)
        {
            setCursor(startingCursor);
            return null;
        }

        if (type == ComplexHPSTypeEnum.NULL_TYPE)
        {
            return getNullITypedData();
        }

        if (type == ComplexHPSTypeEnum.COMPLEX_TYPE)
        {
            return returnComplexITypedData(startingCursor, firstPrefix);
        }

        return returnPrimitiveITypedData(startingCursor, type);
    }

    //region getNextElement() Helpers
    private ComplexHPSTypeEnum readType(int _typeByte)
    {
        if (_typeByte == ComplexHPSTypeEnum.NULL_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.NULL_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.BOOLEAN_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.BOOLEAN_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.BYTE_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.BYTE_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.CHAR_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.CHAR_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.SHORT_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.SHORT_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.INT_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.INT_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.FLOAT_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.FLOAT_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.LONG_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.LONG_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.DOUBLE_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.DOUBLE_TYPE;
        }

        return ComplexHPSTypeEnum.COMPLEX_TYPE;
    }

    private ITypedData getNullITypedData()
    {
        return ComplexHPSTypedData.create(null, ComplexHPSTypeEnum.NULL_TYPE);
    }

    private ITypedData returnComplexITypedData(int _startingCursor, int firstPrefix)
    {
        Integer size = StrategyHelper.readSizePrefixing(firstPrefix,
                ComplexHPSConstants.COMPLEX_SINGLE_PREFIX_OFFSET,
                ComplexHPSConstants.COMPLEX_ONE_BYTE_SECOND_PREFIX_CODE,
                ComplexHPSConstants.COMPLEX_TWO_BYTE_SECOND_PREFIX_CODE,
                ComplexHPSConstants.COMPLEX_FOUR_BYTE_SECOND_PREFIX_CODE,
                readableHamplet);

        if (size == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        byte[] data = readableHamplet.getNextNBytes(size);

        if (data == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        return ComplexHPSTypedData.create(data, ComplexHPSTypeEnum.COMPLEX_TYPE);
    }

    private ITypedData returnPrimitiveITypedData(int _startingCursor, ComplexHPSTypeEnum _type)
    {
        byte[] data = readableHamplet.getNextNBytes(_type.getExpectedSize());

        if (data == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        return ComplexHPSTypedData.create(data, _type);
    }
    //endregion

    //endregion

    //region Version Factory
    private static final IHPSVersionedFactory VERSIONED_FACTORY = new IHPSVersionedFactory()
    {
        @Override
        public IHeadlessPrefixingStrategyReadable build(byte[] _bytes)
        {
            return ComplexHPSReadable.create(_bytes);
        }

        @Override
        public IHeadlessPrefixingStrategyReadable build(ITypedData _data)
        {
            return ComplexHPSReadable.createTwin(_data);
        }

        @Override
        public IHeadlessPrefixingStrategyReadable buildTwin(IHeadlessAmpletTwinReadable _hamplet)
        {
            return ComplexHPSReadable.createTwin(_hamplet);
        }

        @Override
        public char getVersion()
        {
            return ComplexHPSConstants.COMPLEX_HPS_VERSION;
        }
    };

    public static IHPSVersionedFactory getVersionedFactory()
    {
        return VERSIONED_FACTORY;
    }
    //endregion
}
