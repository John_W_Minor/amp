package amp.headless_prefixing_strategies.strategies.complex_hps;

class ComplexHPSConstants
{
    //Formats built on top of HAs should have version numbers greater than 4
    //This is an easy way to ensure there are no collisions with HPAs.
    static final char COMPLEX_HPS_VERSION = 5;

    static final byte NULL_PREFIX_CODE = 0;
    static final byte BOOLEAN_PREFIX_CODE = 1;
    static final byte BYTE_PREFIX_CODE = 2;
    static final byte SHORT_PREFIX_CODE = 3;
    static final byte CHAR_PREFIX_CODE = 4;
    static final byte INT_PREFIX_CODE = 5;
    static final byte FLOAT_PREFIX_CODE = 6;
    static final byte LONG_PREFIX_CODE = 7;
    static final byte DOUBLE_PREFIX_CODE = 8;

    static final byte COMPLEX_ONE_BYTE_SECOND_PREFIX_CODE = 9;
    static final byte COMPLEX_TWO_BYTE_SECOND_PREFIX_CODE = 10;
    static final byte COMPLEX_FOUR_BYTE_SECOND_PREFIX_CODE = 11;

    static final int COMPLEX_SINGLE_PREFIX_OFFSET = COMPLEX_FOUR_BYTE_SECOND_PREFIX_CODE + 1;
    static final int COMPLEX_SINGLE_PREFIX_MAX_FOOTPRINT = 255 - COMPLEX_SINGLE_PREFIX_OFFSET;
}
