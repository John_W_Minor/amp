package amp.headless_prefixing_strategies.strategies.complex_hps;

import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.exceptions.TypedDataMismatchedTypeException;
import amp.headless_amplet.HeadlessAmpletTwinReadable;
import amp.typed_data.AbstractTypedData;
import amp.typed_data.IDataType;
import amp.typed_data.ITypedData;

class ComplexHPSTypedData extends AbstractTypedData
{
    private final ComplexHPSTypeEnum type;

    private ComplexHPSTypedData(byte[] _data, ComplexHPSTypeEnum _type)
    {
        type = _type;

        if (type == null)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _type argument was passed into ComplexHPSTypedData().");
            }
            return;
        }

        if (type == ComplexHPSTypeEnum.NULL_TYPE)
        {
            if (_data != null)
            {
                valid = false;
                if (AmpException.areExceptionsEnabled())
                {
                    throw new TypedDataMismatchedTypeException(type.name());
                }
            }
            return;
        }

        if (_data == null)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _data argument was passed into ComplexHPSTypedData(), and the given type wasn't NULL_TYPE.");
            }
            return;
        }

        if (type.getExpectedSize() != null && _data.length != type.getExpectedSize())
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new TypedDataMismatchedTypeException(type.name());
            }
            return;
        }

        data = HeadlessAmpletTwinReadable.create(_data);
    }

    static ITypedData create(byte[] _data, ComplexHPSTypeEnum _type)
    {
        ITypedData tempHCPAOutput = new ComplexHPSTypedData(_data, _type);
        if (tempHCPAOutput.isValid())
        {
            return tempHCPAOutput;
        }
        return null;
    }

    @Override
    public IDataType getType()
    {
        return type.getType();
    }
}
