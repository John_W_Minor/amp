package amp.headless_prefixing_strategies.strategies.complex_hps;

import amp.AmpConstants;
import amp.Amplet;
import amp.ByteTools;
import amp.exceptions.*;
import amp.headless_prefixing_strategies.AbstractHPSWritable;
import amp.headless_prefixing_strategies.strategies.StrategyHelper;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;
import amp.typed_data.ITypedData;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public class ComplexHPSWritable extends AbstractHPSWritable
{
    //region Constructors
    private ComplexHPSWritable()
    {
        super();

        writableHamplet.addElement(ComplexHPSConstants.COMPLEX_HPS_VERSION);
        setCursor(0);
    }

    private ComplexHPSWritable(byte[] _bytes)
    {
        super(_bytes);

        if (!valid)
        {
            return;
        }

        Character version = readableHamplet.getNextCharacter();

        if (version == null || version != ComplexHPSConstants.COMPLEX_HPS_VERSION)
        {
            valid = false;
            if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidHeadlessPrefixingStrategyDataException("ComplexHPSWritable");
            }
        }
    }

    public static ComplexHPSWritable create()
    {
        return new ComplexHPSWritable();
    }

    public static ComplexHPSWritable create(byte[] _bytes)
    {
        ComplexHPSWritable complexHPSWritable = new ComplexHPSWritable(_bytes);
        if (complexHPSWritable.isValid())
        {
            return complexHPSWritable;
        }
        return null;
    }
    //endregion

    //region Adders
    @Override
    public boolean addBytes(byte[] _bytes)
    {
        return addBytes(_bytes, ComplexHPSTypeEnum.COMPLEX_TYPE);
    }

    private boolean addBytes(byte[] _bytes, ComplexHPSTypeEnum _type)
    {
        if (_type == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _type argument was passed into addBytes().");
            }
            return false;
        }

        if (_bytes == null)
        {
            return addBytesOfNullType();
        }

        if (_type == ComplexHPSTypeEnum.NULL_TYPE)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new TypedDataMismatchedTypeException(_type.name());
            }
            return false;
        }

        if (_type == ComplexHPSTypeEnum.COMPLEX_TYPE)
        {
            return addBytesOfComplexType(_bytes);
        }

        return addBytesOfPrimitiveType(_bytes, _type);
    }

    //region addBytes() Helpers
    private boolean addBytesOfNullType()
    {
        writableHamplet.addElement(ComplexHPSTypeEnum.NULL_TYPE.getPrefixCode());

        return true;
    }

    private boolean addBytesOfComplexType(byte[] _bytes)
    {
        int size = _bytes.length;

        StrategyHelper.insertSizePrefixing(size,
                ComplexHPSConstants.COMPLEX_SINGLE_PREFIX_MAX_FOOTPRINT,
                ComplexHPSConstants.COMPLEX_SINGLE_PREFIX_OFFSET,
                ComplexHPSConstants.COMPLEX_ONE_BYTE_SECOND_PREFIX_CODE,
                ComplexHPSConstants.COMPLEX_TWO_BYTE_SECOND_PREFIX_CODE,
                ComplexHPSConstants.COMPLEX_FOUR_BYTE_SECOND_PREFIX_CODE,
                writableHamplet);

        writableHamplet.addBytes(_bytes);

        return true;
    }

    private boolean addBytesOfPrimitiveType(byte[] _bytes, ComplexHPSTypeEnum _type)
    {
        if (_bytes.length != _type.getExpectedSize())
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new TypedDataMismatchedTypeException(_type.name());
            }

            return false;
        }

        writableHamplet.addElement(_type.getPrefixCode());
        writableHamplet.addBytes(_bytes);

        return true;
    }
    //endregion

    @Override
    public boolean addElement(byte _element)
    {
        byte[] byteArray = {_element};

        return addBytes(byteArray, ComplexHPSTypeEnum.BYTE_TYPE);
    }

    @Override
    public boolean addElement(boolean _element)
    {
        byte[] byteArray = ByteTools.deconstructBoolean(_element);

        return addBytes(byteArray, ComplexHPSTypeEnum.BOOLEAN_TYPE);
    }

    @Override
    public boolean addElement(short _element)
    {
        byte[] byteArray = ByteTools.deconstructShort(_element);

        return addBytes(byteArray, ComplexHPSTypeEnum.SHORT_TYPE);
    }

    @Override
    public boolean addElement(char _element)
    {
        byte[] byteArray = ByteTools.deconstructChar(_element);

        return addBytes(byteArray, ComplexHPSTypeEnum.CHAR_TYPE);
    }

    @Override
    public boolean addElement(int _element)
    {
        byte[] byteArray = ByteTools.deconstructInt(_element);

        return addBytes(byteArray, ComplexHPSTypeEnum.INT_TYPE);
    }

    @Override
    public boolean addElement(float _element)
    {
        byte[] byteArray = ByteTools.deconstructFloat(_element);

        return addBytes(byteArray, ComplexHPSTypeEnum.FLOAT_TYPE);
    }

    @Override
    public boolean addElement(long _element)
    {
        byte[] byteArray = ByteTools.deconstructLong(_element);

        return addBytes(byteArray, ComplexHPSTypeEnum.LONG_TYPE);
    }

    @Override
    public boolean addElement(double _element)
    {
        byte[] byteArray = ByteTools.deconstructDouble(_element);

        return addBytes(byteArray, ComplexHPSTypeEnum.DOUBLE_TYPE);
    }

    @Override
    public boolean addElement(String _element)
    {
        if (_element != null)
        {
            return addBytes(_element.getBytes(AmpConstants.UTF8), ComplexHPSTypeEnum.COMPLEX_TYPE);
        }
        return addBytes(null, ComplexHPSTypeEnum.COMPLEX_TYPE);
    }

    @Override
    public boolean addElement(String _element, String _charsetName) throws UnsupportedEncodingException
    {
        if (_element != null)
        {
            return addBytes(_element.getBytes(_charsetName), ComplexHPSTypeEnum.COMPLEX_TYPE);
        }
        return addBytes(null, ComplexHPSTypeEnum.COMPLEX_TYPE);
    }

    @Override
    public boolean addElement(BigInteger _element)
    {
        if (_element != null)
        {
            return addBytes(_element.toByteArray(), ComplexHPSTypeEnum.COMPLEX_TYPE);
        }
        return addBytes(null, ComplexHPSTypeEnum.COMPLEX_TYPE);
    }

    @Override
    public boolean addElement(IAmpByteSerializable _element)
    {
        if (_element != null)
        {
            return addBytes(_element.serializeToBytes(), ComplexHPSTypeEnum.COMPLEX_TYPE);
        }
        return addBytes(null, ComplexHPSTypeEnum.COMPLEX_TYPE);
    }

    @Override
    public boolean addElement(IAmpAmpletSerializable _element)
    {
        if (_element != null)
        {
            Amplet tempAmp = _element.serializeToAmplet();
            if (tempAmp != null)
            {
                return addBytes(tempAmp.serializeToBytes(), ComplexHPSTypeEnum.COMPLEX_TYPE);
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidAmpletException("Failed to create amplet.");
            }

            return false;
        }
        return addBytes(null, ComplexHPSTypeEnum.COMPLEX_TYPE);
    }
    //endregion

    //region Body Travellers
    @Override
    public ITypedData getNextElement()
    {
        int startingCursor = getCursor();

        Integer firstPrefix = readableHamplet.getNextUnsignedByte();

        if (firstPrefix == null)
        {
            setCursor(startingCursor);
            return null;
        }

        ComplexHPSTypeEnum type = readType(firstPrefix);

        if (type == null)
        {
            setCursor(startingCursor);
            return null;
        }

        if (type == ComplexHPSTypeEnum.NULL_TYPE)
        {
            return getNullITypedData();
        }

        if (type == ComplexHPSTypeEnum.COMPLEX_TYPE)
        {
            return returnComplexITypedData(startingCursor, firstPrefix);
        }

        return returnPrimitiveITypedData(startingCursor, type);
    }

    //region getNextElement() Helpers
    private ComplexHPSTypeEnum readType(int _typeByte)
    {
        if (_typeByte == ComplexHPSTypeEnum.NULL_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.NULL_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.BOOLEAN_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.BOOLEAN_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.BYTE_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.BYTE_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.CHAR_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.CHAR_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.SHORT_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.SHORT_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.INT_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.INT_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.FLOAT_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.FLOAT_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.LONG_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.LONG_TYPE;

        } else if (_typeByte == ComplexHPSTypeEnum.DOUBLE_TYPE.getPrefixCode())
        {
            return ComplexHPSTypeEnum.DOUBLE_TYPE;
        }

        return ComplexHPSTypeEnum.COMPLEX_TYPE;
    }

    private ITypedData getNullITypedData()
    {
        return ComplexHPSTypedData.create(null, ComplexHPSTypeEnum.NULL_TYPE);
    }

    private ITypedData returnComplexITypedData(int _startingCursor, int firstPrefix)
    {
        Integer size = StrategyHelper.readSizePrefixing(firstPrefix,
                ComplexHPSConstants.COMPLEX_SINGLE_PREFIX_OFFSET,
                ComplexHPSConstants.COMPLEX_ONE_BYTE_SECOND_PREFIX_CODE,
                ComplexHPSConstants.COMPLEX_TWO_BYTE_SECOND_PREFIX_CODE,
                ComplexHPSConstants.COMPLEX_FOUR_BYTE_SECOND_PREFIX_CODE,
                readableHamplet);

        if (size == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        byte[] data = readableHamplet.getNextNBytes(size);

        if (data == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        return ComplexHPSTypedData.create(data, ComplexHPSTypeEnum.COMPLEX_TYPE);
    }

    private ITypedData returnPrimitiveITypedData(int _startingCursor, ComplexHPSTypeEnum _type)
    {
        byte[] data = readableHamplet.getNextNBytes(_type.getExpectedSize());

        if (data == null)
        {
            setCursor(_startingCursor);
            return null;
        }

        return ComplexHPSTypedData.create(data, _type);
    }
    //endregion

    //endregion
}
