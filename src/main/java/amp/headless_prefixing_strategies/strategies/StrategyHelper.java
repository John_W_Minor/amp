package amp.headless_prefixing_strategies.strategies;

import amp.AmpConstants;
import amp.ByteTools;
import amp.headless_amplet.IHeadlessAmpletReadable;
import amp.headless_amplet.IHeadlessAmpletWritable;

public class StrategyHelper
{
    public static void insertSizePrefixing(int _size,
                                           int _singlePrefixMaxFootprint,
                                           int _singlePrefixOffset,
                                           byte _oneByteSecondPrefixCode,
                                           byte _twoByteSecondPrefixCode,
                                           byte _fourByteSecondPrefixCode,
                                           IHeadlessAmpletWritable _writableHamplet)
    {
        if (_size <= _singlePrefixMaxFootprint)
        {
            _writableHamplet.addElement((byte) (_size + _singlePrefixOffset));
        } else if (_size < AmpConstants.UBYTE_MAX_VALUE)
        {
            _writableHamplet.addElement(_oneByteSecondPrefixCode);
            _writableHamplet.addElement((byte) _size);
        } else if (_size < AmpConstants.USHORT_MAX_VALUE)
        {
            _writableHamplet.addElement(_twoByteSecondPrefixCode);
            _writableHamplet.addElement((short) _size);
        } else
        {
            _writableHamplet.addElement(_fourByteSecondPrefixCode);
            _writableHamplet.addElement(_size);
        }
    }

    public static Integer readSizePrefixing(int _firstPrefix,
                                            int _singlePrefixOffset,
                                            byte _oneByteSecondPrefixCode,
                                            byte _twoByteSecondPrefixCode,
                                            byte _fourByteSecondPrefixCode,
                                            IHeadlessAmpletReadable _readableHamplet)
    {
        int size = -1;

        if (_firstPrefix >= _singlePrefixOffset)
        {
            size = _firstPrefix - _singlePrefixOffset;

        } else if (_firstPrefix == _oneByteSecondPrefixCode)
        {
            Byte byteSize = _readableHamplet.getNextByte();

            if (byteSize == null)
            {
                return null;
            }

            size = ByteTools.buildUnsignedByte(byteSize);

        } else if (_firstPrefix == _twoByteSecondPrefixCode)
        {
            Character charSize = _readableHamplet.getNextCharacter();

            if (charSize == null)
            {
                return null;
            }

            size = charSize;

        } else if (_firstPrefix == _fourByteSecondPrefixCode)
        {
            Integer intSize = _readableHamplet.getNextInt();

            if (intSize == null)
            {
                return null;
            }

            size = intSize;
        }

        return size;
    }
}
