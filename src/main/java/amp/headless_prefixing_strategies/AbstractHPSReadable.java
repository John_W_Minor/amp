package amp.headless_prefixing_strategies;

import amp.AmpConstants;
import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.headless_amplet.HeadlessAmpletTwinReadable;
import amp.headless_amplet.IHeadlessAmpletReadable;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.typed_data.ITypedData;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractHPSReadable implements IHeadlessPrefixingStrategyReadable
{
    //region Member Fields
    protected int prefixingOffset = AmpConstants.CHAR_BYTE_FOOTPRINT;

    protected IHeadlessAmpletReadable readableHamplet;

    protected boolean valid = false;
    //endregion

    //region Constructors
    protected AbstractHPSReadable()
    {
    }

    protected AbstractHPSReadable(byte[] _bytes)
    {
        if (_bytes != null)
        {
            readableHamplet = HeadlessAmpletTwinReadable.create(_bytes);

            if (readableHamplet != null)
            {
                valid = true;
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _bytes argument was passed into AbstractHPSReadable().");
        }
    }

    protected AbstractHPSReadable(IHeadlessAmpletTwinReadable _hamplet)
    {
        if (_hamplet != null)
        {
            readableHamplet = HeadlessAmpletTwinReadable.createTwin(_hamplet);

            if (readableHamplet != null)
            {
                valid = true;
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _hamplet argument was passed into AbstractHPSReadable().");
        }
    }

    protected AbstractHPSReadable(ITypedData _data)
    {
        if (_data != null)
        {
            readableHamplet = _data.getDataAsHeadlessAmpletTwinReadable();

            if (readableHamplet != null)
            {
                valid = true;
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _data argument was passed into AbstractHPSReadable().");
        }
    }
    //endregion

    //region Getters
    @Override
    public int getByteFootprint()
    {
        return readableHamplet.getByteFootprint();
    }

    @Override
    public boolean isValid()
    {
        return valid;
    }
    //endregion

    //region Cursor Position
    @Override
    public void resetCursor()
    {
        readableHamplet.setCursor(prefixingOffset);
    }

    @Override
    public void setCursor(int _index)
    {
        readableHamplet.setCursor(_index + prefixingOffset);
    }

    @Override
    public int getCursor()
    {
        return readableHamplet.getCursor() - prefixingOffset;
    }

    @Override
    public int getMaxCursorPosition()
    {
        return readableHamplet.getMaxCursorPosition() - prefixingOffset;
    }
    //endregion

    //region Body Travellers
    @Override
    public ITypedData peekNextElement()
    {
        int startingCursor = getCursor();

        ITypedData element = getNextElement();

        setCursor(startingCursor);

        return element;
    }

    @Override
    public boolean hasNextElement()
    {
        ITypedData element = peekNextElement();

        return element != null;
    }

    @Override
    public boolean skipNextElement()
    {
        ITypedData element = getNextElement();

        return element != null;
    }

    @Override
    public int getNumberOfElements()
    {
        int cursor = getCursor();

        resetCursor();

        int count = 0;

        while (skipNextElement())
        {
            count++;
        }

        setCursor(cursor);

        return count;
    }

    @Override
    public boolean travelToElementIndex(int _elementIndex)
    {
        resetCursor();

        for (int i = 0; i < _elementIndex; i++)
        {
            if (!skipNextElement())
            {
                return false;
            }
        }

        return hasNextElement();
    }

    @Override
    public ITypedData travelToElementIndexAndGetElement(int _elementIndex)
    {
        if (!travelToElementIndex(_elementIndex))
        {
            return null;
        }
        return getNextElement();
    }

    @Override
    public ITypedData getElementAtElementIndexNoTravel(int _elementIndex)
    {
        int cursor = getCursor();

        ITypedData element = travelToElementIndexAndGetElement(_elementIndex);

        setCursor(cursor);

        return element;
    }

    @Override
    public List<ITypedData> getAllElements()
    {
        int cursor = getCursor();

        resetCursor();

        ArrayList<ITypedData> elements = new ArrayList<>();

        while (true)
        {
            ITypedData element = getNextElement();

            if (element == null)
            {
                break;
            }

            elements.add(element);
        }

        setCursor(cursor);

        return elements;
    }
    //endregion

    //region Serialization
    @Override
    public byte[] serializeToBytes()
    {
        return readableHamplet.serializeToBytes();
    }
    //endregion
}
