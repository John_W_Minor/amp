package amp.headless_prefixing_strategies;

import amp.AmpConstants;
import amp.Amplet;
import amp.ByteTools;
import amp.backing_lists.GrowModeEnum;
import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.exceptions.InvalidAmpletException;
import amp.headless_amplet.HeadlessAmpletWritable;
import amp.headless_amplet.IHeadlessAmpletWritable;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;
import amp.typed_data.ITypedData;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public abstract class AbstractHPSWritable extends AbstractHPSReadable implements IHeadlessPrefixingStrategyWritable
{
    protected IHeadlessAmpletWritable writableHamplet;

    //region Constructors
    protected AbstractHPSWritable()
    {
        writableHamplet = HeadlessAmpletWritable.create();
        readableHamplet = writableHamplet;

        valid = true;
    }

    protected AbstractHPSWritable(byte[] _bytes)
    {
        if (_bytes != null)
        {
            writableHamplet = HeadlessAmpletWritable.create(_bytes);
            readableHamplet = writableHamplet;

            if (readableHamplet != null)
            {
                valid = true;
            }

        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _bytes argument was passed into AbstractHPSWritable().");
        }
    }
    //endregion

    //region List Features
    @Override
    public int getCurrentSize()
    {
        return writableHamplet.getCurrentSize();
    }

    @Override
    public void ensureCapacity(int _minCapacity)
    {
        writableHamplet.ensureCapacity(_minCapacity + prefixingOffset);
    }

    @Override
    public void setGrowMode(GrowModeEnum _growMode)
    {
        writableHamplet.setGrowMode(_growMode);
    }

    @Override
    public GrowModeEnum getGrowMode()
    {
        return writableHamplet.getGrowMode();
    }

    @Override
    public void trimToSize()
    {
        writableHamplet.trimToSize();
    }
    //endregion

    //region Adders
    @Override
    public boolean addElement(byte _element)
    {
        byte[] byteArray = {_element};

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(boolean _element)
    {
        byte[] byteArray = ByteTools.deconstructBoolean(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(short _element)
    {
        byte[] byteArray = ByteTools.deconstructShort(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(char _element)
    {
        byte[] byteArray = ByteTools.deconstructChar(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(int _element)
    {
        byte[] byteArray = ByteTools.deconstructInt(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(float _element)
    {
        byte[] byteArray = ByteTools.deconstructFloat(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(long _element)
    {
        byte[] byteArray = ByteTools.deconstructLong(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(double _element)
    {
        byte[] byteArray = ByteTools.deconstructDouble(_element);

        return addBytes(byteArray);
    }

    @Override
    public boolean addElement(String _element)
    {
        if (_element != null)
        {
            return addBytes(_element.getBytes(AmpConstants.UTF8));
        }
        return addBytes(null);
    }

    @Override
    public boolean addElement(String _element, String _charsetName) throws UnsupportedEncodingException
    {
        if (_element != null)
        {
            return addBytes(_element.getBytes(_charsetName));
        }
        return addBytes(null);
    }

    @Override
    public boolean addElement(BigInteger _element)
    {
        if (_element != null)
        {
            return addBytes(_element.toByteArray());
        }
        return addBytes(null);
    }

    @Override
    public boolean addElement(IAmpByteSerializable _element)
    {
        if (_element != null)
        {
            return addBytes(_element.serializeToBytes());
        }
        return addBytes(null);
    }

    @Override
    public boolean addElement(IAmpAmpletSerializable _element)
    {
        if (_element != null)
        {
            Amplet tempAmp = _element.serializeToAmplet();
            if (tempAmp != null)
            {
                return addBytes(tempAmp.serializeToBytes());
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidAmpletException("Failed to create amplet.");
            }

            return false;
        }
        return addBytes(null);
    }
    //endregion

    //region Body Travellers
    @Override
    public boolean deleteNextElement()
    {
        int startingCursor = getCursor();

        ITypedData element = getNextElement();

        int byteCount = getCursor() - startingCursor;

        setCursor(startingCursor);

        if (element != null)
        {
            return writableHamplet.deleteNextNBytes(byteCount);
        }

        return false;
    }
    //endregion
}
