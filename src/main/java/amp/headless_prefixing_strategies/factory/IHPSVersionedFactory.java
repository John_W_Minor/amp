package amp.headless_prefixing_strategies.factory;

public interface IHPSVersionedFactory extends IHPSFactory
{
    char getVersion();
}
