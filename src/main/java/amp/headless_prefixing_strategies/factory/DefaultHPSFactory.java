package amp.headless_prefixing_strategies.factory;

import amp.AmpLogging;
import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.exceptions.NoCompatibleVersionedFactoryAvailableException;
import amp.exceptions.errors.ConflictingHPSBuilderVersionsError;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.strategies.complex_hps.ComplexHPSReadable;
import amp.headless_prefixing_strategies.strategies.simple_hps.SimpleHPSReadable;
import amp.typed_data.ITypedData;
import logging.IAmpexLogger;

import java.util.ArrayList;
import java.util.List;

public class DefaultHPSFactory implements IHPSFactoryExtendable
{
    private static List<IHPSVersionedFactory> versionedFactories = new ArrayList<>();

    private boolean verbose = false;

    public DefaultHPSFactory()
    {
        this(false);
    }

    public DefaultHPSFactory(boolean _verbose)
    {
        verbose = _verbose;

        IHPSVersionedFactory dummyOne = new IHPSVersionedFactory()
        {
            @Override
            public IHeadlessPrefixingStrategyReadable build(byte[] _bytes)
            {
                return null;
            }

            @Override
            public IHeadlessPrefixingStrategyReadable build(ITypedData _data)
            {
                return null;
            }

            @Override
            public IHeadlessPrefixingStrategyReadable buildTwin(IHeadlessAmpletTwinReadable _hamplet)
            {
                return null;
            }

            @Override
            public char getVersion()
            {
                return 1;
            }
        };

        IHPSVersionedFactory dummyTwo = new IHPSVersionedFactory()
        {
            @Override
            public IHeadlessPrefixingStrategyReadable build(byte[] _bytes)
            {
                return null;
            }

            @Override
            public IHeadlessPrefixingStrategyReadable build(ITypedData _data)
            {
                return null;
            }

            @Override
            public IHeadlessPrefixingStrategyReadable buildTwin(IHeadlessAmpletTwinReadable _hamplet)
            {
                return null;
            }

            @Override
            public char getVersion()
            {
                return 2;
            }
        };

        versionedFactories.add(dummyOne);
        versionedFactories.add(dummyTwo);
        versionedFactories.add(SimpleHPSReadable.getVersionedFactory());
        versionedFactories.add(ComplexHPSReadable.getVersionedFactory());
    }

    @Override
    public IHeadlessPrefixingStrategyReadable build(byte[] _bytes)
    {
        if (_bytes == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _bytes argument was passed into build().");
            }
            return null;
        }

        IHeadlessPrefixingStrategyReadable hps = null;

        for (IHPSVersionedFactory builder : versionedFactories)
        {
            try
            {
                hps = builder.build(_bytes);

                if (hps != null)
                {
                    break;
                }

            } catch (Exception e)
            {
                if (verbose)
                {
                    IAmpexLogger logger = AmpLogging.getLogger();
                    if (logger != null)
                    {
                        logger.warn("", e);
                    }
                }
            }
        }

        if (hps == null && AmpException.areExceptionsEnabled())
        {
            throw new NoCompatibleVersionedFactoryAvailableException();
        }

        return hps;
    }

    @Override
    public IHeadlessPrefixingStrategyReadable build(ITypedData _data)
    {
        if (_data == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _data argument was passed into build().");
            }
            return null;
        }
        return buildTwin(_data.getDataAsHeadlessAmpletTwinReadable());
    }

    @Override
    public IHeadlessPrefixingStrategyReadable buildTwin(IHeadlessAmpletTwinReadable _hamplet)
    {
        if (_hamplet == null)
        {
            if (AmpException.areExceptionsEnabled())
            {
                throw new BadArgumentException("Null _hamplet argument was passed into buildTwin().");
            }
            return null;
        }

        IHeadlessPrefixingStrategyReadable hps = null;

        for (IHPSVersionedFactory builder : versionedFactories)
        {
            try
            {
                hps = builder.buildTwin(_hamplet);

                if (hps != null)
                {
                    break;
                }

            } catch (Exception e)
            {
                if (verbose)
                {
                    IAmpexLogger logger = AmpLogging.getLogger();
                    if (logger != null)
                    {
                        logger.warn("", e);
                    }
                }
            }
        }

        if (hps == null && AmpException.areExceptionsEnabled())
        {
            throw new NoCompatibleVersionedFactoryAvailableException();
        }

        return hps;
    }

    @Override
    public void addVersionedFactory(IHPSVersionedFactory _versionedFactory)
    {
        if (_versionedFactory != null)
        {
            char probationVersion = _versionedFactory.getVersion();

            for (IHPSVersionedFactory builder : versionedFactories)
            {
                char testingVersion = builder.getVersion();
                if (testingVersion == probationVersion)
                {
                    throw new ConflictingHPSBuilderVersionsError(probationVersion);
                }
            }

            versionedFactories.add(_versionedFactory);
        }
    }

    //region Statics
    public static final IHPSFactory staticFactory = new DefaultHPSFactory();

    public static IHeadlessPrefixingStrategyReadable staticBuild(byte[] _bytes)
    {
        return staticFactory.build(_bytes);
    }

    public static IHeadlessPrefixingStrategyReadable staticBuild(ITypedData _data)
    {
        return staticFactory.build(_data);
    }

    public static IHeadlessPrefixingStrategyReadable staticBuildTwin(IHeadlessAmpletTwinReadable _hamplet)
    {
        return staticFactory.buildTwin(_hamplet);
    }
    //endregion
}
