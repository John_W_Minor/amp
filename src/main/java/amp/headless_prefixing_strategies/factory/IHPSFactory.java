package amp.headless_prefixing_strategies.factory;

import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.serialization.IAmpByteFactory;
import amp.typed_data.ITypedData;

public interface IHPSFactory extends IAmpByteFactory
{
    IHeadlessPrefixingStrategyReadable build(byte[] _bytes);

    IHeadlessPrefixingStrategyReadable build(ITypedData _data);

    IHeadlessPrefixingStrategyReadable buildTwin(IHeadlessAmpletTwinReadable _hamplet);
}
