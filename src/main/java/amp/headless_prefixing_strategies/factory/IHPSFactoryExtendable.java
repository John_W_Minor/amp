package amp.headless_prefixing_strategies.factory;

public interface IHPSFactoryExtendable extends IHPSFactory
{
    void addVersionedFactory(IHPSVersionedFactory _versionedFactory);
}
