package amp;

import amp.backing_lists.ByteList;
import amp.exceptions.AmpException;
import amp.exceptions.BadArgumentException;
import amp.headless_amplet.abstract_classes.AbstractHeadlessAmpletWritable;

@Deprecated
public class HeadlessAmplet extends AbstractHeadlessAmpletWritable
{
    //Headless Amplet objects use no prefixing to note where data is
    //because it is assumed they will only be used when the programmer has a strong guarantee about the byte footprint if specific data.

    //region Constructors
    private HeadlessAmplet()
    {
        validHeadlessAmplet = true;
    }

    private HeadlessAmplet(byte[] _bytes)
    {
        if (_bytes != null)
        {
            bytes = new ByteList(_bytes);

            validHeadlessAmplet = true;
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _bytes argument was passed into HeadlessAmplet().");
        }
    }

    public static HeadlessAmplet create()
    {
        return new HeadlessAmplet();
    }

    public static HeadlessAmplet create(byte[] _bytes)
    {
        HeadlessAmplet tempHeadless = new HeadlessAmplet(_bytes);
        if (tempHeadless.isValidHeadlessAmplet())
        {
            return tempHeadless;
        }
        return null;
    }
    //endregion
}
