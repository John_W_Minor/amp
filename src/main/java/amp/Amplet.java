package amp;

import amp.classification.IAmpClassCollection;
import amp.classification.classes.IAmpClass;
import amp.exceptions.*;
import amp.group_ids.GroupID;
import amp.group_primitives.*;
import amp.group_primitives.tools.GroupHeaderTools;
import amp.group_primitives.tools.GroupTools;
import amp.headless_amplet.*;
import amp.serialization.IAmpByteSerializable;
import amp.serialization.IAmpDeserializable;
import logging.IAmpexLogger;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;

public class Amplet implements IAmpByteSerializable, IAmpDeserializable
{
    //region Member Fields
    private byte[] bytes = null;

    private ArrayList<GroupHeader> groupHeaders = null;
    private int initialGroupsCount = 0;
    private int ampHeaderAndGroupSize = -1;

    private UnpackedGroup[] unpackedGroupsArray = null;

    private ArrayList<UnpackedGroup> unpackedGroupsList = new ArrayList<>();
    private ArrayList<UnpackedGroup> addedUnpackedGroupsList = new ArrayList<>();

    private byte[] trailer = null;

    private boolean validAmplet = false;
    //endregion

    //region Constructors
    private Amplet(IAmpClassCollection _ampClassCollection)
    {
        this(_ampClassCollection.getUnpackedGroups());
    }

    private Amplet(IAmpClass _ampClass)
    {
        this(_ampClass.getUnpackedGroups());
    }

    private Amplet(ArrayList<UnpackedGroup> _unpackedGroups)
    {
        if (_unpackedGroups != null)
        {
            if (_unpackedGroups.size() > 0 && _unpackedGroups.size() < AmpConstants.GROUPS_MAX_COUNT)
            {
                initialGroupsCount = _unpackedGroups.size();
                unpackedGroupsArray = new UnpackedGroup[initialGroupsCount];
                groupHeaders = new ArrayList<>(initialGroupsCount);

                for (int i = 0; i < initialGroupsCount; i++)
                {
                    unpackedGroupsArray[i] = _unpackedGroups.get(i);
                    unpackedGroupsList.add(unpackedGroupsArray[i]);
                    groupHeaders.add(unpackedGroupsArray[i].getHeader());
                }

                sealGroups();

                boolean soAreGroupsValid = areGroupsValid();
                boolean soDoRepeatGroupIDsExist = doRepeatGroupIDsExist();

                validAmplet = soAreGroupsValid && !soDoRepeatGroupIDsExist;

                if (!validAmplet && AmpException.areExceptionsEnabled())
                {
                    throw new InvalidAmpletException(soAreGroupsValid, soDoRepeatGroupIDsExist);
                }
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new UnsupportedGroupCountException(_unpackedGroups.size());
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _unpackedGroups argument was passed into Amplet().");
        }
    }

    private Amplet(byte[] _bytes)
    {
        if (_bytes != null)
        {
            bytes = _bytes.clone();

            groupHeaders = GroupHeaderTools.processHeader(bytes);

            if (groupHeaders != null)
            {
                initialGroupsCount = groupHeaders.size();
                unpackedGroupsArray = new UnpackedGroup[initialGroupsCount];

                boolean soAreGroupsValid = areGroupsValid();
                boolean soDoRepeatGroupIDsExist = doRepeatGroupIDsExist();
                //calculateAmpHeaderAndGroupSize();
                boolean soAreThereSufficientBytes = areThereSufficientBytes();

                validAmplet = soAreGroupsValid && !soDoRepeatGroupIDsExist && soAreThereSufficientBytes;

                if (!validAmplet && AmpException.areExceptionsEnabled())
                {
                    throw new InvalidAmpletException(soAreGroupsValid, soDoRepeatGroupIDsExist, soAreThereSufficientBytes);
                }

                if (ampHeaderAndGroupSize < bytes.length)
                {
                    int trailerSize = bytes.length - ampHeaderAndGroupSize;

                    trailer = new byte[trailerSize];

                    System.arraycopy(bytes, ampHeaderAndGroupSize, trailer, 0, trailerSize);
                }

            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidGroupHeaderException("No headers generated.");
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _bytes argument was passed into Amplet().");
        }
    }


    public static Amplet create(IAmpClassCollection _ampClassCollection)
    {
        return create(_ampClassCollection.getUnpackedGroups());
    }

    public static Amplet create(IAmpClass _ampClass)
    {
        return create(_ampClass.getUnpackedGroups());
    }

    public static Amplet create(UnpackedGroup _unpackedGroup)
    {
        ArrayList<UnpackedGroup> groups = new ArrayList<>();
        groups.add(_unpackedGroup);
        return create(groups);
    }

    public static Amplet create(ArrayList<UnpackedGroup> _unpackedGroups)
    {
        Amplet tempAmplet = new Amplet(_unpackedGroups);
        if (tempAmplet.isValidAmplet())
        {
            return tempAmplet;
        }
        return null;
    }

    public static Amplet create(byte[] _bytes)
    {
        Amplet tempAmplet = new Amplet(_bytes);
        if (tempAmplet.isValidAmplet())
        {
            return tempAmplet;
        }
        return null;
    }
    //endregion

    //region Construction and Verification Utilities
    private void sealGroups()
    {
        for (UnpackedGroup unpacked : unpackedGroupsList)
        {
            unpacked.seal();
        }
    }

    private boolean areGroupsValid()
    {
        for (GroupHeader gh : groupHeaders)
        {
            if (!gh.isValidGroup())
            {
                return false;
            }
        }
        return true;
    }

    private boolean doRepeatGroupIDsExist()
    {
        for (GroupHeader header : groupHeaders)
        {
            if (doesGroupIDExistMoreThanOnce(header))
            {
                return true;
            }
        }
        return false;
    }

    private boolean doesGroupIDExistMoreThanOnce(GroupHeader _header)
    {
        int count = 0;
        for (GroupHeader currentHeader : groupHeaders)
        {
            if (currentHeader.getClassID() == _header.getClassID() && currentHeader.getClassInstanceID() == _header.getClassInstanceID())
            {
                count++;
                if (count > 1)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void calculateAmpHeaderAndGroupSize()
    {
        if (bytes != null && groupHeaders != null)
        {
            ampHeaderAndGroupSize = GroupHeaderTools.getHeaderLength(bytes);
            for (GroupHeader gh : groupHeaders)
            {
                ampHeaderAndGroupSize += gh.getByteCountTotalAllElements();
            }
        }
    }

    private boolean areThereSufficientBytes()
    {
        calculateAmpHeaderAndGroupSize();
        if (ampHeaderAndGroupSize <= bytes.length && ampHeaderAndGroupSize != -1)
        {
            return true;
        }
        return false;
    }
    //endregion

    //the only normal getter in this tangle
    public boolean isValidAmplet()
    {
        return validAmplet;
    }

    //region Does Group/Class Exist?
    private boolean doesGroupIDExist(long _classID, long _classInstanceID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE && _classInstanceID > -1 && _classInstanceID < AmpConstants.CLASS_INSTANCE_ID_MAX_VALUE)
        {
            for (GroupHeader header : groupHeaders)
            {
                if (header.getClassID() == _classID && header.getClassInstanceID() == _classInstanceID)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean doesGroupIDExist(GroupID _groupID)
    {
        if (_groupID != null)
        {
            return doesGroupIDExist(_groupID.getClassID(), _groupID.getClassInstanceID());
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into doesGroupIDExist().");
        }
        return false;
    }

    public boolean doesClassIDExist(long _classID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE)
        {
            for (GroupHeader header : groupHeaders)
            {
                if (header.getClassID() == _classID)
                {
                    return true;
                }
            }
        }
        return false;
    }
    //endregion

    //region Groups
    private UnpackedGroup unpackGroup(long _classID, long _classInstanceID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE && _classInstanceID > -1 && _classInstanceID < AmpConstants.CLASS_INSTANCE_ID_MAX_VALUE)
        {
            for (int i = 0; i < initialGroupsCount; i++)
            {
                if (groupHeaders.get(i).getClassID() == _classID && groupHeaders.get(i).getClassInstanceID() == _classInstanceID)
                {
                    if (unpackedGroupsArray[i] == null)
                    {
                        UnpackedGroup temp = UnpackedGroup.create(groupHeaders.get(i), bytes);
                        unpackedGroupsArray[i] = temp;
                        unpackedGroupsList.add(temp);
                        return temp;
                    } else
                    {
                        return unpackedGroupsArray[i];
                    }
                }
            }

            for (UnpackedGroup group : addedUnpackedGroupsList)
            {
                if (group.getHeader().getClassID() == _classID && group.getHeader().getClassInstanceID() == _classInstanceID)
                {
                    return group;
                }
            }
        }
        return null;
    }

    public UnpackedGroup unpackGroup(GroupID _groupID)
    {
        if (_groupID != null)
        {
            return unpackGroup(_groupID.getClassID(), _groupID.getClassInstanceID());
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into unpackGroup().");
        }
        return null;
    }

    private UnpackedGroup getUnpackedGroup(long _classID, long _classInstanceID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE && _classInstanceID > -1 && _classInstanceID < AmpConstants.CLASS_INSTANCE_ID_MAX_VALUE)
        {
            for (UnpackedGroup group : unpackedGroupsList)
            {
                if (group.getHeader().getClassID() == _classID && group.getHeader().getClassInstanceID() == _classInstanceID)
                {
                    return group;
                }
            }

            for (UnpackedGroup group : addedUnpackedGroupsList)
            {
                if (group.getHeader().getClassID() == _classID && group.getHeader().getClassInstanceID() == _classInstanceID)
                {
                    return group;
                }
            }
        }
        return null;
    }

    public UnpackedGroup getUnpackedGroup(GroupID _groupID)
    {
        if (_groupID != null)
        {
            return getUnpackedGroup(_groupID.getClassID(), _groupID.getClassInstanceID());
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into getUnpackedGroup().");
        }
        return null;
    }

    public boolean addUnpackedGroup(UnpackedGroup _group)
    {
        if (validAmplet && _group != null)
        {
            GroupHeader tempHeader = _group.getHeader();

            if (!doesGroupIDExist(tempHeader.getClassID(), tempHeader.getClassInstanceID()))
            {
                groupHeaders.add(tempHeader);
                addedUnpackedGroupsList.add(_group);
                _group.seal();
                return true;
            }
        }
        return false;
    }

    public boolean addMultipleUnpackedGroups(ArrayList<UnpackedGroup> _groups)
    {
        if (validAmplet && _groups != null)
        {
            boolean success = true;

            for (UnpackedGroup group : _groups)
            {
                if (doesGroupIDExist(group.getHeader().getClassID(), group.getHeader().getClassInstanceID()))
                {
                    return false;
                }
            }

            for (UnpackedGroup group : _groups)
            {
                addUnpackedGroup(group);
            }
            return true;
        }
        return false;
    }

    private boolean unpackAndDeleteGroup(long _classID, long _classInstanceID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE && _classInstanceID > -1 && _classInstanceID < AmpConstants.CLASS_INSTANCE_ID_MAX_VALUE)
        {
            UnpackedGroup temp = unpackGroup(_classID, _classInstanceID);
            if (temp != null && !temp.getHeader().isWriteLocked())
            {
                temp.clearElements();
                return true;
            }
        }
        return false;
    }

    public boolean unpackAndDeleteGroup(GroupID _groupID)
    {
        if (_groupID != null)
        {
            return unpackAndDeleteGroup(_groupID.getClassID(), _groupID.getClassInstanceID());
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into unpackAndDeleteGroup().");
        }
        return false;
    }
    //endregion

    //region Classes
    public ArrayList<UnpackedGroup> unpackClass(long _classID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE)
        {
            ArrayList<UnpackedGroup> unpackedClass = new ArrayList<>();

            for (int i = 0; i < initialGroupsCount; i++)
            {
                if (groupHeaders.get(i).getClassID() == _classID)
                {
                    if (unpackedGroupsArray[i] == null)
                    {
                        UnpackedGroup temp = UnpackedGroup.create(groupHeaders.get(i), bytes);
                        unpackedGroupsArray[i] = temp;
                        unpackedGroupsList.add(temp);
                        unpackedClass.add(temp);
                    } else
                    {
                        unpackedClass.add(unpackedGroupsArray[i]);
                    }
                }
            }

            for (UnpackedGroup group : addedUnpackedGroupsList)
            {
                if (group.getHeader().getClassID() == _classID)
                {
                    unpackedClass.add(group);
                }
            }

            if (unpackedClass.size() != 0)
            {
                return unpackedClass;
            }
        }
        return null;
    }

    public ArrayList<UnpackedGroup> getUnpackedClass(long _classID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE)
        {
            ArrayList<UnpackedGroup> unpackedClass = new ArrayList<>();

            for (UnpackedGroup group : unpackedGroupsList)
            {
                if (group.getHeader().getClassID() == _classID)
                {
                    unpackedClass.add(group);
                }
            }

            for (UnpackedGroup group : addedUnpackedGroupsList)
            {
                if (group.getHeader().getClassID() == _classID)
                {
                    unpackedClass.add(group);
                }
            }

            if (unpackedClass.size() != 0)
            {
                return unpackedClass;
            }
        }
        return null;
    }

    public boolean addUnpackedClass(IAmpClass _ampClass)
    {
        if (validAmplet && _ampClass != null)
        {
            return addMultipleUnpackedGroups(_ampClass.getUnpackedGroups());
        }
        return false;
    }

    public boolean addUnpackedClassCollection(IAmpClassCollection _ampClassCollection)
    {
        if (validAmplet && _ampClassCollection != null)
        {
            return addMultipleUnpackedGroups(_ampClassCollection.getUnpackedGroups());
        }
        return false;
    }

    public boolean unpackAndDeleteClass(long _classID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE)
        {
            ArrayList<UnpackedGroup> temp = unpackClass(_classID);
            if (temp != null && !ByteTools.isClassIDWriteLocked(_classID))
            {
                for (UnpackedGroup group : temp)
                {
                    group.clearElements();
                }
                return true;
            }
        }
        return false;
    }
    //endregion

    //region Trailer
    public byte[] getTrailer()
    {
        if (trailer != null)
        {
            return trailer.clone();
        }
        return null;
    }

    public String getTrailerAsString()
    {
        if (trailer != null)
        {
            return new String(trailer, AmpConstants.UTF8);
        }
        return null;
    }

    public String getTrailerAsString(String _charsetName) throws UnsupportedEncodingException
    {
        if (trailer != null)
        {
            return new String(trailer, _charsetName);
        }
        return null;
    }

    public String getTrailerAsHexString()
    {
        if (trailer != null)
        {
            return ByteTools.buildHexString(trailer);
        }
        return null;
    }

    public BigInteger getTrailerAsBigInteger()
    {
        if (trailer != null)
        {
            return new BigInteger(trailer);
        }
        return null;
    }

    @Deprecated
    public HeadlessAmplet getTrailerAsHeadlessAmplet()
    {
        if (trailer != null)
        {
            return HeadlessAmplet.create(trailer);
        }
        return null;
    }

    public IHeadlessAmpletTwinReadable getElementAsIHeadlessAmpletTwinReadable()
    {
        if (trailer != null)
        {
            return HeadlessAmpletTwinReadable.create(trailer);
        }
        return null;
    }

    public IHeadlessAmpletWritable getTrailerAsIHeadlessAmpletWritable()
    {
        if (trailer != null)
        {
            return HeadlessAmpletWritable.create(trailer);
        }
        return null;
    }

    public Amplet getTrailerAsAmplet()
    {
        if (trailer != null)
        {
            try
            {
                return Amplet.create(trailer);
            } catch (Exception e)
            {
                IAmpexLogger logger = AmpLogging.getLogger();
                if (logger != null)
                {
                    logger.warn("", e);
                }
                return null;
            }
        }
        return null;
    }

    public void setTrailer(byte[] _trailer)
    {
        if (_trailer == null)
        {
            trailer = null;
            return;
        }

        trailer = _trailer.clone();
    }

    public void setTrailer(IAmpByteSerializable _trailer)
    {
        setTrailer(_trailer.serializeToBytes());
    }
    //endregion

    //region Serializing to Amp Byte Format
    private boolean markGroupForDeletion(long _classID, long _classInstanceID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE && _classInstanceID > -1 && _classInstanceID < AmpConstants.CLASS_INSTANCE_ID_MAX_VALUE)
        {
            for (GroupHeader header : groupHeaders)
            {
                if (header.getClassID() == _classID && header.getClassInstanceID() == _classInstanceID)
                {
                    header.markForDeletion();
                    return true;
                }
            }
        }
        return false;
    }

    public boolean markGroupForDeletion(GroupID _groupID)
    {
        if (_groupID != null)
        {
            return markGroupForDeletion(_groupID.getClassID(), _groupID.getClassInstanceID());
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _groupID argument was passed into markGroupForDeletion().");
        }
        return false;
    }

    public boolean markClassForDeletion(long _classID)
    {
        if (validAmplet && _classID > -1 && _classID < AmpConstants.CLASS_ID_MAX_VALUE)
        {
            boolean success = false;
            for (GroupHeader header : groupHeaders)
            {
                if (header.getClassID() == _classID)
                {
                    header.markForDeletion();
                    success = true;
                }
            }
            return success;
        }
        return false;
    }

    public ArrayList<PackedGroup> repackGroups()
    {
        if (validAmplet)
        {
            ArrayList<PackedGroup> repackedGroups = new ArrayList<>();

            for (int i = 0; i < initialGroupsCount; i++)
            {
                if (!groupHeaders.get(i).isMarkedForDeletion())
                {
                    if ((unpackedGroupsArray[i] == null || !unpackedGroupsArray[i].isChanged()) && bytes != null)
                    {
                        repackedGroups.add(GroupTools.packGroupFromByteArray(groupHeaders.get(i), bytes));
                    } else if (unpackedGroupsArray[i] != null && unpackedGroupsArray[i].getNumberOfElements() != 0)
                    {
                        repackedGroups.add(GroupTools.repackUnpackedGroup(unpackedGroupsArray[i]));
                    }
                }
            }

            for (UnpackedGroup group : addedUnpackedGroupsList)
            {
                if (!group.getHeader().isMarkedForDeletion() && group.getNumberOfElements() != 0)
                {
                    repackedGroups.add(GroupTools.repackUnpackedGroup(group));
                }
            }

            if (repackedGroups.size() > 0 && repackedGroups.size() < AmpConstants.GROUPS_MAX_COUNT)
            {
                return repackedGroups;
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new UnsupportedGroupCountException(repackedGroups.size());
            }
        }
        return null;
    }

    public Amplet repackSelf()
    {
        if (validAmplet)
        {
            return create(serializeToBytes());
        }
        return null;
    }

    public byte[] serializeToBytes()
    {
        if (validAmplet)
        {
            return GroupTools.buildAmpletBytesFromPackedGroups(repackGroups(), trailer);
        }
        return null;
    }
    //endregion
}
