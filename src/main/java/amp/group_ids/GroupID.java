package amp.group_ids;

import amp.AmpConstants;
import amp.AmpLogging;
import amp.exceptions.*;
import logging.IAmpexLogger;

import java.util.ArrayList;

public class GroupID
{
    //region Member Fields
    private long classID = 0;
    private long classInstanceID = 0;
    private boolean duplicatable = false;
    private String name = null;

    private boolean collision = false;
    //endregion

    //region Constructors
    public GroupID(long _classID, long _classInstanceID, String _name, boolean _duplicatable, boolean _warning)
    {
        classID = _classID;
        classInstanceID = _classInstanceID;
        name = _name;
        duplicatable = _duplicatable;

        if (!(classID > -1 && classID < AmpConstants.CLASS_ID_MAX_VALUE))
        {
            throw new InvalidClassIDException(classID);
        }

        if (!(classInstanceID > -1 && classInstanceID < AmpConstants.CLASS_ID_MAX_VALUE))
        {
            throw new InvalidClassInstanceIDException(classInstanceID);
        }

        if (_warning)
        {
            GroupID collider = registerGroupIDIfNoCollision(this);

            if (collider != null)
            {
                collision = true;

                if (strongCollisionProtectionEnabled)
                {
                    throw new DuplicateGroupIDException(this, collider);
                } else
                {
                    IAmpexLogger logger = AmpLogging.getLogger();

                    String warn = "\nWarning: There are colliding Amp Group IDs:" +
                            "\nColliding Class ID is: " + classID +
                            "\nColliding Class Instance ID is: " + classInstanceID +
                            "\nThe name of this Group ID is: " + name +
                            "\nThe name of the other Group ID is: " + collider.getName();

                    if (logger != null)
                    {
                        AmpLogging.getLogger().warn(warn);
                    }
                }
            }
        }
    }

    public GroupID(long _classID, long _classInstanceID, String _name, boolean _warning)
    {
        this(_classID, _classInstanceID, _name, false, _warning);
    }

    public GroupID(long _classID, long _classInstanceID, String _name)
    {
        this(_classID, _classInstanceID, _name, false, true);
    }
    //endregion

    //region Getters
    public long getClassID()
    {
        return classID;
    }

    public long getClassInstanceID()
    {
        return classInstanceID;
    }

    public boolean isDuplicatable()
    {
        return duplicatable;
    }

    public String getName()
    {
        return name;
    }

    public boolean isThereACollision()
    {
        return collision;
    }
    //endregion

    //region Collision Protection
    private static volatile boolean collisionProtectionEnabled = true;

    private static volatile boolean strongCollisionProtectionEnabled = false;

    private static volatile ArrayList<GroupID> registeredGroupIDs = new ArrayList<>();

    public static synchronized GroupID registerGroupIDIfNoCollision(GroupID _groupID)
    {
        if (_groupID != null)
        {
            if (!collisionProtectionEnabled)
            {
                return null;
            }

            for (GroupID registeredGroupID : registeredGroupIDs)
            {
                if (registeredGroupID.getClassID() == _groupID.getClassID() && registeredGroupID.getClassInstanceID() == _groupID.getClassInstanceID())
                {
                    if (!registeredGroupID.isDuplicatable() || !_groupID.isDuplicatable())
                    {
                        return registeredGroupID;
                    } else
                    {
                        return null;
                    }
                }
            }

            registeredGroupIDs.add(_groupID);
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _element argument was passed into registerGroupIDIfNoCollision().");
        }

        return null;
    }

    public static synchronized void unregisterGroupID(GroupID _groupID)
    {
        if (_groupID != null)
        {
            registeredGroupIDs.removeIf((GroupID registeredGroupID) -> registeredGroupID.getClassID() == _groupID.getClassID()
                    && registeredGroupID.getClassInstanceID() == _groupID.getClassInstanceID());
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _element argument was passed into unregisterGroupID().");
        }
    }

    public static synchronized void unregisterAllGroupIDs()
    {
        registeredGroupIDs.clear();
    }

    public static synchronized void enableCollisionProtection()
    {
        collisionProtectionEnabled = true;
    }

    public static synchronized void disableCollisionProtection()
    {
        collisionProtectionEnabled = false;

        unregisterAllGroupIDs();
    }

    public static synchronized void enableStrongCollisionProtection()
    {
        strongCollisionProtectionEnabled = true;
    }

    public static synchronized void disableStrongCollisionProtection()
    {
        strongCollisionProtectionEnabled = false;
    }
    //endregion
}
