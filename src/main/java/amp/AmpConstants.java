package amp;

import java.nio.charset.Charset;

public class AmpConstants
{
    //region Amp Version
    public static final short MAJOR_VERSION = 0;
    public static final short MINOR_VERSION = 2;
    public static final String VERSIONSTRING = MAJOR_VERSION + "." + MINOR_VERSION;

    public static final int VERSIONASINTEGER = ByteTools.concatenateShorts(MAJOR_VERSION, MINOR_VERSION);
    //endregion

    //region Bytemasks
    public static final long AMPLET_CLASS_MARKER = 0x40000000L;
    public static final long WRITE_LOCK_MARKER = 0x80000000L;
    //endregion

    //region Byte Footprints
    public static final int BYTE_BYTE_FOOTPRINT = 1;
    public static final int BOOLEAN_BYTE_FOOTPRINT = 1 * BYTE_BYTE_FOOTPRINT;
    public static final int SHORT_BYTE_FOOTPRINT = 2 * BYTE_BYTE_FOOTPRINT;
    public static final int CHAR_BYTE_FOOTPRINT = 2 * BYTE_BYTE_FOOTPRINT;
    public static final int INT_BYTE_FOOTPRINT = 4 * BYTE_BYTE_FOOTPRINT;
    public static final int FLOAT_BYTE_FOOTPRINT = 4 * BYTE_BYTE_FOOTPRINT;
    public static final int LONG_BYTE_FOOTPRINT = 8 * BYTE_BYTE_FOOTPRINT;
    public static final int DOUBLE_BYTE_FOOTPRINT = 8 * BYTE_BYTE_FOOTPRINT;

    public static final int GROUP_HEADER_BYTE_FOOTPRINT = INT_BYTE_FOOTPRINT + INT_BYTE_FOOTPRINT + INT_BYTE_FOOTPRINT + INT_BYTE_FOOTPRINT;
    //endregion

    //region Max Values
    public static final double CLASS_ID_MAX_VALUE = Math.pow(2, 32);
    public static final double CLASS_INSTANCE_ID_MAX_VALUE = Math.pow(2, 32);

    public static final int UBYTE_MAX_VALUE = 256;
    public static final int USHORT_MAX_VALUE = 65536;
    //endregion

    //region Implementation Specific Constants
    public static final int SAFETY_PADDING = 500 * BYTE_BYTE_FOOTPRINT;
    public static final int ELEMENT_COUNT_MAX = (Integer.MAX_VALUE * BYTE_BYTE_FOOTPRINT) - SAFETY_PADDING;
    public static final int ELEMENT_MAX_BYTE_FOOTPRINT = (Integer.MAX_VALUE * BYTE_BYTE_FOOTPRINT) - SAFETY_PADDING;
    public static final int BYTE_ARRAY_MAX_BYTE_COUNT = (Integer.MAX_VALUE * BYTE_BYTE_FOOTPRINT) - SAFETY_PADDING;
    public static final int GROUPS_MAX_COUNT = (Integer.MAX_VALUE * BYTE_BYTE_FOOTPRINT) - SAFETY_PADDING;
    //endregion

    public static final Charset UTF8 = Charset.availableCharsets().get("UTF-8");
}
