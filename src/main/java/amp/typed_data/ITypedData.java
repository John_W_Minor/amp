package amp.typed_data;

import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_amplet.IHeadlessAmpletWritable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.factory.IHPSFactory;
import amp.serialization.IAmpByteSerializable;

public interface ITypedData extends IAmpByteSerializable
{
    byte[] getData();

    IHeadlessAmpletTwinReadable getDataAsHeadlessAmpletTwinReadable();

    IHeadlessAmpletWritable getDataAsIHeadlessAmpletWritable();

    IHeadlessPrefixingStrategyReadable getDataAsIHeadlessPrefixingStrategyReadable();

    IHeadlessPrefixingStrategyReadable getDataAsIHeadlessPrefixingStrategyReadable(IHPSFactory _hpsFactory);

    IDataType getType();

    boolean isValid();
}