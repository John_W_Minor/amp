package amp.typed_data.defaults;

import amp.typed_data.IDataType;

public enum DefaultDataTypeEnum implements IDataType
{
    INDETERMINATE_TYPE,
    NULL_TYPE,
    BOOLEAN_TYPE,
    BYTE_TYPE,
    SHORT_TYPE,
    CHAR_TYPE,
    INT_TYPE,
    FLOAT_TYPE,
    LONG_TYPE,
    DOUBLE_TYPE,
    COMPLEX_TYPE,
    KEY_TYPE,
    VALUE_TYPE;
}
