package amp.typed_data.defaults;

import amp.headless_amplet.HeadlessAmpletTwinReadable;
import amp.typed_data.AbstractTypedData;
import amp.typed_data.IDataType;
import amp.typed_data.ITypedData;

public class DefaultTypedData extends AbstractTypedData
{
    private DefaultTypedData(byte[] _data)
    {
        data = HeadlessAmpletTwinReadable.create(_data);
        valid = true;
    }

    public static ITypedData create(byte[] _data)
    {
        ITypedData tempData = new DefaultTypedData(_data);
        if (tempData.isValid())
        {
            return tempData;
        }
        return null;
    }

    @Override
    public IDataType getType()
    {
        return DefaultDataTypeEnum.INDETERMINATE_TYPE;
    }
}
