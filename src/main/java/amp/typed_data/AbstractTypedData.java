package amp.typed_data;

import amp.headless_amplet.HeadlessAmpletTwinReadable;
import amp.headless_amplet.HeadlessAmpletWritable;
import amp.headless_amplet.IHeadlessAmpletTwinReadable;
import amp.headless_amplet.IHeadlessAmpletWritable;
import amp.headless_prefixing_strategies.IHeadlessPrefixingStrategyReadable;
import amp.headless_prefixing_strategies.factory.DefaultHPSFactory;
import amp.headless_prefixing_strategies.factory.IHPSFactory;

public abstract class AbstractTypedData implements ITypedData
{
    protected IHeadlessAmpletTwinReadable data;

    protected boolean valid = true;

    @Override
    public byte[] getData()
    {
        if (data != null)
        {
            return data.serializeToBytes();
        }
        return null;
    }

    @Override
    public IHeadlessAmpletTwinReadable getDataAsHeadlessAmpletTwinReadable()
    {
        if (data != null)
        {
            return HeadlessAmpletTwinReadable.createTwin(data);
        }
        return null;
    }

    @Override
    public IHeadlessAmpletWritable getDataAsIHeadlessAmpletWritable()
    {
        if (data != null)
        {
            return HeadlessAmpletWritable.create(data.serializeToBytes());
        }
        return null;
    }

    @Override
    public IHeadlessPrefixingStrategyReadable getDataAsIHeadlessPrefixingStrategyReadable()
    {
        return getDataAsIHeadlessPrefixingStrategyReadable(DefaultHPSFactory.staticFactory);
    }

    @Override
    public IHeadlessPrefixingStrategyReadable getDataAsIHeadlessPrefixingStrategyReadable(IHPSFactory _hpsFactory)
    {
        if (data != null)
        {
            return _hpsFactory.buildTwin(data);
        }
        return null;
    }

    @Override
    public boolean isValid()
    {
        return valid;
    }

    @Override
    public byte[] serializeToBytes()
    {
        return getData();
    }
}
