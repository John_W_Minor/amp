package amp.typed_data;

public interface IDataType
{
    String name();
}
