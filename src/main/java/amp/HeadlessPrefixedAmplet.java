package amp;

import amp.backing_lists.GrowModeEnum;
import amp.exceptions.*;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class HeadlessPrefixedAmplet implements IAmpByteSerializable
{
    //This was not converted to the HPS system because it returns naked byte arrays and legacy HeadlessAmplets.
    //Of course legacy HeadlessAmplets are just a duplicate implementation of HeadlessAmpletWritable now, but that's neither here nor there.

    //region Member Fields
    private HeadlessAmplet hamplet = null;

    private boolean validHeadlessPrefixedAmplet = false;
    //endregion

    //region Constructors
    private HeadlessPrefixedAmplet()
    {
        hamplet = HeadlessAmplet.create();
        validHeadlessPrefixedAmplet = true;
    }

    private HeadlessPrefixedAmplet(byte[] _bytes)
    {
        if (_bytes != null)
        {
            hamplet = HeadlessAmplet.create(_bytes);

            if (hamplet != null)
            {
                validHeadlessPrefixedAmplet = true;
            }
        } else if (AmpException.areExceptionsEnabled())
        {
            throw new BadArgumentException("Null _bytes argument was passed into HeadlessPrefixedAmplet().");
        }
    }

    public static HeadlessPrefixedAmplet create()
    {
        return new HeadlessPrefixedAmplet();
    }

    public static HeadlessPrefixedAmplet create(byte[] _bytes)
    {
        HeadlessPrefixedAmplet hpamplet = new HeadlessPrefixedAmplet(_bytes);
        if (hpamplet.isValidHeadlessPrefixedAmplet())
        {
            return hpamplet;
        }
        return null;
    }
    //endregion

    //region Getters
    public int getByteFootprint()
    {
        return hamplet.getByteFootprint();
    }

    public boolean isValidHeadlessPrefixedAmplet()
    {
        return validHeadlessPrefixedAmplet;
    }
    //endregion

    //region Cursor Position
    public void resetCursor()
    {
        hamplet.resetCursor();
    }

    public void setCursor(int _index)
    {
        hamplet.setCursor(_index);
    }

    public int getCursor()
    {
        return hamplet.getCursor();
    }

    public int getMaxCursorPosition()
    {
        return hamplet.getMaxCursorPosition();
    }
    //endregion

    //region List Features
    public int getCurrentSize()
    {
        return hamplet.getCurrentSize();
    }

    public void ensureCapacity(int _minCapacity)
    {
        hamplet.ensureCapacity(_minCapacity);
    }

    public void setGrowMode(GrowModeEnum _growMode)
    {
        hamplet.setGrowMode(_growMode);
    }

    public GrowModeEnum getGrowMode()
    {
        return hamplet.getGrowMode();
    }

    public void trimToSize()
    {
        hamplet.trimToSize();
    }
    //endregion

    //region Adders
    public boolean addBytes(byte[] _bytes)
    {
        if (_bytes == null)
        {
            hamplet.addElement((byte) 1);
            hamplet.addElement((byte) 0);

            return true;
        }

        int size = _bytes.length;

        if (size == 0)
        {
            hamplet.addElement((byte) 1);
            hamplet.addElement((byte) 0);

            return true;
        }

        byte prefixSize;

        if (size < 256)
        {
            prefixSize = 1;
        } else if (size < 65536)
        {
            prefixSize = 2;
        } else
        {
            prefixSize = 4;
        }

        hamplet.addElement(prefixSize);

        if (prefixSize == 4)
        {
            hamplet.addElement(size);
        } else if (prefixSize == 2)
        {
            hamplet.addElement((short) size);
        } else// if (prefixSize == 1)
        {
            hamplet.addElement((byte) size);
        }

        hamplet.addBytes(_bytes);

        return true;
    }

    public boolean addElement(byte _element)
    {
        byte[] byteArray = {_element};

        return addBytes(byteArray);
    }

    public boolean addElement(boolean _element)
    {
        byte[] byteArray = ByteTools.deconstructBoolean(_element);

        return addBytes(byteArray);
    }

    public boolean addElement(short _element)
    {
        byte[] byteArray = ByteTools.deconstructShort(_element);

        return addBytes(byteArray);
    }

    public boolean addElement(char _element)
    {
        byte[] byteArray = ByteTools.deconstructChar(_element);

        return addBytes(byteArray);
    }

    public boolean addElement(int _element)
    {
        byte[] byteArray = ByteTools.deconstructInt(_element);

        return addBytes(byteArray);
    }

    public boolean addElement(float _element)
    {
        byte[] byteArray = ByteTools.deconstructFloat(_element);

        return addBytes(byteArray);
    }

    public boolean addElement(long _element)
    {
        byte[] byteArray = ByteTools.deconstructLong(_element);

        return addBytes(byteArray);
    }

    public boolean addElement(double _element)
    {
        byte[] byteArray = ByteTools.deconstructDouble(_element);

        return addBytes(byteArray);
    }

    public boolean addElement(String _element)
    {
        if (_element != null)
        {
            return addBytes(_element.getBytes(AmpConstants.UTF8));
        }
        return addBytes(null);
    }

    public boolean addElement(String _element, String _charsetName) throws UnsupportedEncodingException
    {
        if (_element != null)
        {
            return addBytes(_element.getBytes(_charsetName));
        }
        return addBytes(null);
    }

    public boolean addElement(BigInteger _element)
    {
        if (_element != null)
        {
            return addBytes(_element.toByteArray());
        }
        return addBytes(null);
    }

    public boolean addElement(IAmpByteSerializable _element)
    {
        if (_element != null)
        {
            return addBytes(_element.serializeToBytes());
        }
        return addBytes(null);
    }

    public boolean addElement(IAmpAmpletSerializable _element)
    {
        if (_element != null)
        {
            Amplet tempAmp = _element.serializeToAmplet();
            if (tempAmp != null)
            {
                return addBytes(tempAmp.serializeToBytes());
            } else if (AmpException.areExceptionsEnabled())
            {
                throw new InvalidAmpletException("Failed to create amplet.");
            }

            return false;
        }
        return addBytes(null);
    }
    //endregion

    //region Body Travellers
    public byte[] getNextElement()
    {
        int startingCursor = hamplet.getCursor();

        Byte prefixSize = hamplet.getNextByte();

        if (prefixSize == null)
        {
            hamplet.setCursor(startingCursor);
            return null;
        }

        if (prefixSize == 4)
        {
            Integer intSize = hamplet.getNextInt();

            if (intSize == null)
            {
                hamplet.setCursor(startingCursor);
                return null;
            }

            return hamplet.getNextNBytes(intSize);
        } else if (prefixSize == 2)
        {
            Character charSize = hamplet.getNextCharacter();

            if (charSize == null)
            {
                hamplet.setCursor(startingCursor);
                return null;
            }

            return hamplet.getNextNBytes(charSize);
        } else if (prefixSize == 1)
        {
            Byte byteSize = hamplet.getNextByte();

            if (byteSize == null)
            {
                hamplet.setCursor(startingCursor);
                return null;
            }

            int size = ByteTools.buildUnsignedByte(byteSize);

            return hamplet.getNextNBytes(size);
        } else
        {
            hamplet.setCursor(startingCursor);
            return null;
        }
    }

    public byte[] peekNextElement()
    {
        int startingCursor = hamplet.getCursor();

        byte[] bytes = getNextElement();

        hamplet.setCursor(startingCursor);

        return bytes;
    }

    public boolean hasNextElement()
    {
        byte[] element = peekNextElement();

        return element != null;
    }

    public boolean skipNextElement()
    {
        byte[] element = getNextElement();

        return element != null;
    }

    public boolean deleteNextElement()
    {
        int startingCursor = hamplet.getCursor();

        byte[] element = getNextElement();

        int byteCount = hamplet.getCursor() - startingCursor;

        hamplet.setCursor(startingCursor);

        if (element != null)
        {
            return hamplet.deleteNextNBytes(byteCount);
        }

        return false;
    }

    public HeadlessAmplet getNextElementAsHeadlessAmplet()
    {
        byte[] nextElement = getNextElement();

        if (nextElement == null)
        {
            return null;
        }

        return HeadlessAmplet.create(nextElement);
    }

    public HeadlessAmplet peekNextElementAsHeadlessAmplet()
    {
        byte[] nextElement = peekNextElement();

        if (nextElement == null)
        {
            return null;
        }

        return HeadlessAmplet.create(nextElement);
    }

    public int getNumberOfElements()
    {
        int cursor = hamplet.getCursor();

        hamplet.resetCursor();

        int count = 0;

        while (skipNextElement())
        {
            count++;
        }

        hamplet.setCursor(cursor);

        return count;
    }

    public void travelToElementIndex(int _elementIndex)
    {
        hamplet.resetCursor();

        for (int i = 0; i < _elementIndex && hasNextElement(); i++)
        {
            skipNextElement();
        }
    }

    public byte[] travelToElementIndexAndGetElement(int _elementIndex)
    {
        travelToElementIndex(_elementIndex);
        return getNextElement();
    }

    public byte[] getElementAtElementIndexNoTravel(int _elementIndex)
    {
        int cursor = hamplet.getCursor();

        travelToElementIndex(_elementIndex);
        byte[] tempData = getNextElement();

        hamplet.setCursor(cursor);

        return tempData;
    }

    public List<byte[]> getAllElements()
    {
        int cursor = hamplet.getCursor();

        hamplet.resetCursor();

        ArrayList<byte[]> hcpaDataList = new ArrayList<>();

        while (true)
        {
            byte[] tempData = getNextElement();

            if (tempData == null)
            {
                break;
            }

            hcpaDataList.add(tempData);
        }

        hamplet.setCursor(cursor);

        return hcpaDataList;
    }
    //endregion

    //region Serialization
    public byte[] serializeToBytes()
    {
        return hamplet.serializeToBytes();
    }
    //endregion
}
